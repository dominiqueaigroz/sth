<!DOCTYPE html>
<!--
Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
Titre: annuaire_stage
Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
Version: 1.0.0
Date: 25.11.2016
Copyright: Entreprise Ecole CFPT-I © 2016-2017
-->
<?php
require_once './inc.view.php';
require_once '../Model/inc.all.php';
if (ESession::getRole() === false){
	header('location: ./index.php');
}
else{
	if (ESession::getRole() !== EC_ROLE_ADMIN)
		header('location: ./index.php');
}
?>
<html>
<head lang="fr">
	<?php require_once './head.php'; ?>
	<title>Détail du tuteur</title>
</head>
<body>
	<header class="cd-morph-dropdown">
		<?php
			include_once '../php/Nav/bar_nav.php';
		?>
	</header>
		<section id="maincontent" class="container-fluid">
			<h1 id="tutor">Détail du tuteur</h1>
			<section class="table-responsive">
				<table id="content" class="tablesorter table">
					<thead class="thead-inverse">
						<tr>
							<th>Stagiaire</th>
							<th>Entreprise</th>
							<th>Domaine</th>
							<th>Sujet du stage</th>
							<th>Secteur</th>
							<th>Année</th>
						</tr>
					</thead>
					<tbody id="students-data">
					</tbody>
				</table>
				<a href="./listTutor.php"><button class="btn btn-primary"><span class="fa fa-arrow-left" aria-hidden="true"> <span class="buttonText">Retour</span></span></button></a>
			</section>
		</section>
<?php 
	include_once './footer.html';
?>
</body>
<script>
$(document).ready(function(){
	var email = "<?php echo $_POST['email']; ?>";
	var tutorName = "<?php echo $_POST['tutorName']; ?>";
	
	$('#tutor').html(tutorName);
	
	ELibrary.get_data('../Controller/get_allTraineeFromUser.php', createTraineehipsStudents, {'tutor':email});

	/**
	 * Construit un tableau qui contient la liste des stages pour chaque stagiaire qu'a eu un tuteur
	 * @param JSON arData			Tableau JSON qui contient les données à afficher
	 */
	function createTraineehipsStudents(arData) {
		var el = $('#students-data');

		arData.forEach(function(table){
			var tr = $('<tr>');

			var tdStagiaire = $('<td>');
			tdStagiaire.html(table.stud.civility + ' ' + table.stud.firstName + ' ' + table.stud.lastName)
			tr.append(tdStagiaire);

			var tdEnter = $('<td>');
			var link = $('<a name="' + table.enter.name + '" class="enterLinks" id="' + table.enter.id + '" href="#">');
			link.click(function(event){
				event.preventDefault();
				var id = $(this).attr('id');
				var name = $(this).attr('name');
				$.redirect('./description_enterprise.php',{id: id, enterName: name},"POST");
			});
			link.html(table.enter.name);
			tdEnter.append(link);
			tr.append(tdEnter);

			var tdDomain = $('<td>');
			tdDomain.html(table.enter.domain.name);
			tr.append(tdDomain);

			var tdSubj = $('<td>');
			var link = $('<a name="' + table.subj + '" class="enterLinks" id="' + table.id + '" href="#">');
			link.click(function(event){
				event.preventDefault();
				var id = $(this).attr('id');
				var name = $(this).attr('name');
				$.redirect('./description_traineeship.php',{id: id, traineeship: name},"POST");
			});
			link.html(table.subj);
			tdSubj.append(link);
			tr.append(tdSubj);

			var tdSect = $('<td>');
			var tdSectHtml = '';
			$indexSect = 0;
			table.sectors.forEach(function(sector){
				$indexSect++;
				if($indexSect > 1)
					tdSectHtml = tdSectHtml + ', ' +  sector.name;
				else 
					tdSectHtml = sector.name;	
			})
			tdSect.html(tdSectHtml);
			tr.append(tdSect);


			var tdYear = $('<td>');
			var year = '';
			if (table.dateEnd != null){
				var date = new Date(table.dateEnd);
				year = date.getFullYear();
			}
			else {
				year = 'Aucune information';
			}
			tdYear.html(year);
			tr.append(tdYear);
			
			el.append(tr);
		}) // End forEach
	}
	
});
</script>
</html>