<!DOCTYPE html>
<!--
Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
Titre: annuaire_stage
Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
Version: 1.0.0
Date: 25.11.2016
Copyright: Entreprise Ecole CFPT-I © 2016-2017
-->
<?php
require_once './inc.view.php';
require_once './popup/popupAssignement.html';
require_once '../Model/inc.all.php';
if (ESession::getRole() === false){
	header('location: ./index.php');
}
else{
	if (ESession::getRole() !== EC_ROLE_ADMIN)
		header('location: ./index.php');
}
?>
<html>
	<head lang="fr">
	<?php require_once './head.php'; ?>
		<title>Affectation</title>
	</head>
	<body>
		<header class="cd-morph-dropdown">
			<?php
				include_once '../php/Nav/bar_nav.php';
			?>
		</header>
		<section id="maincontent" class="container-fluid">
			<h1>Affectation</h1>
			<section style="display: none" id="assignementAccepted" class="alert alert-success" role="alert">
				<strong>Réussi!</strong> L'assignement a bien <strong>fonctionné</strong>.
			</section>
			<section style="display: none" id="assignementRefused" class="alert alert-danger" role="alert">
				<strong>Réussi!</strong> L'assignement a bien <strong>fonctionné</strong>.
			</section>
			<section class="table-responsive col-sm-8 col-lg-4">
				<table class="table">
					<tbody id="content">
						<tr>
							<th><label for="trainee">Sélectionner un stage :</label></th>
						</tr>
						<tr>
							<td><select size=5 id="trainee" class="form-control smallTableElementSM"></select></td>
						</tr>
						<tr>
							<th><label for="student">Sélectionner l'élève participant au stage :</label></th>
						</tr>
						<tr>
							<td><select id="student" class="form-control smallTableElementSM"></select></td>
						</tr>
						<tr>
							<th><label for="tutor">Sélectionner le tuteur s'occupant du stagiaire :</label></th>
						</tr>
						<tr>
							<td><select id="tutor" class="form-control smallTableElementSM"></select></td>
						</tr>
						
						<tr>
							<td>
								<button class="btn btn-primary buttonComment" id="valid">Confirmer</button>
								<button style="display:none" id="btnPopupValid" data-toggle="modal" data-target="#assignement"></button>
							</td>
						</tr>
					</tbody>
				</table>
			</section>
			<fieldset>
				<legend>Stages actuellement disponible</legend>
				<section class="table-responsive">
					<table class="table">
						<thead class="thead-inverse">
							<tr>
								<th>Stages</th>
								<th>Tuteur</th>
								<th>Élève</th>
							</tr>
						</thead>
						<tbody id="contentTrainee">
						</tbody>
					</table>
				</section>
			</fieldset>
		</section>
		<?php 
			include_once './footer.html';
		?>
</body>
<script>
$(document).ready(function() {
	ELibrary.get_data('../Controller/get_traineeships.php', createTraineeshipsListForAssignement);
	ELibrary.get_data('../Controller/get_availableTrainee.php', processData, {'student':true, 'tutor':true});
	
	$('#valid').click(function() {
		var traineeSubj = $('#trainee option:selected').text();
		var studentName = $('#student option:selected').text();
		var tutorName = $('#tutor option:selected').text();

		if(traineeSubj && studentName && tutorName != "") {
			$('#btnPopupValid').click();
		}
	});

	// Affiche une popup de confirmation pour l'ajout ou la modification d'une entreprise
	$('#btnPopupValid').click(function(traineeSubj, studentName, tutorName){
		var traineeSubj = $('#trainee option:selected').text();
		var studentName = $('#student option:selected').text();
		var tutorName = $('#tutor option:selected').text();
		$('#assignementToShow').text("Êtes-vous sûr de vouloir assigner " + studentName + " à " + tutorName + " au stage suivant : " + traineeSubj);
	});

	$('#btnAccept').click(function(){
		var traineeSubj = $('#trainee option:selected').text();
		var traineeId = $('#trainee option:selected').val();
		var emailStudent = $.trim($('#student option:selected').val()); // Le trim permet d'évité une potentielle erreur lors de l'envoi du mail
		var emailTutor = $.trim($('#tutor option:selected').val());

		var studentName = $('#student option:selected').text();
		var tutorName = $('#tutor option:selected').text();

		var subject = "Vous avez été assigner à un stage";
		var msgStudent = "Bonjour " + studentName + ", vous avez été assigné au stage : " + traineeSubj + ", et vous aurez comme tuteur : " + tutorName;
		var msgTutor = "Bonjour " + tutorName + ", vous avez été assigné au stage : " + traineeSubj + ", et vous aurez comme élève : " + studentName;
		
		ELibrary.get_data('../Controller/set_assignement.php', showConfirmation, {'traineeship': traineeId, 'emailStudent': emailStudent, 'emailTutor': emailTutor, 'studentName': studentName, 'tutorName': tutorName, 'msgStudent': msgStudent, 'msgTutor': msgTutor, 'subject': subject, 'delete': 'false'});
	});

	/**
	* Recharge les select et le tableau qui son présent sur la pages afin qu'ils soient à jours
	*/
	function proccessAssignement() {
		$('#contentTrainee > tr').remove(); // Suppression du contenu actuel du tableau
		ELibrary.get_data('../Controller/get_traineeships.php', createTraineeshipsListForAssignement, undefined);
		ELibrary.get_data('../Controller/get_availableTrainee.php', processData, {'student':true, 'tutor':true});
	}
	
	/**
	 * Construit un tableau qui contient les offres de stages
	 * @param JSON 		arData		Tableau JSON qui contient les offres de stages
	 */
	 function createTraineeshipsListForAssignement(arData) {			
		var el = $('#contentTrainee');

		arData.forEach(function(table){

			var tr = $('<tr>');

			// Ce lien sert à supprimer un assignement
			var crossDelete = $('<a class="delete" title="supprimer l\'assignement" name="' + table.subj + '" class="traineeshipLinks" id="' + table.id + '" href="#"><span class="fa fa-times"></span></a>');
			crossDelete.click(function(event){
				event.preventDefault();
				var id = $(this).attr('id');
				// L'appel ajax vas envoier les données afin que la supréssion puisse se faire puis, il vas recharger les champs de la page
			    $.ajax({
				    method: 'POST',
				    url: '../Controller/set_assignement.php',
				    data: {'traineeship': id, 'delete': 'true'},
				    success: function(data) {
					    // Recharge les champs de la page
						proccessAssignement();
				    },
				    error: function(jqXHR) {
					    
				    }
			    });
			});
			
			var tdSubj = $('<td>');
			var link = $('<a name="' + table.subj + '" class="traineeshipLinks" id="' + table.id + '" href="#">');
			link.click(function(event){
				event.preventDefault();
				var id = $(this).attr('id');
				var name = $(this).attr('name');
				$.redirect('./description_traineeship.php',{id: id, traineeship: name},"POST");
			});
			link.html(table.subj);
			tdSubj.append(crossDelete);
			tdSubj.append(link);
			tr.append(tdSubj);

			var tdTutor = $('<td>');
			tdTutor.html(table.tutor);
			tr.append(tdTutor);

			var tdStudent = $('<td>');
			tdStudent.html(table.stud);
			tr.append(tdStudent);
			
			el.append(tr)
			
			// Mise à jour du cache pour les tablesorter
			el.trigger("update");

		})
	}
	
	/**
	 * Appel les fonctions d'affichages pour la page d'assignement
	 * @param JSON data	Tableau JSON qui contient nos différentes données
	 */
	function processData(data) {
		
		createOptionsTraineeships(data.DataTrainee);
		createOptionsPeople(data.DataStudent, 'student');
		createOptionsPeople(data.DataTutor, 'tutor');
	}

	/**
	 * Crée les options pour la liste déroulante des élèves et tuteurs
	 * @param JSON arData	Tableau JSON qui contient les données à afficher
	 */
	function createOptionsTraineeships(arData) {
		var mainSelect = $('#trainee');
		mainSelect.empty();
		firstTrainee = true;
		arData.forEach(function(table){	
			var tdOption = $('<option>');
			tdOption.val(table.id);
			tdOption.append(table.subj);
			if(firstTrainee) {
				// Sélectionne par défaut le premier stage
				tdOption.attr('selected', 'selected')
				firstTrainee = false;
			}
			mainSelect.append(tdOption);
		}) // End forEach
	}

	/**
	 * Crée les options pour la liste déroulante des élèves et tuteurs
	 * @param JSON arData		Tableau JSON qui contient les données à afficher
	 * @param string idSelect	L'ID de la balise <select> dans laquelle on va ajouté nos options
	 */
	function createOptionsPeople(arData, idSelect) {
		var el = $('#' + idSelect);
		el.empty();
		arData.forEach(function(table){	
			var tdOption = $('<option>');
			tdOption.val(table.email);
			var people = table.civility + ' ' + table.firstName + ' ' + table.lastName;
			tdOption.append(people);
			el.append(tdOption);
		}) // End forEach
	}
	
	/**
	 * Affiche un message qui confirme la création d'un nouvel assignement
	 * @param bool ok	Si ok vaut true alors l'assignement a correctement fonctionné | sinon une erreur est survenue
	 */
	function showConfirmation(ok) {
		if (ok) {
			ELibrary.showHtmlElement('assignementAccepted');
			ELibrary.get_data('../Controller/get_availableTrainee.php', processData, {'student':true, 'tutor':true});
			$('#contentTrainee > tr').remove(); // Suppression du contenu actuel du tableau
			ELibrary.get_data('../Controller/get_traineeships.php', createTraineeshipsListForAssignement, undefined);
		}
		else {
			ELibrary.showHtmlElement('assignementRefused');
		}
	}
	
});
</script>
</html>