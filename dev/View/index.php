<!DOCTYPE html>
<?php
/*
 * Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017
 */
require_once './inc.view.php';
require_once '../Model/ESession.php';
if (ESession::getRole() === false) {
	ESession::setRole(EC_ROLE_UNKNOWN);
}
?>
<html>
<head lang="fr">
<?php require_once './head.php'; ?>
<title>Accueil</title>
</head>
<body>

	<header class="cd-morph-dropdown">
		<?php
		include_once '../php/Nav/bar_nav.php';
		?>
	</header>
	<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow="ratio: 6:3; animation: push">

    <ul class="uk-slideshow-items">
        <li>
            <img src="../img/tpg.png" alt="" uk-cover>
        </li>
        <li>
			<img src="../img/sig.jpg" alt="" uk-cover>
        </li>
        <li>
			<img src="../img/ubs.png" alt="" uk-cover>
        </li>
    </ul>

    <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
    <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>

</div>
	<?php
	include_once './footer.html';
	?>
	</body>
</html>