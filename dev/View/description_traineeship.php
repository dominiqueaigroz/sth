<!DOCTYPE html>
<?php
/*
 * Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017
 */
require_once './inc.view.php';
 if (ESession::getRole() === false){
	header('location: ./index.php');
}
else if ((ESession::getRole() !== EC_ROLE_ADMIN)&&(ESession::getRole() !== EC_ROLE_USER)){
	header('location: ./index.php');
}
?>
<html>
	<head lang="fr">
		<?php require_once './head.php'; ?>
		<title>Description du stage</title>
	</head>
<body>
	<header class="cd-morph-dropdown">
		<?php
		// C'est pour les tests, mais cette variable sera settée
		// après le login
		include_once '../php/Nav/bar_nav.php';
		?>	
	</header>
	<section id="maincontent" class="container-fluid">
		<h1>Description du stage</h1>
		<section class="table-responsive">
			<table id="content" class="table">
				<thead class="thead-inverse">
					<tr>
						<th>Entreprise</th>
						<th>Domaine</th>
						<th>Sujet du stage</th>
						<th>Secteur</th>
						<th>Durée</th>
						<th>Date de début</th>
						<th>Date de fin</th>
						<th>Disponibilité</th>
						<th>Etat</th>
					</tr>
				</thead>
				<tbody id="traineeship-data"></tbody>
			</table>
		</section>
		<fieldset id="infoTraineeship">
			<legend>Informations complémentaires </legend>
			<section class="table-responsive">
				<table id="information-data" class="modif table smallTable"></table>
			</section>
		</fieldset>
		<fieldset id="infoStudentAndTutor-data">
			<legend>Informations sur le stagiaire et le tuteur </legend>
			<section class="table-responsive">
				<table id="studentAndTutor-data" class="table smallTable"></table>
			</section>
		</fieldset>
		<fieldset id="appreciationTraineeship">
			<legend>Appréciation du stage</legend>
			<section class="table-responsive">
				<table id="appreciation-data" class="table smallTable"></table>
			</section>
		</fieldset>
	</section>
	<?php 
		include_once './footer.html';
	?>
</body>
<script>
$(document).ready(function(){
	var role = "<?php if (ESession::getRole() !== false){ echo ESession::getRole(); } ?>";
	var roleAdmin = "<?php echo EC_ROLE_ADMIN; ?>";	
	var traineeship = "<?php echo $_POST['id']; ?>";

	var classTable = 'smallTableElementSM';
	
	ELibrary.get_data('../Controller/get_traineeships.php', processTraineeships, {'traineeship': traineeship });
	
	function processTraineeships(data) {
		createTraineeship(data);
		processDescriptionTraineeship(data);
	}
	
	if (role == roleAdmin) {
		modifButton();
	}
	
	function modifButton() {
		var button = $('<button class="btn btn-primary"><span class="fa fa-pencil"></span> Modifier</button>');
		button.click(function(event){
			event.preventDefault();
			$.redirect('./edit_traineeships.php',{id: traineeship},"POST");
		});
		$('.modif').append(button);
	}
	
	/**
	 * Construit un tableau qui contient l'offre de stages pour la page de description
	 * @param JSON arData	Tableau JSON qui contient les offres de stages
	 */
	function createTraineeship (arData) {			
		var el = $('#traineeship-data');
		
		var tr = $('<tr>');
	
		var tdName = $('<td>');
		var link = $('<a name="' + arData.enter.name + '" class="enterLinks" id="' + arData.enter.id + '" href="#">');
		link.click(function(event){
			event.preventDefault();
			var id = $(this).attr('id');
			var name = $(this).attr('name');
			$.redirect('./description_enterprise.php',{id: id, enterName: name},"POST");
		});
		link.html(arData.enter.name);
		tdName.append(link);
		tr.append(tdName);
	
		var tdDomain = $('<td>');
		tdDomain.html(arData.enter.domain.name);
		tr.append(tdDomain);
	
		var tdSubj = $('<td>');
		tdSubj.append(arData.subj);
		tr.append(tdSubj);
	
		var tdSect = $('<td>');
		var tdSectHtml = 'Non définit';
		$indexSect = 0;
		arData.sectors.forEach(function(sector){
			$indexSect++;
			if($indexSect > 1)
				tdSectHtml = tdSectHtml + ', ' +  sector.name;
			else 
				tdSectHtml = sector.name;	
		})
		tdSect.html(tdSectHtml);
		tr.append(tdSect);
	
		var tdDuration = $('<td>');
		tdDuration.html(arData.duration);
		tr.append(tdDuration);
	
		var tdDateBeg = $('<td>');
		if (arData.dateBeg != null)
			tdDateBeg.html(arData.dateBeg);
		else
			tdDateBeg.html('Non définie');
		tr.append(tdDateBeg);
	
		var tdDateEnd = $('<td>')
		if (arData.dateBeg != null)
			tdDateEnd.html(arData.dateEnd);
		else
			tdDateEnd.html('Non définie');
		tr.append(tdDateEnd);
	
		var tdAvailab = $('<td>');
		tdAvailab.html(arData.availab.label);
		tr.append(tdAvailab);
	
		var tdStatus = $('<td>');
		tdStatus.html(arData.status);
		tr.append(tdStatus);
		
		el.append(tr)
		
		// Mise à jour du cache pour les tablesorter
		el.trigger("update");
	}

	/**
	 * Construit un tableau avec les données d'une offre de stage
	 * @param arData	Tableau JSON qui contient les données d'une offre de stage
	 */
	function processDescriptionTraineeship(arData) {

		// Section "Information complémentaires"
		var mainTab = $('#information-data');
		createInformationTraineeship(mainTab, "Mission à accomplir : ", arData.mission);
		createInformationTraineeship(mainTab, "Compétences et savoir-faire requis : ", arData.compet);
		createInformationTraineeship(mainTab, "Remarque : ", arData.remark);
		createInformationTraineeship(mainTab, "Responsable du stage : ", arData.chargeOf.civility, arData.chargeOf.lastName, arData.chargeOf.firstName);
		createInformationTraineeship(mainTab, "Téléphone de travail : ", arData.chargeOf.workPhone);
		createInformationTraineeship(mainTab, "Téléphone privé : ", arData.chargeOf.mobilePhone);
		createInformationTraineeship(mainTab, "Email : ", arData.chargeOf.email);

		// Section "Information sur le stagiaire et le tuteur"
		mainTab = $('#studentAndTutor-data');
		if (typeof arData.stud.civility === 'undefined') {
			createInformationTraineeship(mainTab, "Aucun stagiaire n'est assigné au stage : ");
		}
		else {
			createInformationTraineeship(mainTab, "Stagiaire : ", arData.stud.civility, arData.stud.lastName, arData.stud.firstName);
			createInformationTraineeship(mainTab, "Email du stagiaire : ", arData.stud.email);
		}
		if (typeof arData.tutor.civility === 'undefined') {
			createInformationTraineeship(mainTab, "Aucun tuteur n'est assigné au stage : ");
		}
		else {
			createInformationTraineeship(mainTab, "Tuteur : ", arData.tutor.civility, arData.tutor.lastName, arData.tutor.firstName);
			createInformationTraineeship(mainTab, "Email du tuteur : ", arData.tutor.email);	
		}

		// Section "Appréciation du stage"
		mainTab = $('#appreciation-data');
		arData.criteria.forEach(function(criterion) {
			createInformationTraineeship(mainTab, criterion.label, criterion.satisfaction);
		})
		
		createStudentComment(mainTab, arData.comm.comment);
		
	}

	/**
	 * Ajout d'une donnée d'un stage dans un tableau
	 * @param string el			Il s'agit de l'élément html dans lequel on va ajouter notre donnée
	 * @param string label		Le texte associé à la donnée
	 * @param string data		Contient la donnée à ajouter
	 * @param string lastName	Le nom de la personne (facultatif)
	 * @param string firstName	Le prénom de la personne (facultatif)
	 */
	function createInformationTraineeship(el, label, data, lastName = null, firstName = null) {
		var tr = $('<tr>');
		var tdLabel = $('<td class="' + classTable + '">' + label + '</td>');
		tr.append(tdLabel);
		var tdData = $('<td class="' + classTable + '">');
		
		// On affiche rien si aucune donnée est reçu
		if (data != null && data != '') {
			if (lastName == null && firstName == null) 
				tdData.html(data);
			else {
				// Si on a reçu un nom et un prénom cela signifie que data représente la civilité
				tdData.html(data + ' ' + lastName + ' ' + firstName); 
			}
			switch (data){
				case "Peu satisfaisant":
					tdData.css({"color":"red", "font-weight":"bold"});
					break;
				case "Satisfaisant":
					tdData.css({"color":"orange", "font-weight":"bold"});
					break;
				case "Très satisfaisant":
					tdData.css({"color":"green", "font-weight":"bold"});
					break;
			}
		}
		else {
			tdData.html("Aucune informations spécifiées");
		}
		tr.append(tdData);
		el.append(tr);
	}

	/**
	 * Crée une balise <textarea> qui contient le commentaire de l'élève
	 * @param string el			Il s'agit de l'élément html dans lequel on va ajouter notre balise
	 * @param string comment	Le commentaire de l'élève
	 */
	function createStudentComment(el, comment) {
		var tr = $('<tr>');
		var tdLabel = $('<td class="' + classTable + '">Commentaire du stagiaire : </td>');
		tr.append(tdLabel);
		
		var tdData = $('<td class="' + classTable + '">');
		var textArea = $('<textarea class="form-control ' + classTable + '" disabled>');

		if (comment != null && comment != '') {
			textArea.val(comment);
		}
		else {
			textArea.val("Le stagiaire n'a pas encore laissé de commentaire ou le commentaire n'a pas encore été validé.");
		}
		tdData.append(textArea);
		tr.append(tdData);
		el.append(tr);
	}
	
});
</script>
</html>