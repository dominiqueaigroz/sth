<!DOCTYPE html>
<?php
/*
 * Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017
 */
require_once './inc.view.php';
require_once '../Model/ESession.php';

if (ESession::getRole() === false){
	header('location: ./index.php');
}
else if ((ESession::getRole() !== EC_ROLE_ADMIN)&&(ESession::getRole() !== EC_ROLE_USER)){
	header('location: ./index.php');
}
?>
<html>
<head lang="fr">
<?php require_once './head.php'; ?>
<title>Offres de stages</title>
</head>
<body>
	<header class="cd-morph-dropdown">
		<?php
		include_once '../php/Nav/bar_nav.php';
		?>
		
	</header>
	<section id="maincontent" class="container-fluid">
			<h1>Offres de stages</h1>
			<section class="table-responsive">
				<table class="table filterTable">
					<tr>
						<td class="col-sm-4 col-lg-4">
							<section class="input-group stylish-input-group">
			                    <input id="search" type="text" class="form-control" placeholder="Search" >
			                    <span class="input-group-addon">
			                            <span class="fa fa-search"></span>
			                    </span>
		                	</section>
						</td>
						<td class="td-filter col-sm-1 col-lg-1"><button class="btn btn-danger btn-filter fa fa-filter" id="showAndHide" value="show"><span class="text-filter">Filtres</span></button></td>
						<td class="col-sm-7 col-lg-7">
							<section class="filters collapse">
								<button id="clearFilter" class="btn btn-warning btn-filter fa fa-refresh"><span class="text-filter">Réinitialiser les filtres</span></button>
								<button id="applyFilter" class="btn btn-success btn-filter fa fa-check"><span class="text-filter">Appliquer les filtres</span></button>
							</section>
						</td>					
					</tr>
				</table>
			</section>
			<section class="table-responsive filters collapse">
				<table class="table">
					<tr>
						<td id="tdDomains"></td>
						<td id="tdSectors"></td>
						<td><input type="datetime" class="form-control datepicker" id="dateBegin" placeholder="Disponible dès le :"></td>
						<td><input type="datetime" class="form-control datepicker" id="dateEnd" placeholder="jusqu'au :"></td>
						<td id="tdAvailable"></td>
					</tr>
				</table>
			</section>
			<section class="table-responsive">
				<table id="content" class="tablesorter table">
					<thead class="thead-inverse">
						<tr>
							<th>Entreprise</th>
							<th>Domaine</th>
							<th>Sujet du stage</th>
							<th>Secteur</th>
							<th>Durée</th>
							<th>Date de début</th>
							<th>Date de fin</th>
							<th>Disponibilité</th>
						</tr>
					</thead>
					<tbody id="traineeship-data">
					</tbody>
				</table>
			</section>
	</section>
	<?php 
		include_once './footer.html';
	?>
</body>
<script type="text/javascript">
$(document).ready(function(){
	var role = "<?php if (ESession::getRole() !== false){ echo ESession::getRole(); } ?>";
	var roleAdmin = "<?php echo EC_ROLE_ADMIN; ?>";
	
	if (role == roleAdmin){
		addTraineeshipLink();
	}
	
	function addTraineeshipLink(){
		var link = $('<a href="#"><span title="Ajouter un stage" class="fa fa-plus"></span></a>');
		link.click(function(event){
			event.preventDefault();
			$.redirect('./edit_traineeships.php');
		});
		$('.tablesorter').append(link);
	}
	
	var btnFilter = $('#applyFilter');
	var btnClearFilter = $('#clearFilter');
	var btnShowHide = $('#showAndHide');
	var inputSearch = $('#search');
	var elDomains = $('#tdDomains');
	var elSectors = $('#tdSectors');
	var elAvailable = $('#tdAvailable');
	var inputDateBeg = $('#dateBegin');
	var inputDateEnd = $('#dateEnd');
	var datePickers = $('.datepicker');

	var idSelectDomains = 'selectDomains';
	var idSelectSectors = 'selectSectors';
	var idSelectAvailable = 'selectAvailable';

	var classFilters = 'filters';
	var tableRows = '#traineeship-data > tr';

	var optionTextDomains = 'Afficher tout les domaines';
	var optionTextSectors = 'Afficher tout les secteurs';
	var optionTextAvailable = 'Afficher toutes les disponibilités';
	
	var colDomains = 2;
	var colSectors = 4;
	var colAvailable = 8;
	var colDateBeg = 6;
	var colDateEnd = 7;
	
	ELibrary.get_data('../Controller/get_traineeships.php', createTraineeshipsList);
	ELibrary.get_data('../Controller/get_domains.php', ELibrary.createSelect, undefined, elDomains, idSelectDomains, 'form-control ' + classFilters, optionTextDomains);
	ELibrary.get_data('../Controller/get_sectors.php', ELibrary.createSelect, undefined, elSectors, idSelectSectors, 'form-control ' + classFilters, optionTextSectors);
	ELibrary.get_data('../Controller/get_available.php', ELibrary.createSelect, undefined, elAvailable, idSelectAvailable, 'form-control ' + classFilters, optionTextAvailable, false);

	
	datePickers.datepicker({
		dateFormat: 'yy-mm-dd'	
	});
	
	// Filtrage du tableau 
	btnFilter.click(function() {

		var search = $.trim(inputSearch.val());
		if (search != "") {
			$(tableRows).remove(); // Suppression du contenu actuel du tableau

			ELibrary.get_data('../Controller/get_traineeships.php', proccessTraineeships, {'search': search});

			function proccessTraineeships(data) {
				createTraineeshipsList(data);
				// Il faut faire un filtre de la table une fois que le tableau ait été créé
				ELibrary.filterTable(idSelectDomains, tableRows, colDomains);
				ELibrary.filterTable(idSelectSectors, tableRows, colSectors, false);
				ELibrary.filterTable(idSelectAvailable, tableRows, colAvailable, false);
			}
		}

		ELibrary.filterTable(idSelectDomains, tableRows, colDomains);
		ELibrary.filterTable(idSelectSectors, tableRows, colSectors, false);
		ELibrary.filterTable(idSelectAvailable, tableRows, colAvailable, false);
		
		if (inputDateBeg.val() != '') {
			var dateBeg = ELibrary.formatDate(inputDateBeg.val());
			ELibrary.filterDate(dateBeg, true, tableRows, colDateBeg);
		}
		if (inputDateEnd.val() != '') {
			var dateEnd = ELibrary.formatDate(inputDateEnd.val());
			ELibrary.filterDate(dateEnd, false, tableRows, colDateEnd);
		}
	})

	// Effectue la recherche lorsqu'on appuie sur Enter
	inputSearch.keydown(function() {
		if(event.keyCode == 13) {
			btnFilter.click();
		}
	})
	
	// Réinitialisation des filtres + Réaffichage des offres
	btnClearFilter.click(function() {
		$('.' + classFilters).val(0);
		datePickers.val("");
		$(tableRows).remove(); // Suppression du contenu actuel du tableau
		ELibrary.get_data('../Controller/get_traineeships.php', createTraineeshipsList, undefined);
		inputSearch.val("");
	})

	// Affiche ou masque les filtres
	btnShowHide.click(function() {
		ELibrary.showAndHideFilters(btnShowHide, '.' + classFilters);
	})

	/**
	 * Construit un tableau qui contient les offres de stages
	 * @param JSON 		arData		Tableau JSON qui contient les offres de stages
	 */
	function createTraineeshipsList(arData) {			
		var el = $('#traineeship-data');

		arData.forEach(function(table){

			var tr = $('<tr>');
			
			var editEnter = $('<a name="' + table.enter.name + '" class="fa fa-pencil editEnter" id="' + table.enter.id + '" href="#"></a>');
			editEnter.click(function(event){
				event.preventDefault();
				var id = $(this).attr('id');
				var name = $(this).attr('name');
				$.redirect('./edit_enterprises.php',{id: id, enterName: name},"POST");
			});

			var tdName = $('<td>');
			var link = $('<a name="' + table.enter.name + '" class="traineeshipLinks" id="' + table.enter.id + '" href="#">');
			link.click(function(event){
				event.preventDefault();
				var id = $(this).attr('id');
				var name = $(this).attr('name');
				$.redirect('./description_enterprise.php',{id: id, enterName: name},"POST");
			});
			link.html(table.enter.name);
			if (role == roleAdmin)
				tdName.append(editEnter);
			tdName.append(link);
			tr.append(tdName);

			var tdDomain = $('<td>');
			tdDomain.html(table.enter.domain.name);
			tr.append(tdDomain);

			var editTrainee = $('<a class="fa fa-pencil editTraineeship" id="' + table.id + '" href="#"></a>');
			editTrainee.click(function(event){
				event.preventDefault();
				var id = $(this).attr('id');
				$.redirect('./edit_traineeships.php',{id: id},"POST");
			});
			
			var tdSubj = $('<td>');
			var link = $('<a name="' + table.subj + '" class="traineeshipLinks" id="' + table.id + '" href="#">');
			link.click(function(event){
				event.preventDefault();
				var id = $(this).attr('id');
				var name = $(this).attr('name');
				$.redirect('./description_traineeship.php',{id: id, traineeship: name},"POST");
			});
			link.html(table.subj);
			if (role == roleAdmin)
				tdSubj.append(editTrainee);
			tdSubj.append(link);
			tr.append(tdSubj);

			var tdSect = $('<td>');
			var tdSectHtml = 'Non définit';
			$indexSect = 0;
			table.sectors.forEach(function(sector){
				$indexSect++;
				if($indexSect > 1)
					tdSectHtml = tdSectHtml + ', ' +  sector.name;
				else 
					tdSectHtml = sector.name;	
			})
			tdSect.html(tdSectHtml);
			tr.append(tdSect);

			var tdDuration = $('<td>');
			tdDuration.html(table.duration);
			tr.append(tdDuration);

			var tdDateBeg = $('<td>');
			if (table.dateBeg != null)
				tdDateBeg.html(table.dateBeg);
			else
				tdDateBeg.html('Non définie');
			tr.append(tdDateBeg);

			var tdDateEnd = $('<td>')
			if (table.dateBeg != null)
				tdDateEnd.html(table.dateEnd);
			else
				tdDateEnd.html('Non définie');
			tr.append(tdDateEnd);

			var tdAvailab = $('<td>');
			tdAvailab.html(table.availab.label);
			tr.append(tdAvailab);
			
			el.append(tr)
			
			// Mise à jour du cache pour les tablesorter
			el.trigger("update");

		})
		
	}
	
});
</script>
</html>