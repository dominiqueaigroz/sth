<!DOCTYPE html>
<?php
/*
 * Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017
 */
require_once './inc.view.php';
require_once './popup/popupCancel.html';
require_once './popup/popupValidation.html';
require_once '../Model/inc.all.php';
if (ESession::getRole() === false){
	header('location: ./index.php');
}
else if (ESession::getRole() !== EC_ROLE_ADMIN){
	header('location: ./index.php');
}
?>
<html>
<head lang="fr">
<?php require_once './head.php'; ?>
<title>Édition d'un stage</title>
</head>
<body>
	<header class="cd-morph-dropdown">
			<?php
			include_once '../php/Nav/bar_nav.php';
			if(isset($_POST['id'])) 
				$idTraineeship = intval($_POST['id']);
			else
				$idTraineeship = -1;
			?>
	</header>
	<section id="maincontent" class="container-fluid">
		<h1>Edition d'un stage</h1>
		<fieldset class="largeTable">
			<legend>Information sur le stage</legend>
			<section class="table-responsive">
				<table class="table">
					<tr>
						<td><label>Entreprise* :</label></td>
						<td id="tdEnterprises" class="smallTableElementSM">
							<span class="error" id="errorEnterprises" style="display: none;"></span>
						</td>
						<td class="status"><label>Le stage est annulé : </label></td>
						<td class="status"><label for="radioCancel">Oui</label><input type="radio" id="radioCancel" name="status" value="1"></input></td>
						<td class="status"><label for="radioToCome">Non</label><input type="radio" id="radioToCome" name="status" value="2"></input></td>
					</tr>
					<tr>
						<td><label>Sujet du stage* :</label></td>
						<td class="form-group smallTableElementSM">
							<input type="text" class="form-control smallTableElementSM" name="subject" id="subject" placeholder="Sujet sur lequelle le stage porte...">
							<span class="error errorSubject" id="msgErrorSubject" style="display: none;"></span>
						</td>
					</tr>
					<tr>
						<td><label>Secteur* :</label></td>
						<td id="tdSectors" class="smallTableElementSM">
							<span class="error" id="errorSectors" style="display: none;">Ce champs est obligatoire !</span>
						</td>
						<td><label>Ajouter un nouveau secteur :</label></td>
						<td class="smallTableElementSM"><input class="form-control smallTableElementSM" type="text" name="new_sector"></td>
						<td><button class="btn btn-success" id="new_sector">Ajouter un secteur</button></td>
					</tr>
					<tr>
						<td><label>Commence le :</label></td>
						<td class="smallTableElementSM">
							<input type="datetime" class="datepicker form-control smallTableElementSM" id="dateBeg" name="dateBeg">
							<span class="error" id="errorDateBeg" style="display: none;">Ce champs est obligatoire !</span>
						</td>
						<td><label>et se termine le : </label></td>
						<td class="smallTableElementSM">
							<input type="datetime" class="datepicker form-control smallTableElementSM" id="dateEnd" name="dateEnd">
							<span class="error" id="errorDateEnd" style="display: none;">Ce champs est obligatoire !</span>
						</td>
					</tr>
					<tr>
						<td><label>Mission à accomplir* :</label></td>
						<td class="smallTableElementSM">
							<textarea class="form-control smallTableElementSM" id="mission" name="mission" placeholder="Description détaillé du stage sur le stage en lui-même et le secteur du stage."></textarea>
							<span class="error" id="msgErrorMission" style="display: none;"></span>
						</td>
					</tr>
					<tr>
						<td><label>Compétences et savoir-faire requis :</label></td>
						<td class="smallTableElementSM"><textarea name="competence" class="form-control smallTableElementSM" placeholder="Les compétences requises pour participer au stage."></textarea></td>
					</tr>
					<tr>
						<td><label>Indemnité* :</label></td>
						<td id="tdIndemnities" class="form-group">
							<span class="error" id="errorIndemnities" style="display: none;"></span>
						</td>
						<td><label>Disponible pour* :</label></td>
						<td id="tdAvailable" class="form-group">
							<span class="error" id="errorAvailable" style="display: none;"></span>
						</td>
					</tr>
					<tr>
						<td><label>Remarques :</label></td>
						<td class="smallTableElementSM"><textarea name="remark" class="form-control smallTableElementSM" placeholder="Diverses remarque qui peuvent être intéressentes à communiquer."></textarea></td>
					</tr>
				</table>
			</section>
		</fieldset>
		<fieldset class="smallTable">
			<legend>Information sur le responsable du stage</legend>
			<section class="table-responsive">
				<table id="" class="table">
					<tr>
						<td id="listChargeOf" class="smallTableElementSM">
							<span class="error errorSubject" id="errorInChargeOf" style="display: none;"></span>
						</td>
					</tr>
					<tr>
						<td><input type="radio" id="newInChargeOf" name="inChargeOf" value="newInChargeOf" checked><label for="newInChargeOf">Ou créer un nouveau responsable</label></td>
					</tr>			
					<tr>
						<td><label >État civil :</label></td>
						<td id="tdCivilities" class="smallTableElementSM"></td>
						<td><label>Titre :</label></td>
						<td class="smallTableElementSM"><input type="text" class="form-control smallTableElementSM chargeOf" name="title" placeholder="Titre du responsable au sein de l'entreprise"></td>
					</tr>
					<tr>
						<td><label>Nom :</label></td>
						<td class="smallTableElementSM"><input class="chargeOf form-control smallTableElementSM" type="text" id="lastName" name="last_name" placeholder="Nom"></td>
						<td><label>Prénom :</label></td>
						<td class="smallTableElementSM"><input class="chargeOf form-control smallTableElementSM" id="firstName" type="text" name="first_name" placeholder="Prénom"></td>
					</tr>
					<tr>
						<td><label>Email :</label></td>
						<td class="smallTableElementSM"><input class="chargeOf form-control smallTableElementSM" type="email" name="email"></td>
					</tr>
					<tr>
						<td><label>Téléphone de travail :</label></td>
						<td class="smallTableElementSM"><input class="chargeOf form-control smallTableElementSM" type="text" name="work_phone"></td>
						<td><label>Mobile personnel :</label></td>
						<td class="smallTableElementSM"><input class="chargeOf form-control smallTableElementSM" type="text" name="mobile_phone"></td>
					</tr>
				</table>
			</section>
		</fieldset>
		<div id="btnValidation">
			<button class="btn btn-form btn-primary buttonComment" id="cancel" data-toggle="modal" data-target="#popupCancel">Annuler</button>
			<button class="btn btn-form btn-primary buttonComment" id="valid"></button>
			<button style="display:none" id="btnPopupValid" data-toggle="modal" data-target="#popupValidation"></button>
		</div>
	</section>
	<?php 
		include_once './footer.html';
	?>
</body>
<script>

$(document).ready(function() {
	var idTraineeship = <?php echo $idTraineeship;?>; // Vaut false en cas de création, sinon vaut l'ID du stage en modification
	
	$('.datepicker').datepicker({
		dateFormat: 'yy-mm-dd'	
	});
	
	var elEnterprises = $('#tdEnterprises');
	var elSectors = $('#tdSectors');
	var elIndemnities = $('#tdIndemnities');
	var elAvailable = $('#tdAvailable');
	var elCivilities = $('#tdCivilities');

	var idSelectEnterprises = "enterprises";
	var idSelectSectors = "sectors";
	var idSelectIndemnities = "indemnities";
	var idSelectAvailable = "available";
	var idSelectCivilities = "civilities";
	var classSelect = "form-control smallTableElementSM";
	
	var optionTextEnterprises = "Sélectionner l'entreprise qui propose le stage";
	var optionTextSectors = "Sélectionner un ou plusieurs secteurs";
	var optionTextIndemnities = "Le stage est-il rémunéré ?";
	var optionTextAvailable = "Le stage est disponible pour ";
	var optionTextCivilities = "État civil";

	var popupCancelText = "";
	var popupValidText = "";
	
	ELibrary.get_data('../Controller/get_enterprises.php', ELibrary.createSelect, undefined, elEnterprises, idSelectEnterprises, classSelect, optionTextEnterprises, undefined, true);
	ELibrary.get_data('../Controller/get_sectors.php', ELibrary.createSelect, undefined, elSectors, idSelectSectors, '', optionTextSectors, undefined, undefined, idSelectSectors);
	ELibrary.get_data('../Controller/get_indemnities.php', ELibrary.createSelect, undefined, elIndemnities, idSelectIndemnities, classSelect, optionTextIndemnities, false, true);
	ELibrary.get_data('../Controller/get_available.php', ELibrary.createSelect, undefined, elAvailable, idSelectAvailable, classSelect, optionTextAvailable, false, true);
	ELibrary.get_data('../Controller/get_civilities.php', ELibrary.createSelect, undefined, elCivilities, idSelectCivilities, 'chargeOf ' + classSelect, "", false, true);
	
	// Est-ce qu'on a un id?
	if (idTraineeship > 0) {
		$('h1').text("Modification d'un stage");
		$('#valid').text("Modifier");
		
		popupCancelText = "Êtes-vous sûr de vouloir quitter la page ?</br>Toutes les modifications non enregistrées seront perdues.";
		popupValidText = "Confirmez-vous la modification des données du stage ?</br>Vous serez redirigé vers la page d'offres.";

		ELibrary.get_data('../Controller/get_inChargeOf.php', setSelectedInChargeOf, {'idTrain': idTraineeship});
		ELibrary.get_data('../Controller/get_traineeships.php', setDataTraineeshipForEdit, {'traineeship': idTraineeship});
	}
	else {			
		$('h1').text("Création d'un nouveau stage");
		$('#valid').text("Créer");
		$('#btnActivation').remove();
		$('.status').hide();
		
		popupCancelText = "Êtes-vous sûr de vouloir quitter la page ?</br>La création du stage n'aura pas lieu.";
		popupValidText = "Confirmez-vous la création du stage ?</br>Vous serez redirigé vers la page d'offres.";
		
		ELibrary.get_data('../Controller/get_inChargeOf.php', createInChargeOf);
	}

	
	// Ajout / Modification du stage
	$('#valid').click(function(e) {
		$('.error').attr('style', 'display: none;');
		
		var dataOk = true;
		var today = $.datepicker.formatDate('yy-mm-dd', new Date());
		
		var enterprise = $('select[name=' + idSelectEnterprises + '] option:selected').val();
		var subject = $.trim($('input[name=subject]').val());
		var sectors = $('#' + idSelectSectors).val(); // Récupère un tableau qui contient tout les secteurs sélectionnés
		var dateBeg = $('input[name=dateBeg]').val();
		dateBeg = $.datepicker.formatDate('yy-mm-dd', new Date(dateBeg));
		var dateEnd = $('input[name=dateEnd]').val();
		dateEnd = $.datepicker.formatDate('yy-mm-dd', new Date(dateEnd));
		var mission = $.trim($('textarea[name=mission]').val());
		var compet = $.trim($('textarea[name=competence]').val());
		var indemn = $('select[name=' + idSelectIndemnities + '] option:selected').val();
		var available = $('select[name=' + idSelectAvailable + '] option:selected').val();
		var remark = $.trim($('textarea[name=remark]').val());

		var civility = $('select[name=' + idSelectCivilities + '] option:selected').val();
		var title = $('input[name=title]').val();
		var lastName = $.trim($('input[name=last_name]').val());
		var firstName = $.trim($('input[name=first_name]').val());
		var email = $('input[name=email]').val();
		var workPhone = $('input[name=work_phone]').val();
		var mobilePhone = $('input[name=mobile_phone]').val();
		var inChargeOf;

		var status = $('input[name=status]:checked').val();

		// Si status n'existe pas, cela veut dire qu'on est en création
		if (status == undefined) 
			status = 2; // Status à venir
/* 
 
 # Ceci est la partie sécurité pour le responsable de stage. #

 // On assigne le stage a un responsable déjà existant
		if($('#inChargeOf').prop('checked')) {
			inChargeOf = $('select[name="inChargeOf"] option:selected').val();
			if (inChargeOf == undefined) {
				$('select[name="inChargeOf"]').addClass('has-error has-feedback');
				$('#errorInChargeOf').html('Sélectionner une personne !');
				$('#errorInChargeOf').attr('style', 'display: true;');
			}
			$('#' + idSelectCivilities).parent().removeClass('has-error has-feedback');
			$('input[name=last_name]').parent().removeClass('has-error has-feedback');
			$('input[name=first_name]').parent().removeClass('has-error has-feedback');
		}
		else {
			// On assigne le stage à un nouveau responsable
			if (civility == 0) {
				dataOk = false;
				$('#' + idSelectCivilities).parent().addClass('has-error has-feedback');
				$('#errorCivility').html('Ce champs est obligatoire !');
				$('#errorCivility').attr('style', 'display: true;');
			} 
			else {
				$('#' + idSelectCivilities).parent().removeClass('has-error has-feedback');
				$('#errorCivility').attr('style', 'display: none;');
			}
			if (lastName.length == 0) {
				dataOk = false;
				$('input[name=last_name]').parent().addClass('has-error has-feedback');
				$('#errorLastName').html('Ce champs est obligatoire !');
				$('#errorLastName').attr('style', 'display: true;');
			}
			else {
				$('input[name=last_name]').parent().removeClass('has-error has-feedback');
				$('#errorLastName').attr('style', 'display: none;');
			}
			if (firstName.length == 0) {
				dataOk = false;
				$('input[name=first_name]').parent().addClass('has-error has-feedback');
				$('#errorFirstName').html('Ce champs est obligatoire !');
				$('#errorFirstName').attr('style', 'display: true;');
			}
			else {
				$('input[name=first_name]').parent().removeClass('has-error has-feedback');
				$('#errorFirstName').attr('style', 'display: none;');
			}
		}
*/			
		if(sectors == 0 || sectors == undefined) {
			dataOk = false;
			$(elSectors).addClass('has-error has-feedback');
			$('#errorSectors').html('Ce champs est obligatoire !');
			$('#errorSectors').attr('style', 'display: true;');
		}
		else {
			$(elSectors).removeClass('has-error has-feedback');
			$('#errorSectors').attr('style', 'display: none;');
		}
		if (enterprise == 0) {
			dataOk = false;
			$('#' + idSelectEnterprises).parent().addClass('has-error has-feedback');
			$('#errorEnterprises').html('Ce champs est obligatoire !');
			$('#errorEnterprises').attr('style', 'display: true;');
		}
		else{
			$('#' + idSelectEnterprises).parent().removeClass('has-error has-feedback');
			$('#errorEnterprises').attr('style', 'display: none;');
		}
		if (indemn  == 0) {
			dataOk = false;
			$('#' + idSelectIndemnities).parent().addClass('has-error has-feedback');
			$('#errorIndemnities').html('Ce champs est obligatoire !');
			$('#errorIndemnities').attr('style', 'display: true;');
		}		
		else{
			$('#' + idSelectIndemnities).parent().removeClass('has-error has-feedback');
			$('#errorIndemnities').attr('style', 'display: none;');
		}
		if (available == 0) {
			dataOk = false;
			$('#' + idSelectAvailable).parent().addClass('has-error has-feedback');
			$('#errorAvailable').html('Ce champs est obligatoire !');
			$('#errorAvailable').attr('style', 'display: true;');
		}
		else{
			$('#' + idSelectAvailable).parent().removeClass('has-error has-feedback');
			$('#errorAvailable').attr('style', 'display: none;');
		}

		if (subject.length < 5 || subject.length > 45) {
			dataOk = false;
			$('#subject').parent().addClass('has-error has-feedback');
			$('#msgErrorSubject').html('Le sujet doit être compris entre 5 et 45 caractères !');
			$('#msgErrorSubject').attr('style', 'display: true;');
		}
		else{
			$('#subject').parent().removeClass('has-error has-feedback');
			$('#msgErrorSubject').attr('style', 'display: none;');
			$('#errorSubject').attr('style', 'display: none;');
		}
		if (mission.length < 10) {
			dataOk = false;
			$('#mission').parent().addClass('has-error has-feedback');
			$('#msgErrorMission').html('La mission doit faire minimum 10 caractères !');
			$('#msgErrorMission').attr('style', 'display: true;');
		}
		else{
			$('#mission').parent().removeClass('has-error has-feedback');
			$('#msgErrorMission').attr('style', 'display: none;');
			$('#msgErrorMission').attr('style', 'display: none;');
		}

		if (dateBeg == "NaN-NaN-NaN") {
			dataOk = false;
			$('#dateBeg').parent().addClass('has-error has-feedback');
			$('#errorDateBeg').html('La date de début ne peux pas être vide !');
			$('#errorDateBeg').attr('style', 'display: true;');
		}
		else{
			$('#dateBeg').parent().removeClass('has-error has-feedback');
			$('#errorDateBeg').attr('style', 'display: none;');
			$('#errorDateBeg').attr('style', 'display: none;');
			if (dateBeg < today) {
				dataOk = false;
				$('#dateBeg').parent().addClass('has-error has-feedback');
				$('#errorDateBeg').html('La date de début ne peux pas être plus petite que la date d\'aujourd\'hui !');
				$('#errorDateBeg').attr('style', 'display: true;');
			}
			if (dateBeg == today) {
				dataOk = false;
				$('#dateBeg').parent().addClass('has-error has-feedback');
				$('#errorDateBeg').html('La date de début ne peux pas être égale à la date d\'aujourd\'hui !');
				$('#errorDateBeg').attr('style', 'display: true;');
			}
			if (dateBeg > dateEnd) {
				dataOk = false;
				$('#dateBeg').parent().addClass('has-error has-feedback');
				$('#errorDateBeg').html('La date de début ne peux pas être plus grande que la date de fin !');
				$('#errorDateBeg').attr('style', 'display: true;');
			}
			if (dateBeg == dateEnd) {
				dataOk = false;
				$('#dateBeg').parent().addClass('has-error has-feedback');
				$('#errorDateBeg').html('La date de début ne peux pas être égale à la date de fin !');
				$('#errorDateBeg').attr('style', 'display: true;');
			}
		}
		
		if (dateEnd == "NaN-NaN-NaN"){
			dataOk = false;
			$('#dateEnd').parent().addClass('has-error has-feedback');
			$('#errorDateEnd').html('La date de fin ne peux pas être vide !');
			$('#errorDateEnd').attr('style', 'display: true;');
		}
		else{
			$('#dateEnd').parent().removeClass('has-error has-feedback');
			$('#errorDateEnd').attr('style', 'display: none;');
			$('#errorDateEnd').attr('style', 'display: none;');
			if (dateEnd < today){
				dataOk = false;
				$('#dateEnd').parent().addClass('has-error has-feedback');
				$('#errorDateEnd').html('La date de fin ne peux pas être plus petite que la date d\'aujourd\'hui !');
				$('#errorDateEnd').attr('style', 'display: true;');
			}
			if (dateEnd == today) {
				dataOk = false;
				$('#dateEnd').parent().addClass('has-error has-feedback');
				$('#errorDateEnd').html('La date de fin ne peux pas être égale à la date d\'aujourd\'hui !');
				$('#errorDateEnd').attr('style', 'display: true;');
			}
			if (dateBeg == dateEnd) {
				dataOk = false;
				$('#dateEnd').parent().addClass('has-error has-feedback');
				$('#errorDateEnd').html('La date de fin ne peux pas être égale à la date de début !');
				$('#errorDateEnd').attr('style', 'display: true;');
			}
		}

		// TO DO : Format de l'email et des téléphones
			
		if (dataOk) {
			$('#btnPopupValid').click();
		}
	});

	// Affiche une popup de confirmation pour l'ajout ou la modification d'une entreprise
	$('#btnPopupValid').click(function(){
		$('#validationToShow').html(popupValidText);
	});

	// Création ou modification d'une entreprise
	$('#btnAcceptValidation').click(function(){

		var enterprise = $('select[name=' + idSelectEnterprises + '] option:selected').val();
		var subject = $.trim($('input[name=subject]').val());
		var sectors = $('#' + idSelectSectors).val(); // Récupère un tableau qui contient tout les secteurs sélectionnés
		var dateBeg = $('input[name=dateBeg]').val();
		dateBeg = $.datepicker.formatDate('yy-mm-dd', new Date(dateBeg));
		var dateEnd = $('input[name=dateEnd]').val();
		dateEnd = $.datepicker.formatDate('yy-mm-dd', new Date(dateEnd));
		var mission = $.trim($('textarea[name=mission]').val());
		var compet = $.trim($('textarea[name=competence]').val());
		var indemn = $('select[name=' + idSelectIndemnities + '] option:selected').val();
		var available = $('select[name=' + idSelectAvailable + '] option:selected').val();
		var remark = $.trim($('textarea[name=remark]').val());

		var civility = $('select[name=' + idSelectCivilities + '] option:selected').val();
		var title = $('input[name=title]').val();
		var lastName = $.trim($('input[name=last_name]').val());
		var firstName = $.trim($('input[name=first_name]').val());
		var email = $('input[name=email]').val();
		var workPhone = $('input[name=work_phone]').val();
		var mobilePhone = $('input[name=mobile_phone]').val();
		var inChargeOf = $('select[name=inChargeOf] option:selected').val();
		if (inChargeOf == undefined) 
			inChargeOf = -1;
		
		var status = $('input[name=status]:checked').val();
		// Si status n'existe pas, cela veut dire qu'on est en création
		if (status == undefined) 
			status = 2; // Status à venir
		
    	$.ajax({
		    method: 'POST',
		    url: '../Controller/set_traineeships.php',
		    data: {
			    'enterprise': enterprise, 'subject': subject, 'sectors': sectors, 'dateBeg': dateBeg, 'dateEnd': dateEnd,
			    'mission': mission, 'compet': compet, 'indemn': indemn, 'available': available, 'remark': remark,
			    'civility': civility, 'title': title, 'lastName': lastName, 'firstName': firstName, 'email': email,
			    'workPhone': workPhone, 'mobilePhone': mobilePhone, 'inChargeOf': inChargeOf, 'traineeship': idTraineeship,
			    'status': status
			},
    	});
		// Redirection sur la page d'offre de stage après la création/modification d'une entreprise
		$.redirect('./offers.php');
		
	    // TO DO : Afficher un message qui indique que le stage a été correctement créé/modifié.
	});

	// Ajout d'un nouveau secteur
	$('#new_sector').click(function() {
		var sector = $.trim($('input[name=new_sector]').val());
		
		if (sector != "" && sector.length >= 3) {
			ELibrary.get_data('../Controller/set_sectors.php', add_sector, {'sector': sector});
			
		}
	});

	// Affichage d'une popup de confirmation pour l'annulation de la création ou modification d'un stage
	$('#cancel').click(function(event){
		event.preventDefault();
		$('#cancelToShow').html(popupCancelText);
		
	});
	
	// Redirection sur la page d'offre de stage
	$('#btnAcceptCancel').click(function(){
		$.redirect('./offers.php');
	});

	/**
	 * Affiche une liste déroulante qui contient tous les responsable de stage
	 * @param JSON arData				Tableau JSON qui contient les responsable de stage
	 * @param string chargeOfId			L'ID du responsable de stage (si aucun responsable n'est assigné au stage alors chargeOfId vaudra false)
	 */
	function createInChargeOf(arData, chargeOfId = false) {
		var el = $('#listChargeOf');
		var radio = $('<input type="radio" id="inChargeOf" name="inChargeOf" value="list"><label for="inChargeOf">Sélectionner un responsable existant</label></input>');
		el.append(radio);
		var section = $('<section>');
		var sel = $('<select name="inChargeOf" class="form-control smallTableElementSM">');
		sel.attr('size', 10);
		arData.forEach(function(inChargeOf){
			var op = $('<option value="' + inChargeOf.id + '">' + inChargeOf.civility + ' ' + inChargeOf.firstName + ' ' + inChargeOf.lastName + '</option>');
			sel.append(op);
		})
		section.append(sel);
		el.append(section);

		// Sélectionne automatiquement le responsable du stage dans la liste si il est assigné au stage
		if (chargeOfId != false) {
			$('#inChargeOf').attr('checked', true);
			$('select[name=inChargeOf] option[value="' + chargeOfId + '"]').prop('selected', true);
		}
		else {
			$('#newInChargeOf').attr('checked', true);
		}
		disabledElement();

		// (Dés)active les champs lorsque l'on change le radio 
		$('input[name="inChargeOf"]').change(function() {
			disabledElement();
		});

		function disabledElement() {
			var newInChargeOf = $('#newInChargeOf');
			if(newInChargeOf.prop('checked')) {
				$('select[name="inChargeOf"]').prop('disabled', true);
				$('select[name="inChargeOf"] option:selected').prop('selected', false); // On déselectionne le responsable dans la liste
				$('.chargeOf').prop('disabled', false);
			}
			else {
				$('select[name="inChargeOf"]').prop('disabled', false);
				$('.chargeOf').prop('disabled', true);
			};
		}

	}

	/**
	 * Remplis les différents champs de la page d'édition de stage avec les données provenant de la base
	 * @param JSON arData	Tableau JSON qui contient les données du stage
	 */
	 function setDataTraineeshipForEdit(arData) {

		$('#' + idSelectEnterprises).val(arData.enter.id);
		$('input[name=subject]').val(arData.subj);
		$('textarea[name=mission]').val(arData.mission);
		$('textarea[name=competence]').val(arData.compet);
		$('#' + idSelectIndemnities).val(arData.indemn);
		$('#' + idSelectAvailable).val(arData.availab.code);
		$('textarea[name=remark]').val(arData.remark);

		if (arData.dateBeg != null) {
			dateBeg = $.datepicker.formatDate('yy-mm-dd', new Date(arData.dateBeg));
			$('input[name=dateBeg]').val(dateBeg);
		}
		if (arData.dateEnd != null) {
			dateEnd = $.datepicker.formatDate('yy-mm-dd', new Date(arData.dateEnd));
			$('input[name=dateEnd]').val(dateEnd);
		}
		
		if(arData.status == "Annulé") {
			$('#radioCancel').attr('checked', true);
		}
		else {
			$('#radioToCome').attr('checked', true);
		}
		
		var arSectors = arData.sectors;
		var sectors = [];
		arSectors.forEach(function(sector) {
			// Coche et "active" l'option qui correspond au secteur
			sectors.push(sector.id);
			var input = $('li > a > label > input[value="' + sector.id + '"]');
			input.prop('checked', true);
			input.closest('li').attr('class', 'active');

		})
		// Ajoute les secteurs précédemment stockées dans le multiselect
		$('#sectors').val(sectors);
		// Actualise le texte du multiselect
		$('#sectors').multiselect('updateButtonText');

		if(arData.chargeOf != undefined && arData.chargeOf != null) {
			ELibrary.get_data('../Controller/get_inChargeOf.php', createInChargeOf, undefined, arData.chargeOf.id);
		}
		
	}
	
	/**
	 * Ajoute un secteur au multiselect
	 * @param arSector	Tableau JSON qui contient le secteur
	 */
	function add_sector(arSector) {
		// Création de l'option
		var op = $('<option value="' + arSector.id + '">' + arSector.name + '</option>');
		$('#' + idSelectSectors).append(op);
		
		// Création du li
		var label = $('<label class="checkbox"><input type="checkbox" value="' + arSector.id + '">' + arSector.name + '</label>');			
		var a = $('<a tabindex="0">');
		var li = $('<li class="active">');
		a.append(label);
		li.append(a);
		$('.multiselect-container.dropdown-menu').append(li);
		
		// Coche la case à cocher
		var input = $('li > a > label > input[value="' + arSector.id + '"]');
		input.prop('checked', true);
		
		// Ajoute le nouveau secteur dans la value du multiselect
		var sectors = $('#' + idSelectSectors).val();
		if (sectors == null) 
			sectors = [];
		sectors.push(arSector.id);
		$('#' + idSelectSectors).val(sectors);
		
		// Actualise le texte du multiselect
		$('#' + idSelectSectors).multiselect('updateButtonText');
		
		// Création d'un event change pour le nouveau secteur
		input.change(function() {
			var checked = input.prop('checked');
			var opSelect = $('#' + idSelectSectors + ' > option[value=' + arSector.id + ']');
			
			if (checked) {
				input.closest('li').addClass('active');	
				opSelect.prop('selected', true);
			}
			else {
				input.closest('li').removeClass('active');
				opSelect.prop('selected', false);
			}
			
			// Actualise le texte du multiselect
			$('#' + idSelectSectors).multiselect('updateButtonText');
	
		});
		
		$('input[name=new_sector]').val('');
	}

	/**
	 * Sélectionner par défaut le responsable du stage dans la liste
	 * @param int idInChargeOf	L'ID du responsable du stage
	 */
	function setSelectedInChargeOf (idInChargeOf) {
		var op = $('#listChargeOf > div > select > option[value="' + idInChargeOf + '"]');
		op.prop('selected', 'selected');
	}
	
});
</script>
</html>