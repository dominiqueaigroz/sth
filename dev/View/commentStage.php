<!DOCTYPE html>
<?php
/*
 * Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017
 */
require_once './inc.view.php';
require_once './popup/popupCancel.html';
require_once './popup/popupValidation.html';
if (ESession::getRole() === false){
	header('location: ./index.php');
}
else if (ESession::getRole() !== EC_ROLE_USER){
	header('location: ./index.php');
}
?>
<html>
<head lang="fr">
<?php require_once './head.php'; ?>
<title>Avis sur le stage</title>
</head>
<body>
	<header class="cd-morph-dropdown">
		<?php
		include_once '../php/Nav/bar_nav.php';
		?>
	</header>
	<h1></h1>
	<section id="maincontent" class="container-fluid">
	<h1></h1>
		<section class="table-responsive">
			<table id="content" class="table">
				<thead id="contentthead" class=thead-inverse></thead>
				<tbody id="contenttbody"></tbody>
			</table>
			<table class="table">
				<tr>
					<td>Commentaire sur le stage : </td>
					<td><textarea title="le commentaire doit être constructif et de minimum 20 caractères" class="comment" name="comment"></textarea></td>	
				</tr>
			</table>
		</section>
		<button class="btn btn-danger buttonComment" id="cancel" data-toggle="modal" data-target="#popupCancel">Retour</button>
		<button class="btn btn-primary buttonComment" id="send">Envoyer</button>
		<button style="display:none" id="btnPopupValid" data-toggle="modal" data-target="#popupValidation"></button>
	</section>
		<?php 
			include_once './footer.html';
		?>
</body>
<script type="text/javascript">
$(document).ready(function() {

	var idTraineeship = "<?php echo $_POST['id']; ?>";
	var nbCrit = "<?php echo EC_NB_CRITERIA;?>";
	var nbMinComm = "<?php echo EC_NB_MIN_COMM?>";


	ELibrary.get_data('../Controller/get_traineeships.php', setH1Subject, {'traineeship': idTraineeship});

	function setH1Subject(data) {
		$('h1').html("Avis sur le stage : " + data.subj);
	}

	ELibrary.get_data('../Controller/get_satisfactions.php', processOpinions, {'idTraineeship': idTraineeship});
	
	/**
	 * Appel la fonction de création des opinions
	 * @param JSON	data	Tableau JSON qui contient les donnèes des opinions
	 */
	function processOpinions(data) {
		createOpinions(data.DataSatisfaction, data.DataCriteria, data.DataSatisfForCrit);
	}

	/**
	 * Construit un tableau avec les satisfactions, les critères de satisfactions et des radiobuttons
	 * @param JSON arSatisfaction		Tableau JSON qui contient les satisfactions
	 * @param JSON arCriteria			Tableau JSON qui contient les critères
	 * @param JSON arSatisfForCrit		Tableau JSON qui contient les satisfactions pour les critères du stage déjà stockées dans la base (si il y en a)
	 */
	function createOpinions(arSatisfactions, arCriteria, arSatisfForCrit) {
		// En-tête du tableau
		var thead = $('#contentthead');
		var tr = $('<tr>');
		var thFirst = $('<th>');

		thFirst.html('Liste des critères')
		tr.append(thFirst);

		// Création des colonnes de satisfactions
		arSatisfactions.forEach(function(table){

			var th = $('<th>');
			th.html(table.label);
			tr.append(th);

		}) // End forEach arSatisfactions
		thead.append(tr);

		// Corps du tableau
		var tbody = $('#contenttbody');
		var idCrit = 0;

		// Créations des lignes de critères
		arCriteria.forEach(function(tableCriteria){

			tr = $('<tr>');
			var tdLabel = $('<td>');
			tdLabel.html(tableCriteria.label);
			tr.append(tdLabel);

			// Création des radiobutton correspondant aux satisfactions
			arSatisfactions.forEach(function(tableSatisfactions) {
				var tdRadio = $('<td>');


				// Si des données sont déjà stockées dans la base et si la satisfaction correspond, alors on coche le radiobutton
				if(arSatisfForCrit != false && arSatisfForCrit[idCrit].idSatisf == tableSatisfactions.code)
					var inputRadio = $('<input type="radio" checked>');
				else
					var inputRadio = $('<input type="radio">');

				tdRadio.val(tableSatisfactions.label);
				inputRadio.attr('name', 'criterion' + tableCriteria.code);
				inputRadio.val(tableSatisfactions.code);
				tdRadio.append(inputRadio);
				tr.append(tdRadio);


				if(arSatisfForCrit != false) {
					inputRadio.prop("disabled", true);
				}
				else {
					$(tdRadio).click(function(){
						$(tdRadio).children().prop("checked", true);
					});
				}

				
			}) // End forEach arSatisfactions

			tbody.append(tr);
			idCrit++;
		}) // End forEach arCriteria

	}

	ELibrary.get_data('../Controller/get_comments.php', setComment, {'idTraineeship': idTraineeship});
	
	/**
	 * Met une valeur par défaut à la balise <textarea> pour le commentaire
	 * @param string comm	Le commentaire à définir
	 */
	function setComment(comm) {
		$('textarea[name=comment]').val(comm.comment);
	}

	// Envoyer de l'avis du stage (satisfactions + commentaire)
	$('#send').click(function() {
		var comment = $('textarea[name=comment]').val();
		
		var arCrit = [];
		var critOK = true; // Si cette variable passe a false, cela voudra dire qu'un des critères n'a pas été sélectionné
		
		for (codeCrit = 1; codeCrit <= nbCrit; codeCrit++) {
			var crit = $('input[name=criterion' + codeCrit + ']:checked').val();
			if (typeof crit == 'undefined') {
				critOK = false;
				break;
			}
			else {
				arCrit.push(crit);
			}
		}
		// Si tout les critères ont des satisfactions, alors on peut faire l'appel ajax
		if (critOK) {
			$('#btnPopupValid').click();
		}
	});

	// Affiche une popup de confirmation pour l'ajout ou la modification d'une entreprise
	$('#btnPopupValid').click(function(){
		$('#validationToShow').html("Envoyer l'avis du stage ?<br>Elle sera visible sur la page de description du stage (le commentaire devra être vérifier avant d'être visbile)<br>Vous ne pourrez pas modifier le niveau de satisfaction pour les différents critères mais vous pourrez modifier votre commentaire si il a été refusé par un administrateur.");
	});

	// Création ou modification d'une entreprise
	$('#btnAcceptValidation').click(function(){

		var comment = $('textarea[name=comment]').val();
		
		var arCrit = [];
		
		for (codeCrit = 1; codeCrit <= nbCrit; codeCrit++) {
			var crit = $('input[name=criterion' + codeCrit + ']:checked').val();
			arCrit.push(crit);	
		}
		
		var params = {
			method: 'POST',
			url: '../Controller/set_opinions.php',
		};
		
		if (comment.length >= nbMinComm) 
			params.data = {'idTraineeship': idTraineeship, 'criteria': arCrit, 'comment': comment};
		else 
			params.data = {'idTraineeship': idTraineeship, 'criteria' : arCrit};
		
		$.ajax(params);
		
		$.redirect('./userStage.php');
	});

	// Affichage d'une popup de confirmation pour l'annulation du commentaire
	$('#cancel').click(function(){
		$('#cancelToShow').html("Êtes-vous sûr de vouloir quitter la page ?<br>Toutes les modifications non-enregistrées seront perdues !");
	});

	// Redirection sur la page des stages de l'élève
	$('#btnAcceptCancel').click(function(){
		$.redirect('./userStage.php');
	});
});
</script>
</html>