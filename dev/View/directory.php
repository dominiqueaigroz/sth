<!DOCTYPE html>
<?php
/*
 * Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017
 */
require_once '../Model/inc.all.php';
require_once './inc.view.php';
require_once '../Model/ESession.php';

if (ESession::getRole() === false){
	header('location: ./index.php');
}
else if ((ESession::getRole() !== EC_ROLE_ADMIN)&&(ESession::getRole() !== EC_ROLE_USER)){
	header('location: ./index.php');
}

?>
<html>
	<head lang="fr">
		<?php require_once './head.php'; ?>
		<title>Annuaire des entreprises</title>
	</head>
	<body>
		<header class="cd-morph-dropdown">
			<?php
			include_once '../php/Nav/bar_nav.php';
			?>
		</header>
		<section id="maincontent" class="container-fluid">
			<h1>Annuaire des entreprises</h1>
			<section class="table-responsive">
				<table class="table">
					<tr>
						<td class="col-sm-4 col-lg-4">
							<section class="input-group stylish-input-group">
			                    <input id="search" type="text" class="form-control" placeholder="Search" >
			                    <span class="input-group-addon">
			                            <span class="fa fa-search"></span> 
			                    </span>
		                	</section>
						</td>
						<td class="td-filter col-sm-1 col-lg-1"><button class="btn btn-danger btn-filter fa fa-filter" id="showAndHide" value="show"><span class="text-filter">Filtres</span></button></td>
						<td class="col-sm-7 col-lg-7">
							<section class="filters collapse">
								<button id="clearFilter" class="btn btn-warning btn-filter fa fa-refresh"><span class="text-filter">Réinitialiser les filtres</span></button>
								<button id="applyFilter" class="btn btn-success btn-filter fa fa-check"><span class="text-filter">Appliquer les filtres</span></button>
							</section>
						</td>					
					</tr>
				</table>
			</section>
			<section class="table-responsive filters collapse">
				<table class="table">
					<tr>
						<td id="tdDomains" class="smallTableElementSM"></td>
					</tr>
				</table>
			</section>	
			<section class="table-responsive">
				<table id="content" class="tablesorter table">
					<thead class="thead-inverse">
						<tr>
							<th>Entreprise</th>
							<th>Domaine</th>
							<th>Téléphone</th>
							<th>Site Web</th>
							<th>Adresse</th>
						</tr>
					</thead>
					<tbody id="enterprise-data">
					</tbody>
				</table>
			</section>
		</section>
		<?php 
			include_once './footer.html';
		?>
	</body>
<script type="text/javascript">
$(document).ready(function() {
	var role = "<?php if (ESession::getRole() !== false){ echo ESession::getRole(); } ?>";
	var roleAdmin = "<?php echo EC_ROLE_ADMIN; ?>";
	
	if (role == roleAdmin){
		addEnterLink();
	}
	
	function addEnterLink(){
		var link = $('<a href="#"><span title="Ajouter une entreprise" class="fa fa-plus"></span></a>');
		link.click(function(event){
			event.preventDefault();
			$.redirect('./edit_enterprises.php');
		});
		$('.tablesorter').append(link);
	}
	
	var btnFilter = $('#applyFilter');
	var btnClearFilter = $('#clearFilter');
	var btnShowHide = $('#showAndHide');
	var inputSearch = $('#search');
	var elDomains = $('#tdDomains');
	
	var idSelectDomains = 'selectDomains';
	var classFilters = 'filters';
	var optionTextDomains = 'Afficher tout les domaines';
	var tableRows = '#enterprise-data > tr';
	var colDomains = 2;
	
	ELibrary.get_data('../Controller/get_enterprises.php', createEnterpriseList, undefined, role);
	ELibrary.get_data('../Controller/get_domains.php', ELibrary.createSelect, undefined, elDomains, idSelectDomains, 'form-control ' + classFilters, optionTextDomains);
	
	
	// Filtrage du tableau 
	btnFilter.click(function() {
	
		var search = $.trim(inputSearch.val());
		if (search != "") {
			$(tableRows).remove(); // Suppression du contenu actuel du tableau
			
			ELibrary.get_data('../Controller/get_enterprises.php', proccessEnterprise, {'searchName': search});
			
			function proccessEnterprise(data) {
				createEnterpriseList(data, role);
				ELibrary.filterTable(idSelectDomains, tableRows, colDomains); // Il faut faire un filtre de la table une fois que le tableau ait été créé
			}
		}
		ELibrary.filterTable(idSelectDomains, tableRows, colDomains);
	})
	
	// Effectue la recherche lorsqu'on appuie sur Enter
	inputSearch.keydown(function() {
		if(event.keyCode == 13) {
			btnFilter.click();
		}
	})
	
	// Réafficher toutes les entreprises
	btnClearFilter.click(function() {
		$(tableRows).remove(); // Suppression du contenu actuel du tableau
		ELibrary.get_data('../Controller/get_enterprises.php', createEnterpriseList, undefined, role);
		inputSearch.val("");
		$('#' + idSelectDomains + ' option[value="0"]').prop('selected', true);
	})
	
	// Affiche ou masque les filtres
	btnShowHide.click(function() {
		ELibrary.showAndHideFilters(btnShowHide, '.' + classFilters);
	})

	/**
	 * Construit un tableau qui contient la liste des entreprises
	 * @param JSON 		arData		Tableau JSON qui contient les données à afficher
	 * @param int		userRole	Le role de l'utilisateur (élève ou admin)
	 */
	function createEnterpriseList(arData, userRole) {
		var el = $('#enterprise-data');

		arData.forEach(function(table){
			var tr = $('<tr>');
			
			var tdName = $('<td class="">');
			
			var edit = $('<a name="' + table.name + '" class="fa fa-pencil editEnter" id="' + table.id + '" href="#"></a>');
			edit.click(function(event){
				event.preventDefault();
				var id = $(this).attr('id');
				var name = $(this).attr('name');
				$.redirect('./edit_enterprises.php',{id: id, enterName: name},"POST");
			});
			
			var link = $('<a name="' + table.name + '" class="enterLinks" id="' + table.id + '" href="#">');
			link.click(function(event){
				event.preventDefault();
				var id = $(this).attr('id');
				var name = $(this).attr('name');
				$.redirect('./description_enterprise.php',{id: id, enterName: name},"POST");
			});
			link.html(table.name);
			if (userRole == roleAdmin)
				tdName.append(edit);
			tdName.append(link);
			tr.append(tdName);

			var tdDomain = $('<td>');
			tdDomain.html(table.domain.name);
			tr.append(tdDomain);

			var tdPhone = $('<td>');
			tdPhone.html(table.phone);
			tr.append(tdPhone);

			var tdWebsite = $('<td>');
			tdWebsite.html(table.web);
			tr.append(tdWebsite);

			var tdAddress = $('<td>');
			var street = table.street;
			if (street == null)
				street = "";
			var zip = table.zip;
			if (zip == null)
				zip = "";
			var city = table.city;
			if (city == null)
				city = "";
			var address = $.trim(street + " " + zip +" " + city);
			tdAddress.html(address);
			tr.append(tdAddress);
			
			el.append(tr)
			
			// Mise à jour du cache pour les tablesorter
			el.trigger("update");
		}) // End forEach
	}
	
});
</script>
</html>