<!DOCTYPE html>
<?php
/*
 * Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017
 */

require_once '../Model/inc.all.php';
require_once './inc.view.php';
require_once './popup/popupCancel.html';
require_once './popup/popupValidation.html';
require_once './popup/popupDelete.html';
if (ESession::getRole() === false) {
	header ( 'location: ./index.php' );
} else if (ESession::getRole()  !== EC_ROLE_ADMIN) {
	header ( 'location: ./index.php' );
}
?>
<html>
<head lang="fr">
		<?php require_once './head.php'; ?>
		<title>Édition d'une entreprise</title>
</head>
<body>
	<header class="cd-morph-dropdown">
		<?php
			include_once '../php/Nav/bar_nav.php';
			if (isset ( $_POST ['id'] ))
				$modif = $_POST ['id'];
			else
				$modif = false;
			
			if (isset ( $_POST ['enterName'] ))
				$enterName = $_POST ['enterName'];
			else
				$enterName = false;
		?>
	</header>
	<section id="maincontent" class="container-fluid">
		<h1>Edition d'une entreprise</h1>
		<section class="table-responsive">
			<table class="table largeTable">
				<tr>
					<td><label>Nom de l'entreprise* :</label></td>
					<td class="form-group smallTableElementLG">
						<input id="enterprise" type="text" class="form-control smallTableElementLG" name="enterprise" placeholder="Nom de l'entreprise">
						<span class="fa fa-times form-control-feedback error" style="display: none;" aria-hidden="true"></span>
						<span class="error" id="errorEnterprise" style="display: none;"></span>
					</td>
				</tr>
				<tr>
					<td><label>Description :</label></td>
					<td><textarea class="form-control comment" name="description" placeholder="Description de l'entreprise"></textarea></td>
				</tr>
				<tr>
					<td><label>Domaine :</label></td>
					<td id="tdDomains"></td>
					<td><label>Ajouter un nouveau domaine :</label></td>
					<td><input class="form-control smallTableElementSM" type="text" name="new_domain"></td>
					<td><button class="btn btn-success" id="new_domain">Ajouter un domaine</button></td>
				</tr>
				<tr>
					<td><label>Pays :</label></td>
					<td id="tdCountries"></td>
				</tr>
				<tr>
					<td><label>Quartier :</label></td>
					<td><input class="form-control smallTableElementSM" type="text" name="city" placeholder="Ex: Carouge"></td>
				</tr>
				<tr>
					<td><label>Rue :</label></td>
					<td><input class="form-control smallTableElementSM" type="text" name="street" placeholder="Ex: 51 Rue Jacques-Dalphin"></td>
				</tr>
				<tr>
					<td><label>NPA :</label></td>
					<td><input class="form-control smallTableElementSM" type="text" name="npa" placeholder="Ex: 1212"></td>
				</tr>
				<tr>
					<td><label>Téléphone :</label></td>
					<td><input class="form-control smallTableElementSM" type="text" name="phone" placeholder="Ex: +41 22 297 25 66"></td>
				</tr>
				<tr>
					<td><label>Site web :</label></td>
					<td><input class="form-control smallTableElementSM" type="text" name="web" placeholder="Ex: www.exemple.com"></td>
				</tr>
			</table>
		</section>
		<section id="btnDel"></section>
		<section id="btnValidation">
			<button class="btn btn-form btn-primary buttonComment" style="margin-bottom: 10px;" id="cancel" data-toggle="modal" data-target="#popupCancel">Annuler</button>
			<button class="btn btn-form btn-primary buttonComment" style="margin-bottom: 10px;" id="valid">Créer</button>
			<button style="display: none" id="btnPopupValid" data-toggle="modal" data-target="#popupValidation"></button>
		</section>
	</section>
	<?php
		include_once './footer.html';
	?>
</body>
<script>
$(document).ready(function() {
	var idEnter = "<?php echo $modif;?>"; // Vaut false en cas de création, sinon vaut l'ID de l'entreprise en modification
	var name = "<?php echo $enterName;?>";
		
	var elDomains = $('#tdDomains');
	var elCountries = $('#tdCountries');
	
	var idSelectDomains = 'selectDomains';
	var idSelectCountries = "selectCountries";
	var classSelect = 'form-control smallTableElementSM';
	
	var optionTextDomains = "Sélectionner un domaine";
	var optionTextCountries = "Sélectionner un pays";
	
	var popupCancelText = "";
	var popupValidText = "";

	// Variables qui indiqueront lorsque les liste des domains et des pays auront été créées
	var domains = null;
	var countries = null;
		
	ELibrary.get_data('../Controller/get_domains.php', createDomains);
	ELibrary.get_data('../Controller/get_pays.php', createCountries);

	/*
	 * Crée la liste des domaines et lance l'initialisation de la page si la liste des pays a déjà été créée
	 * @param array arDomains	Tableau qui contient les domaines
	 */
	function createDomains(arDomains) {
		ELibrary.createSelect(arDomains, elDomains, idSelectDomains, classSelect, optionTextDomains, undefined, true);
		domains = true;
		if (countries)
			initialization();
		
	}
	/*
	 * Crée la liste des pays et lance l'initialisation de la page si la liste des domaines a déjà été créée
	 * @param array arDomains	Tableau qui contient les pays
	 */
	function createCountries(arCountries) {
		ELibrary.createSelect(arCountries, elCountries, idSelectCountries, classSelect, optionTextCountries, undefined, true);
		countries = true;
		if (domains)
			initialization();
	}

	/*
	 * Initialise différents éléments de la page
	 */
	function initialization() {
		// Initialisation pour la création d'une entreprise
		if (!idEnter) {
			$('h1').text("Ajout d'une entreprise");
			$('#valid').text("Créer");
			popupCancelText = "Êtes-vous sûr de vouloir quitter la page ?</br>La création de l'entreprise n'aura pas lieu.";
			popupValidText = "Confirmez-vous la création de l'entreprise ?</br>Vous serez redirigé vers la page d'annuaire.";
		}
		// Initialisation pour la modification d'une entreprise
		else {
			$('h1').text("Modification d'une entreprise");
			$('#valid').text("Modifier");
			popupCancelText = "Êtes-vous sûr de vouloir quitter la page ?</br>Toutes les modifications non enregistrées seront perdues.";
		 	popupValidText = "Confirmez-vous la modification des données de l'entreprise ?</br>Vous serez redirigé vers la page d'annuaire.";
		 	
			ELibrary.get_data('../Controller/set_enterprises.php', createButtonDelete, {'check': true, 'idEnter': idEnter});
			ELibrary.get_data('../Controller/get_enterprises.php', setDataEnterpriseForEdit, {'idEnter': idEnter});
		}
	}
	
	// Vérifie les champs avant d'afficher une popup de confirmation ou des indications sur les champs incorrects
	$('#valid').click(function(e) {
		var enterprise = $.trim($('input[name=enterprise]').val());
		
		if (enterprise == "") {
			$('#enterprise').parent().addClass('has-error has-feedback');
			$('.error').attr('style', 'display: true;');
			$('#errorEnterprise').html('Ce champs est obligatoire !');
		} else
				ELibrary.get_data('../Controller/get_enterprises.php', checkEnterprises, {'enterpriseExist': enterprise});
	});

	/**
	 * Check si l'entreprise enxiste dans la base de données
	 * @param	string	entExist	"True" si l'entreprise existe | sinon "false"
	 */
	function checkEnterprises(entExist){
		if (entExist === "true" && idEnter == false) {
			$('#enterprise').parent().addClass('has-error has-feedback');
			$('.error').attr('style', 'display: true;');
			$('#errorEnterprise').html('Une entreprise portant ce nom existe déja');
		}
		else if (entExist === "false" || idEnter != false) {
			$('#enterprise').parent().removeClass('has-error has-feedback');
			$('.error').attr('style', 'display: none;');
			$('#btnPopupValid').click();
		}
	}

	// Ajout d'un nouveau secteur
	$('#new_domain').click(function() {
		var domain = $.trim($('input[name=new_domain]').val());
		
		if (domain != "" && domain.length >= 3) {
			ELibrary.get_data('../Controller/set_domains.php', add_domain, {'domain': domain});
			
		}
	});
	
	// Affiche une popup de confirmation pour l'ajout ou la modification d'une entreprise
	$('#btnPopupValid').click(function(){
		$('#validationToShow').html(popupValidText);
	});
		
	// Création ou modification d'une entreprise
	$('#btnAcceptValidation').click(function(){
		
		var enterprise = $.trim($('input[name=enterprise]').val());
		var description = $.trim($('textarea[name=description]').val());
		var domain = $('select[name=' + idSelectDomains + '] option:selected').val();
		
		var country = $('select[name=' + idSelectCountries + '] option:selected').val();
		var city = $.trim($('input[name=city]').val());
		var street = $.trim($('input[name=street]').val());
		var npa = $.trim($('input[name=npa]').val());
		
		// TO DO : Format du téléphone
		var phone = $.trim($('input[name=phone]').val());
		var web = $.trim($('input[name=web]').val());
		
	    $.ajax({
		    method: 'POST',
		    url: '../Controller/set_enterprises.php',
		    data: {
		    	'enterprise': enterprise, 'description': description, 'domain': domain,
		    	'country': country, 'city': city, 'street': street, 'npa': npa,
		    	'phone': phone, 'web': web, 'idEnter': idEnter
			},
	    });
		// Redirection sur la page d'annuaire après la création/modification d'une entreprise
		$.redirect('./directory.php');
		
	    // TO DO : Afficher un message qui indique que l'entreprise a été correctement créé/modifié.
	});
		
	
	// Affichage d'une popup de confirmation pour l'annulation de la création ou modification de l'entreprise
	$('#cancel').click(function(event){
		event.preventDefault();
		$('#cancelToShow').html(popupCancelText);
	});
	
	// Redirection sur la page d'annuaire
	$('#btnAcceptCancel').click(function(){
		// Redirection sur la page d'annuaire après la création/modification d'une entreprise
		$.redirect('./directory.php');
	});
		
	
	// Suppression de l'entreprise + redirection sur la page d'annuaire
	$('#btnAcceptDelete').click(function(){
		$.ajax({
		    method: 'POST',
		    url: '../Controller/set_enterprises.php',
		    data: {'delete': true, 'idEnter': idEnter},
		});
			
		$.redirect('./directory.php');
	});

	/**
	 * Remplis les différents champs de la page d'édition des entreprises avec les données provenant de la base
	 * @param arData	Tableau JSON qui contient les données de l'entreprise
	 */
	function setDataEnterpriseForEdit(arData) {
		$('input[name=enterprise]').val(arData.name);
		$('textarea[name=description]').val(arData.desc);
		if (arData.domain.id != null)
			$('#selectDomains').val(arData.domain.id);
		if (arData.country != null)
			$('#selectCountries').val(arData.country);
		$('input[name=city]').val(arData.city);
		$('input[name=street]').val(arData.street);
		$('input[name=npa]').val(arData.zip);
		$('input[name=phone]').val(arData.phone);
		$('input[name=web]').val(arData.web);
	}
	
	/**
	 * Crée un boutton de suppression
	 * @param bool check	Crée le bouton si vaut true, sinon ne fait rien
	 */
	function createButtonDelete(check) {
		if (check) {
			var btnDelete = $('<button id="delete" class="btn btn-primary buttonComment" data-toggle="modal" data-target="#popupDelete"></button>"');
			btnDelete.text("Supprimer l'entreprise");
			$('#btnDel').append(btnDelete);
			
			// Affiche un message de confirmation pour la suppression de l'entreprise
			$('#delete').click(function() {
				$('#deleteToShow').html("Êtes-vous sûr de vouloir supprimer cette entreprise ?<br>Cette action est irréversible !");
			});
		}
	}
	
	/**
	 * Ajoute un domaine à la liste déroulante des domaines
	 * @param arSector	Tableau JSON qui contient le domaine
	 */
	function add_domain(arDomain) {
		$('#' + idSelectDomains + ' :selected').prop('selected', false);
		// Création de l'option
		var op = $('<option value="' + arDomain.id + '" selected>' + arDomain.name + '</option>');
		$('#' + idSelectDomains).append(op);
		
		$('input[name=new_domain]').val('');

	}
	
});
	</script>
</html>