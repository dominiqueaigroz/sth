<!DOCTYPE html>
<?php
/*
 * Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017
 */
require_once './inc.view.php';
require_once '../Model/inc.all.php';
if (ESession::getRole() === false){
	header('location: ./index.php');
}
else if ((ESession::getRole() !== EC_ROLE_ADMIN)&&(ESession::getRole() !== EC_ROLE_USER)){
	header('location: ./index.php');
}

?>
<html>
<head lang="fr">
	<?php require_once './head.php'; ?>
	<script type="text/javascript" src="../js/back-to-top.js"></script>
	<link rel="stylesheet" href="../css/back-to-top.css"></link>
	<script type="text/javascript" src="../js/faq-template.js"></script>
	<link rel="stylesheet" href="../css/faq-template.css"></link>
	<title>FAQ</title>
</head>
<body>
	<header class="cd-morph-dropdown">
		<?php
			include_once '../php/Nav/bar_nav.php';
		?>
	</header>
	<h1>FAQ</h1>
	<section id="maincontent" class="container-fluid cd-faq">
		<ul class="cd-faq-categories">
			<li><a href="#enterprises">Entreprises</a></li>
			<li><a href="#traineeships">Stages</a></li>
			<li class="adminQuestion"><a href="#domainsAndSectors">Domaines et secteurs</a></li>
			<li class="adminQuestion"><a href="#comments">Commentaires</a></li>
		</ul>
		
		<section class="cd-faq-items">
			<ul id="enterprises" class="cd-faq-group">
				<li class="cd-faq-title">Entreprises</li>
				<li>
					<a class="cd-faq-trigger">Comment recherche-t-on une entreprise ?</a>
					<div class="cd-faq-content">
						Rendez-vous à la page d'annuaire, écrivez le nom de l'entreprise que vous recherché et appuyer sur "Enter".<br>
						<img class="img-responsive" src="../img/faq/enterprises1.png"><br>
						Le tableau n'affichera plus que les entreprises dans le nom correspond à votre recherche.<br>
						Vous pouvez également rechercher une entreprise d'un domaine spécifique, pour ce faire vous devez cliquer sur le bouton "Filtres" qui vous affichera une liste déroulante contenant les différents domaines, sélectionnez en un et cliquer sur le bouton "Appliquer les filtres".<br>
						<img class="img-responsive" src="../img/faq/enterprises1-2.png"><br>
						Le tableau n'affichera plus que les entreprises dans le domaine correspond à celui que vous avez sélectionné dans la liste.
					</div>
				</li>
				<li>
					<a class="cd-faq-trigger">Comment consulte-t-on les informations d'une entreprise ?</a>
					<div class="cd-faq-content">
						Il suffit de cliquer sur le nom de l'entreprise en question pour être redirigé sur sa page de description.				
					</div>
				</li>
				<li class="adminQuestion">
					<a class="cd-faq-trigger">Comment crée-t-on une entreprise ?</a>
					<div class="cd-faq-content">
						Rendez-vous à la page d'annuaire et cliquez sur le <a><span class="fa fa-plus"></span></a> qui vous redirigera sur la page d'édition d'une entreprise.<br>
						Vous n'avez plus qu'à compléter les divers champs et de cliquer sur le bouton "Créer" afin de créer l'entreprise.
					</div>
				</li>
				<li class="adminQuestion">
					<a class="cd-faq-trigger">Comment modifie-t-on une entreprise ?</a>
					<div class="cd-faq-content">
						Rendez-vous à la page d'annuaire et cliquez sur le <a><span class="fa fa-pencil"></span></a> qui vous redirigera sur la page d'édition d'une entreprise. (Vous pouvez également cliquer sur le bouton "Modifier" qui se trouve sur la page de description de n'importe quel entreprise.<br>
						Vous n'avez plus qu'à modifier les divers champs et de cliquer sur le bouton "Modifier" afin de modifier l'entreprise.
					</div>
				</li>

			</ul>
			<ul id="traineeships" class="cd-faq-group">
				<li class="cd-faq-title">Stages</li>
				<li>
					<a class="cd-faq-trigger">Comment recherche-t-on un stage ?</a>
					<div class="cd-faq-content">
				</li>
				<li>
					<a class="cd-faq-trigger">Comment consulte-t-on les informations d'un stage ?</a>
					<div class="cd-faq-content">
					
					</div>
				</li>
				<li>
					<a class="cd-faq-trigger">Comment laisse-t-on une appréciation à un stage ?</a>
					<div class="cd-faq-content">
					
					</div>
				</li>
				<li class="adminQuestion">
					<a class="cd-faq-trigger">Comment crée-t-on un stage ?</a>
					<div class="cd-faq-content">
					
					</div>
				</li>
				<li class="adminQuestion">
					<a class="cd-faq-trigger">Comment modifie-t-on un stage ?</a>
					<div class="cd-faq-content">
					
					</div>
				</li>
				<li class="adminQuestion">
					<a class="cd-faq-trigger">Comment assigne-t-on un élève et un tuteur à un stage ?</a>
					<div class="cd-faq-content">
					
					</div>
				</li>
				<li class="adminQuestion">
					<a class="cd-faq-trigger">Comment modifie-t-on l'élève et/ou le tuteur assigné à un stage ?</a>
					<div class="cd-faq-content">
					
					</div>
				</li>
			</ul>
			<ul id="domainsAndSectors" class="cd-faq-group adminQuestion">
				<li class="cd-faq-title">Domaines et secteurs</li>
				<li>
					<a class="cd-faq-trigger">Comment crée-t-on un domaine ?</a>
					<div class="cd-faq-content">
					
					</div>
				</li>
				<li>
					<a class="cd-faq-trigger">Comment crée-t-on un stage ?</a>
					<div class="cd-faq-content">
					
					</div>
				</li>
			</ul>
			<ul id="comments" class="cd-faq-group">
				<li class="cd-faq-title">Commentaires</li>
				<li>
					<a class="cd-faq-trigger">Comment laisse-t-on un commentaire ?</a>
					<div class="cd-faq-content">
					
					</div>
				</li>
				<li class="adminQuestion">
					<a class="cd-faq-trigger">Comment gère-t-on les commentaires ?</a>
					<div class="cd-faq-content">
					
					</div>
				</li>
			</ul>
		</section>
	</section>
	<a href="#0" class="cd-top">Back to top</a>
	<?php 
		//include_once './footer.html';
	?>
</body>
<script type="text/javascript">
$(document).ready(function(){
	var role = "<?php if (ESession::getRole() !== false){ echo ESession::getRole(); } ?>";
	// Masque les questions qui concernen uniquement l'administrateur
	if(role != 3)
		$('.adminQuestion').hide();	
});
</script>