<!DOCTYPE html>
<?php
/*
 * Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017
 */
require_once './inc.view.php';
require_once '../Model/inc.all.php';
if (ESession::getRole() === false){
	header('location: ./index.php');
}
else if (ESession::getRole() !== EC_ROLE_ADMIN){
	header('location: ./index.php');
}
?>
<html>
<head lang="fr">
	<?php require_once './head.php'; ?>
	<title>Liste des tuteurs</title>
</head>
<body>
	<header class="cd-morph-dropdown">
		<?php
			include_once '../php/Nav/bar_nav.php';
		?>
	</header>
	<section id="maincontent" class="container-fluid">
		<h1>Liste des tuteurs</h1>
		<section class="table-responsive">
			<table id="content" class="tablesorter table">
				<thead class="thead-inverse">
					<tr>
						<th>Tuteur</th>
						<th>Nombre d'élèves</th>
					</tr>
				</thead>
				<tbody id="tutor-data">
				</tbody>
			</table>
		</section>
	</section>
	<?php 
		include_once './footer.html';
	?>
</body>
<script>
$(document).ready(function(){
	ELibrary.get_data('../Controller/get_allTutor.php', createTutorList, {'tutor':true});
	
	/**
	 * Construit un tableau qui contient la liste des tuteurs
	 * @param JSON arData			Tableau JSON qui contient les données à afficher
	 */
	function createTutorList(arData) {
		var el = $('#tutor-data');
	
		arData.forEach(function(table){
			var tr = $('<tr>');
	
			var tdTutor = $('<td>');
			var link = $('<a class="tutorsLink" name="'+ table.civility + ' ' + table.firstName + ' ' + table.lastName + '" id="' + table.email + '" href="#">');
			link.click(function(event){
				event.preventDefault();
				var e = $(this).attr('id');
				var tutorName = $(this).attr('name');
				$.redirect('./tutorDetails.php',{email: e, tutorName: tutorName},"POST");
			});
			var tutor = table.civility + ' ' + table.firstName + ' ' + table.lastName;
			link.html(tutor);
			tdTutor.append(link);
			tr.append(tdTutor);
	
			var tdNbrStudent = $('<td>');
			tdNbrStudent.html(table.nbrStudent);
			tr.append(tdNbrStudent);
			
			el.append(tr)
			
			// Mise à jour du cache pour les tablesorter
			el.trigger("update");
		})
	}
});
</script>
</html>