<!DOCTYPE html>
<?php
/*
 * Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017
 */
require_once './inc.view.php';
?>
<html>
<head lang="fr">
<?php require_once './head.php'; ?>
<title>À propos</title>
</head>
<body>
	<header class="cd-morph-dropdown">
		<?php
		include_once '../php/Nav/bar_nav.php';
		?>
		</header>
	<section id="maincontent" class="container-fluid">
		<h1>À propos</h1>
		<p>Dans le cadre du projet Entreprise École, les élèves ont développé
			ce site web pour d'aider les personnes du CFPT qui souhaitent trouver
			un stage. Avec l’aide de Mme Terrier et du directeur de
			l’établissement, M. Martinez. Le projet a été supervisé au cours de
			l’année scolaire par M. Aigroz, professeur du groupe d’élèves qui ont
			développé ce site web.</p>
		<p>Développeurs : Baertschi Jessica / Küenzi Jean-Daniel / Genga Dario</p>
		<p>Version : 1.0.0</p>
		<p>14/10/2016</p>
	</section>
		<?php
		include_once './footer.html';
		?>
	</body>
</html>