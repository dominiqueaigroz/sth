<!DOCTYPE html>
<!--
Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
Titre: annuaire_stage
Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
Version: 1.0.0
Date: 25.11.2016
Copyright: Entreprise Ecole CFPT-I © 2016-2017
-->
<?php
require_once './inc.view.php';
require_once './popup/popupComment.html';
require_once '../Model/inc.all.php';
if (ESession::getRole() === false){
	header('location: ./index.php');
}
else if (ESession::getRole() !== EC_ROLE_ADMIN){
	header('location: ./index.php');
}
?>
<html>
<head lang="fr">
<?php require_once './head.php'; ?>
<title>Gestion des commentaires</title>
</head>
<body>
	<header class="cd-morph-dropdown">
		<?php
			include_once '../php/Nav/bar_nav.php';
		?>
	</header>
	<section id="maincontent" class="container-fluid">
		<h1>Gestion des commentaires</h1>
		<section style="display: none" id="commentAccepted"
			class="alert alert-success" role="alert">
			<strong>Réussi!</strong> Le commentaire a bien été <strong>accepté</strong>.
		</section>
		<section style="display: none" id="commentRefused"
			class="alert alert-danger" role="alert">
			<strong>Réussi!</strong> Le commentaire a bien été <strong>refusé</strong>.
		</section>
		<section class="table-responsive">
			<table id="content" class="tablesorter table">
				<thead class="thead-inverse">
					<tr>
						<th>Entreprise</th>
						<th>Domaine</th>
						<th>Sujet du stage</th>
						<th>Secteur</th>
						<th>Stagiaire</th>
						<th>Année</th>
						<th>Commentaire</th>
					</tr>
				</thead>
				<tbody id="comment-data">
				</tbody>
			</table>
		</section>
		<section id="dialog"></section>
	</section>
	<?php 
		include_once './footer.html';
	?>
</body>
<script>
$(document).ready(function(){
	ELibrary.get_data('../Controller/get_comments.php', createTraineeshipsForComments, {'validation':true});

	/**
	 * Construit un tableau qui contient les informations du stage qui correspond à un commentaire
	 * @param JSON arData			Tableau JSON qui contient les données à afficher
	 */
	function createTraineeshipsForComments(arData) {
		var el = $('#comment-data');

		arData.forEach(function(table){
			var tr = $('<tr class="' + table.id + '">');

			var tdHead = $('');

			var tdEnter = $('<td>');
			tdEnter.html(table.train.enter.name);
			tr.append(tdEnter);

			var tdDomain = $('<td>');
			tdDomain.html(table.train.enter.domain.name);
			tr.append(tdDomain);

			var tdSubj = $('<td>');
			tdSubj.html(table.train.subj);
			tr.append(tdSubj);

			var tdSect = $('<td>');
			var tdSectHtml = '';
			$indexSect = 0;
			table.train.sectors.forEach(function(sector){
				$indexSect++;
				if($indexSect > 1)
					tdSectHtml = tdSectHtml + ', ' +  sector.name;
				else 
					tdSectHtml = sector.name;	
			})
			tdSect.html(tdSectHtml);
			tr.append(tdSect);

			var tdStagiaire = $('<td>');
			tdStagiaire.html(table.train.stud.civility + ' ' + table.train.stud.firstName + ' ' + table.train.stud.lastName)
			tr.append(tdStagiaire);

			var tdYear = $('<td>');
			var year = '';
			if	(table.train.dateEnd != null){
				var date = new Date(table.train.dateEnd);
				var year = date.getFullYear();
			}else{
				year = 'Aucune information';
			}
			tdYear.html(year);
			tr.append(tdYear);

			var tdComment = $('<td class="showComment">');
			var comment = $('<a title="Afficher le commentaire" href="#" id="' + table.id + '" comment="' + table.comment + '" data-toggle="modal" data-target="#comment"><span class="fa fa-commenting" aria-hidden="true"></span></a>');
			comment.click(function(event){
				event.preventDefault();
				var com = $(this).attr('comment');
				var idComment = $(this).attr('id');
				$('#commentToShow').text(com);
				$('#commentToShow').val(idComment);
				$('#commentToShow').attr('email', table.train.stud.email);
			});
			tdComment.append(comment);
			tr.append(tdComment);

			el.append(tr)
			
			// Mise à jour du cache pour les tablesorter
			el.trigger("update");
		}) // End forEach
		
		$('#btnAccept').click(function(){
			sendMail("Acceptation de votre commentaire", "Votre commentaire a été accepté par l'administrateur", "accept");
		});

		$('#btnRefuse').click(function(){
			sendMail("Refus de votre commentaire", "Votre commentaire a été refusé par l'administrateur, veuillez le modifier", "refuse");
		});
	}

	/**
	 * Effectue Ajax qui va permettre l'envoie d'un mail à l'utilisateur pour lui prévenir si son commentaire a été validé ou refusé
	 * @param string subject	Le sujet du mail
	 * @param string msg		Le message du mail
	 * @param string action		L'état du commentaire ('accept' => commentaire accepté, 'refuse' => commentaire refusé)
	 */
	function sendMail(subject, msg, action) {
		var idComment = $('#commentToShow').val();
		var emailTo = $('#commentToShow').attr('email');
		ELibrary.get_data("../Controller/set_comment.php", showStateComment, {'action': action, 'id': idComment, 'subject': subject, 'msg': msg, 'emailTo': emailTo});
		$('.' + idComment).remove();
	}

	/**
	 * Affiche un message qui va valider ou invalider un commentaire
	 * @param string data		Indique l'état du commentaire (accepté ou refusé)
	 */
	function showStateComment(data) {
		if (data == "accept"){
			ELibrary.showHtmlElement("commentAccepted");
		}
		else if (data == "refuse"){
			ELibrary.showHtmlElement("commentRefused");
		}
		// On demande le rafraichissement du badge
		ELibrary.get_data("../Controller/get_comments.php", ELibrary.loadUnvalidatedComments, {'count': true}, 'unvalidatedComments', 'comment-data');
	}


});	
</script>
</html>