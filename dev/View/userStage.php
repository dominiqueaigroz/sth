<!DOCTYPE html>
<!--
Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
Titre: annuaire_stage
Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
Version: 1.0.0
Date: 25.11.2016
Copyright: Entreprise Ecole CFPT-I © 2016-2017
-->
<?php
require_once './inc.view.php';
require_once './popup/popupShowComment.html';
if (ESession::getRole() === false){
	header('location: ./index.php');
}
else if (ESession::getRole() !== EC_ROLE_USER){
	header('location: ./index.php');
}
?>
<html>
	<head lang="fr">
		<?php require_once './head.php'; ?>
		<title>Vos stages</title>
	</head>
	<body>
		<header class="cd-morph-dropdown">
			<?php
				include_once '../php/Nav/bar_nav.php';
			?>
		</header>
			<section id="maincontent" class="container-fluid">
				<h1>Liste des stages de l'élève</h1>
				<section class="table-responsive">
					<table id="content" class="tablesorter table">
						<thead  class="thead-inverse">
							<tr>
								<th>Entreprise</th>
								<th>Domaine</th>
								<th>Sujet du stage</th>
								<th>Secteur</th>
								<th>Tuteur</th>
								<th>Email du tuteur</th>
								<th>Commentaire</th>
							</tr>
						</thead>
						<tbody id="traineeships-data">
						</tbody>
					</table>
				</section>
			</section>
	<?php 
		include_once './footer.html';
	?>
	</body>
	<script>
		var user = "<?php echo ESession::getEmail(); ?>";
		
		ELibrary.get_data('../Controller/get_allTraineeFromUser.php', createTraineeshipsForUser, {'student':user});

		/**
		 * Construit un tableau qui contient les stages de l'élève
		 * @param JSON arData	Tableau JSON qui contient les données à afficher
		 */
		 function createTraineeshipsForUser(arData) {
			var el = $('#traineeships-data');
	
			arData.forEach(function(table){
				var tr = $('<tr>');
	
				var tdName = $('<td>');
				var link = $('<a name="' + table.enter.name + '" class="enterLinks" id="' + table.enter.id + '" href="#">');
				link.click(function(event){
					event.preventDefault();
					var id = $(this).attr('id');
					var name = $(this).attr('name');
					$.redirect('./description_enterprise.php',{id: id, enterName: name},"POST");
				});
				link.html(table.enter.name);
				tdName.append(link);
				tr.append(tdName);
	
				var tdDomain = $('<td>');
				tdDomain.html(table.enter.domain.name);
				tr.append(tdDomain);
				
				var tdSubj = $('<td>');
				var link = $('<a class="enterLinks" id="' + table.id + '" href="#">');
				link.click(function(event){
					event.preventDefault();
					var id = $(this).attr('id');
					$.redirect('./description_traineeship.php',{id: id},"POST");
				});
				link.html(table.subj);
				tdSubj.append(link);
				tr.append(tdSubj);
	
				var tdSect = $('<td>');
				var tdSectHtml = '';
				var index = 0;
				table.sectors.forEach(function(sectors){
					index++;
					if (index > 1)
						tdSectHtml = tdSectHtml + ', ' + sectors.name;
					else
						tdSectHtml = sectors.name;
				})
				tdSect.html(tdSectHtml);
				tr.append(tdSect);
	
				var tdTutor = $('<td>');
				var tutor = table.tutor.civility + ' ' + table.tutor.firstName + ' ' + table.tutor.lastName;
				tdTutor.html(tutor);
				tr.append(tdTutor);
	
				var tdMailTutor = $('<td>');
				tdMailTutor.html(table.tutor.email);
				tr.append(tdMailTutor);

				var tdComment = $('<td>');
				var comment = table.comm.comment;
				var states = parseInt(table.comm.states);
				if	(comment != false && comment != undefined) {

					switch (states) {
						case 1: // commentaire en attente
							var buttonComment = $('<p>');
							buttonComment.text("Votre commentaire est en attente");
							break;
						case 2: // commentaire validé
							var buttonComment = $('<button class="btn btn-success buttonComment" id="' + table.id + '" value="' + comment + '" data-toggle="modal" data-target="#comment">Afficher le commentaire</button>');
							buttonComment.click(function(event){
								event.preventDefault();
								var com = $(this).val();
								var idComment = $(this).attr('id');
								$('#commentToShow').text(com);
								$('#commentToShow').val(idComment);
							});
							break;
						case 3: // commentaire refusé
							var buttonComment = $('<button class="btn btn-danger buttonComment" id="' + table.id + '" value="' + comment + '">Modifier le commentaire</button>');
							buttonComment.click(function(event){
								event.preventDefault();
								var com = $(this).val();
								var idComment = $(this).attr('id');
								$.redirect('./commentStage.php',{id: idComment},"POST");
							});
							break;
						default:
							// Status inconnu
							var buttonComment = "Error";
							break;
					}

				} else if (comment == false || comment == undefined){
					var buttonComment = $('<button class="btn btn-primary buttonComment" id="' + table.id + '" value="' + comment + '">Laisser un commentaire</button>');
					buttonComment.click(function(event){
						event.preventDefault();
						var com = $(this).val();
						var idComment = $(this).attr('id');
						$.redirect('./commentStage.php',{id: idComment},"POST");
					});
				} else {
					var buttonComment = "Error";
				}
				tdComment.append(buttonComment);
				tr.append(buttonComment);
				
				el.append(tr)
				
				// Mise à jour du cache pour les tablesorter
				el.trigger("update");
			})
		}
	</script>
</html>