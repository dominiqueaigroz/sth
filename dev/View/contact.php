<!DOCTYPE html>
<?php
/*
 * Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017
 */
require_once './inc.view.php';
?>
<html>
<head lang="fr">
	<?php require_once './head.php'; ?>
	<title>Contact</title>
</head>
<body>
	<header class="cd-morph-dropdown">
			<?php
			include_once '../php/Nav/bar_nav.php';
			?>
		</header>
	<section id="maincontent" class="container-fluid">
		<h1>Contact</h1>
		<h2>Page utilisée pour contacter l'école</h2>
		<p>
			Adresse </br> CFPT - Secrétariat de l'Ecole d'Informatique </br>
			Bâtiment Rhône</br> 10, chemin Gérard-de-Ternier</br> 1213
			Petit-Lancy</br> tél. +41 22 388 87 28 - fax. +41 22 388 87 65 </br>
			Horaire de la réception : </br> Du lundi au vendredi, de 7h30 à 11h30
			et de 13h30 à 16h30.
		</p>

		<p>
			E-Mail de l'école : <a href="mailto:cpftinformatique@edu.ge.ch">cpftinformatique@edu.ge.ch</a>
		</p>
		<p>
			E-Mail de l'administrateur : <a href="mailto:admin@edu.ge.ch">admin@edu.ge.ch</a>
		</p>
	</section>
		<?php
		include_once './footer.html';
		?>
	</body>
</html>