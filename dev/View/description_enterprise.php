<!DOCTYPE html>
<?php
/*
 * Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017
 */
require_once '../Model/inc.all.php';
require_once './inc.view.php';
if (ESession::getRole() === false){
	header('location: ./index.php');
}
else if ((ESession::getRole() !== EC_ROLE_ADMIN)&&(ESession::getRole() !== EC_ROLE_USER)){
	header('location: ./index.php');
}
?>

<html>
<head lang="fr">
<?php require_once './head.php'; ?>
<script type="text/javascript" src="../js/eelmaps.js"></script>
<title>Description de l'entreprise</title>
</head>
<body>
	<header class="cd-morph-dropdown">
		<?php
			include_once '../php/Nav/bar_nav.php';
		?>
	</header>
	<section id="maincontent" class="container-fluid">
		<h1>Description de l'entreprise</h1>
		<fieldset>
			<legend>Information sur l'entreprise</legend>
			<section class="table-responsive">
				<table class="table smallTable">
					<tr>
						<td>
							<table id="enterprise" class="table datatable smallTableElementSM">
								<tbody id="enterprise-data">
								</tbody>
							</table>
						</td>
						<td>
							<div id="map"></div>
						</td>
					</tr>
				</table>
			</section>
		</fieldset>
		<section class="modif"></section>
		<fieldset>
			<legend>Offre(s) de stage actuel </legend>
			<section class="table-responsive">
				<table id="content" class="tablesorter table">
					<thead class="thead-inverse">
						<tr>
							<th>Sujet du stage</th>
							<th>Secteur</th>
							<th>Durée</th>
							<th>Date de début</th>
							<th>Date de fin</th>
							<th>Disponibilité</th>
						</tr>
					</thead>
					<tbody id="offer-data">
					</tbody>
				</table>
			</section>
		</fieldset>
		<fieldset>
			<legend>Historique des stages </legend>
			<section class="table-responsive">
				<table id="content2" class="tablesorter table">
					<thead class="thead-inverse">
						<tr>
							<th>Sujet du stage</th>
							<th>Secteur</th>
							<th>Date de début</th>
							<th>Date de fin</th>
							<th>Disponibilité</th>
							<th>Stagiaire</th>
							<th>Email du Stagiaire</th>
							<th>Tuteur</th>
							<th>Email du tuteur</th>
						</tr>
					</thead>
					<tbody id="history-data">
					</tbody>
				</table>
			</section>			
		</fieldset>
	</section>
	<?php 
		include_once './footer.html';
	?>
</body>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_5DzbXZTLIGynGDbcyrarq0PzuDH9EC8&language=fr&callback=EELMaps.initMap"></script>
<script>			
$(document).ready(function(){
	var role = "<?php if (ESession::getRole() !== false){ echo ESession::getRole(); } ?>";
	var roleAdmin = "<?php echo EC_ROLE_ADMIN; ?>";
	var idEnter = "<?php echo $_POST['id']; ?>";
	var enterName = "<?php echo $_POST['enterName']?>"; // Récupère le nom de mon entreprise passé en paramètre

	ELibrary.get_data('../Controller/get_enterpriseAddress.php', createAddressEnterprises, {'idEnterprise': idEnter });
	ELibrary.get_data('../Controller/get_traineeships.php', createTraineeshipsToCome, {'IdEnter': idEnter, 'status': 'toCome'});
	ELibrary.get_data('../Controller/get_traineeships.php', createTraineeshipsFinished, {'IdEnter': idEnter, 'status': 'finished'});
	ELibrary.get_data('../Controller/get_enterprises.php', createDescriptionEnterprise, {'nameEnterprise': enterName });

	if (role == roleAdmin) {
		modifButton();
	}
	
	function modifButton() {
		var button = $('<button class="btn btn-primary"><span class="fa fa-pencil"></span> Modifier</button>');
		button.click(function(event){
			event.preventDefault();
			$.redirect('./edit_enterprises.php',{id: idEnter, enterName: name},"POST");
		});
		$('.modif').append(button);
	}


	// Cette fonction permet de récupéré les données de géolocalisation (longitude, latitude)
	function locateAddr(addr) {
	    var t = addr;
	    if (t.length > 0){
	        EELMaps.getLocations(t, OnResultLocation);
	    }
	}
	
	// En cas de réponse (succès), efface les anciens markers, ajoute les nouveaux markers (il peut y en avoir plusieurs) aux bonne position puis centre le maps sur la position recherché
	function OnResultLocation(ar, st) {
	  // Est-ce qu'on a quelque chose dans le tableau ?
	  if (ar.length > 0) {
	        for(var i = 0; i < ar.length; ++i) {
	            EELMaps.deleteMarkers(); // Supprime les anciens markers
	            EELMaps.createMarkers(ar[i].location); // Ajoute le/les nouveau(x) markers
	            EELMaps.setPosition(ar[i].location); // centre le maps sur la position (ne marches pas si plusieurs position sont trouvées)
	        }                     
	    }
	}
	
	// Cette fonction génère mon adresse à rechercher puis appelle la fonction locateAddr() et lui passe en paramètre celle-ci
	function createAddressEnterprises(arAddress) {  
	    var address = arAddress.street + ', ' + arAddress.city + ', Genève, Suisse'; // Génère mon adresse sous la forme "Rue, Quartier, Ville, Pays" (ex: 51 Rue Jacques-Dalphin, Carouge, Genève, Suisse)
	    locateAddr(address); // Appelle la fonction et lui passe en paramètre mon adresse
	}

	/**
	 * Construit un tableau qui contient la description d'une entreprise
	 * @param JSON arData			Tableau JSON qui contient les données à afficher
	 * @param string idElement		L'ID de l'élément où l'on affichera les données
	 */
	function createDescriptionEnterprise(arData) {
		var el = $('#enterprise-data');
		
		var trEnter = $('<tr>');
		trEnter.html(arData.name);
		el.append(trEnter);
		
		var trDomain = $('<tr>');
		trDomain.html(arData.domain.name);
		el.append(trDomain);
		
		var trPhone = $('<tr>');
		trPhone.html(arData.phone);
		el.append(trPhone);

		var trWebsite = $('<tr>');
		trWebsite.html(arData.web);
		el.append(trWebsite);

		var trAddress = $('<tr>');
		trAddress.html(arData.street+" "+arData.zip+" "+arData.city);
		el.append(trAddress);

	}
	
	/**
	 * Construit un tableau qui contient les stages à venir d'une entreprise
	 * @param JSON arToCome			Tableau JSON qui contient les données à afficher
	 */
	function createTraineeshipsToCome(arToCome) {
		var el = $('#offer-data');		
	
		arToCome.forEach(function(table) {
			var tr = $('<tr>');
	
			var editTraineeship = $('<a class="fa fa-pencil editTraineeship" id="' + table.id + '" href="#"></a>');
			editTraineeship.click(function(event){
				event.preventDefault();
				var id = $(this).attr('id');
				$.redirect('./edit_traineeships.php',{id: id},"POST");
			});
			
			var tdSubj = $('<td>');
			var link = $('<a name="' + table.subj + '" class="traineeshipLinks" id="' + table.id + '" href="#">');
			link.click(function(event){
				event.preventDefault();
				var id = $(this).attr('id');
				var name = $(this).attr('name');
				$.redirect('./description_traineeship.php',{id: id, traineeship: name},"POST");
			});
			link.html(table.subj);
			if (role == roleAdmin)
				tdSubj.append(editTraineeship);
			tdSubj.append(link);
			tr.append(tdSubj);
	
			var tdSect = $('<td>');
			var tdSectHtml = 'Non défini';
			$indexSect = 0;
			table.sectors.forEach(function(sector){
				$indexSect++;
				if($indexSect > 1)
					tdSectHtml = tdSectHtml + ', ' +  sector.name;
				else 
					tdSectHtml = sector.name;	
			})
			tdSect.html(tdSectHtml);
			tr.append(tdSect);
	
	
			var tdDuration = $('<td>');
			tdDuration.html(table.duration);
			tr.append(tdDuration);
			
			var tdDateBeg = $('<td>');
			if (table.dateBeg != null)
				tdDateBeg.html(table.dateBeg);
			else
				tdDateBeg.html('Non définie');
			tr.append(tdDateBeg);
	
			var tdDateEnd = $('<td>')
			if (table.dateBeg != null)
				tdDateEnd.html(table.dateEnd);
			else
				tdDateEnd.html('Non définie');
			tr.append(tdDateEnd);
	
			var tdAvailab = $('<td>');
			tdAvailab.html(table.availab.label);
			tr.append(tdAvailab);
			
			el.append(tr)
			
			// Mise à jour du cache pour les tablesorter
			el.trigger("update");
		})
	
	}
	
	/**
	 * Construit un tableau qui contient l'historique des stages d'une entreprise
	 * @param JSON arFinished		Tableau JSON qui contient les données à afficher
	 */
	function createTraineeshipsFinished(arFinished) {
		var el = $('#history-data');
	
		arFinished.forEach(function(table) {
			var tr = $('<tr>');
	
			var tdSubj = $('<td>');
			var link = $('<a name="' + table.subj + '" class="traineeshipLinks" id="' + table.id + '" href="#">');
			link.click(function(event){
				event.preventDefault();
				var id = $(this).attr('id');
				var name = $(this).attr('name');
				$.redirect('./description_traineeship.php',{id: id, traineeship: name},"POST");
			});
			link.html(table.subj);
			tdSubj.append(link);
			tr.append(tdSubj);
	
			var tdSect = $('<td>');
			var tdSectHtml = 'Non défini';
			$indexSect = 0;
			table.sectors.forEach(function(sector){
				$indexSect++;
				if($indexSect > 1)
					tdSectHtml = tdSectHtml + ', ' +  sector.name;
				else 
					tdSectHtml = sector.name;	
			})
			tdSect.html(tdSectHtml);
			tr.append(tdSect);
	
			var tdDateBeg = $('<td>');
			if (table.dateBeg != null)
				tdDateBeg.html(table.dateBeg);
			else
				tdDateBeg.html('Non définie');
			tr.append(tdDateBeg);
	
			var tdDateEnd = $('<td>')
			if (table.dateBeg != null)
				tdDateEnd.html(table.dateEnd);
			else
				tdDateEnd.html('Non définie');
			tr.append(tdDateEnd);
	
			var tdAvailab = $('<td>');
			tdAvailab.html(table.availab.label);
			tr.append(tdAvailab);
	
			var tdStudent = $('<td>');
			var studCivil = table.stud.civility || "";
			var studFirstName = table.stud.firstName || "";
			var studLastName = table.stud.lastName || "";
			tdStudent.html(studCivil + " " + studFirstName + " " + studLastName);
			tr.append(tdStudent);
	
			var tdStudentEmail = $('<td>');
			tdStudentEmail.html(table.stud.email);
			tr.append(tdStudentEmail);
	
			var tdTutor = $('<td>');
			var tutorCivil = table.tutor.civility || "";
			var tutorFirstName = table.tutor.firstName || "";
			var tutorLastName = table.tutor.lastName || "";
			tdTutor.html(tutorCivil + " " + tutorFirstName + " " + tutorLastName);
			tr.append(tdTutor);
	
			var tdTutorEmail = $('<td>');
			tdTutorEmail.html(table.tutor.email);
			tr.append(tdTutorEmail);
	
			el.append(tr)
			
			// Mise à jour du cache pour les tablesorter
			el.trigger("update");
		})
	}

});
</script>
</html>