/*
 * Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017
 */

$(document).ready(function(){

		$.ajax({
		    method: 'POST',
		    url: '/php/get_domains.php',
		    dataType: 'json',
		    success: function (data) {
			    var msg = '';

			    switch (data.ReturnCode){
		        case 0 : // tout bon
		            displaySelectDomains(data.Data);
		            break;
		        case 2 : // problème récup données
		        case 3 : // problème encodage sur serveur
			    defaut:
			        msg = data.Message;
		            break;
		        }
		        if (msg.length > 0)
		        	$("#info").html(msg);
		    },
		    error: function(jqXHR){
			    var msg = '';
		        switch(jqXHR.status){
		        case 404 :
		            msg = "page pas trouvée. 404";
		            break;
		        case 200 :
		            msg = "probleme avec json. 200";
		            break;
		             
		        } // End switch
		        if (msg.length > 0)
		        	$("#info").html(msg);
		    }
		                 
		});
});
/**
 * Construit la liste déroulante des domaines
 * @param arData	Tableau JSON qui contient les domaines
 */
function displaySelectDomains(arData) {
	var maindiv = $('#selectDomains');
	var index = 1;
	
	var sel = $('<select id="filterDomains">');
	
	sel.append('<option value="0" selected>Afficher tout les domaines</option>');
	arData.forEach(function(select){
		var op = $('<option value="' + index + '">' + select.name + '</option>');
		sel.append(op);
		index++;
	}) // End forEach
	maindiv.append(sel);
}