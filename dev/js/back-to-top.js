// https://codyhouse.co/gem/back-to-top/

jQuery(document).ready(function($){
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');

	//hide or show the "back to top" link
	// On utilise $('#maincontent') ici pour récupérer l'event .scroll de notre balise '#maincontent'
	// si on souhaite récupérer l'event pour la fenêtre, alors on utilise $(windows)
	$('#maincontent').scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});

	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		//  Mettre ici les éléments html que l'on souhaite animé
		$('#maincontent').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
	});

});