/**
 * @Auteur: Jean-Daniel Küenzi
 * @Titre: annuaire_stage
 * @Description: librairie pour google maps à l'aide de l'api
 * 				 Google Maps JavaScript API
 * @Version: 1.0.0
 * @Date: 03.04.2017
 * @Copyright: Entreprise Ecole CFPT-I © 2016-2017 
 */


var EELMaps = (function(){
	
    var gmap;
    var infoWindow;
    var geocoder;
    var markers = [];
    
	return{
	
		initMap : function() {
	  	  gmap = new google.maps.Map(document.getElementById('map'), {
		        center: {lat: 46.190132399999996, lng: 6.1045475},
		        zoom: 15
		  })
		
		        infoWindow = new google.maps.InfoWindow;
		        // Initialization de l'objet geo coder
		        geocoder = new google.maps.Geocoder();
      }, // #End of initMap()

      getLocations : function(addr, OnResults) {
          if (addr.length > 0){
        	  geocoder.geocode({'address': addr}, function(results, status) {
            	  var res = new Array();
                  if (status === 'OK') {
                      for(var i = 0; i < results.length; ++i){
                          res[i] = { "formattedaddress" : results[i].formatted_address,
                        		     "addresscomps" : results[i].address_components,
                              		 "location" : results[i].geometry.location};
                      }
                  }
                  OnResults.call(this, res, status);
          	});
	      }
      },
      
      /**
       * @brief Définit le map sur tous les marqueurs dans le tableau
       */
      setMapOnAll : function(gmap) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(gmap);
        }
      },
      
      /**
       * @brief Cache les markers mais les gardes sauvegardés dans le tableau
       */
      clearMarkers : function() {
        EELMaps.setMapOnAll(null);
      },
      
      /**
       * @brief Suprimme touts les markers présent sur le map
       */
      deleteMarkers : function() {
          EELMaps.clearMarkers();
          markers = [];
      },
      
  	/**
  	 * @brief Placer un marker sur la map
  	 * @param InLoc L'objet location ()
  	 */
      createMarkers : function(InLoc) {
    	  var marker = new google.maps.Marker({
              map: gmap,
              position: InLoc
            });
          markers.push(marker);
      },

      /**
       * @brief centre le map sur la localisation effectué par la recherche
       * @param InLoc L'objet location ()
       */
      setPosition : function(InLoc) {         
          gmap.setCenter(InLoc);
          gmap.setZoom(16);
      },
      
      handleLocationError : function(browserHasGeolocation, infoWindow, pos) {
          infoWindow.setPosition(pos);
          infoWindow.setContent(browserHasGeolocation ?
                                'Error: The Geolocation service failed.' :
                                'Error: Your browser doesn\'t support geolocation.');
          infoWindow.open(gmap);
      }
	};
}());