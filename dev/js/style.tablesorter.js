/** 
 * @Author	Küenzi Jean-Daniel
 * @Desc	Fichier .js servant à personnaliser les tablesorter
 */
	$(function() {
		//Sert à sélectionner le tableau afin de pouvoir lui appliquer des options/paramêtres
		  $("table").tablesorter({	    
		    theme: 'blue',
		    // initialize zebra striping of the table
		    widgets: ["zebra"],
		    // change the default striping class names
		    // updated in v2.1 to use widgetOptions.zebra = ["even", "odd"]
		    // widgetZebra: { css: [ "normal-row", "alt-row" ] } still works
		    widgetOptions : {
		      zebra : [ "normal-row", "alt-row" ]
		    }

		  });
		});
	