<script>
  $( function() {
    $( document ).tooltip({
    	classes: {
    	    "ui-tooltip": "EE-tooltip"
    	  }
        });
  } );
  </script>

<?php
/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017 */
require_once '../Model/ESession.php';
switch (ESession::getRole()){

	case EC_ROLE_USER:
		include_once '../php/Nav/bar_nav_student.html';
		break;
		
	case EC_ROLE_ADMIN:
		include_once '../php/Nav/bar_nav_admin.html';
		break;

	case EC_ROLE_UNKNOWN:
		include_once '../php/Nav/bar_nav_no_connect.html';
		break;
	default:
			include_once '../php/Nav/bar_nav_no_connect.html';
			break;
		
}