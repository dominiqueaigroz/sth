<?php

/**
 * Auteur: Jean-Daniel Küenzi
 * Description : Ce fichier contient les constantes pour gérer les envoies d'email
 * Version: 1.0.0
 * Date: 24.04.2017
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017  
 */

/** Constante contenant l'email du compte depuis lequel on envoie les email */
define ("EMAIL_EMAIL", "ee.cfpti@gmail.com");

/** Constante contenant le mot de passe du compte depuis lequel on envoie les email */
define ("EMAIL_PASSWORD", "CfPtI2017");

/** Constante contenant le serveur depuis lequel on envoie les email */
define ("EMAIL_SERVER", "smtp.gmail.com");

/** Constante contenant le port par lequel on passe pour envoyer les email */
define ("EMAIL_PORT", "465");

/** Constante contenant le type de transport par lequel on passe pour envoyer les email */
define ("EMAIL_TRANSPORT", "ssl");

?>