<?php

/** 
 * Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017 
 */

define ('EC_ROLE', "role");

define ('EC_ROLE_UNKNOWN', 0);
define ('EC_ROLE_USER', 1);
define ('EC_ROLE_ADMIN', 3);

define('EC_NB_CRITERIA', 3);
/*
 *  Peut-on remplacer le 3 par une méthode qui calcule automatiquement le nombre de critère dans la base ?
 *  ECriterionManager::getInstance()->getNumberOfCriteria()
 *  Cela nécessiterait l'ajout du fichier ECriterionManager.php pour que la fonction marche
 */


define('EC_NB_MIN_COMM', 20); // Nombre minimum de caractère pour le commentaire
?>
