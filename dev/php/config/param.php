<?php

/** 
 * Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Description : Ce fichier contient les constantes utiles à la connexion à la base de données
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017 
 */

define ('EDB_DBTYPE', "mysql");
define ('EDB_HOST', "localhost");
define ('EDB_PORT', "3306");
define ('EDB_DBNAME', "ENT_REPOSITORY");
define ('EDB_USER', "dbsth");
define ('EDB_PASS', "Super");