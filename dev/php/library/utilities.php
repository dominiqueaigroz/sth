<?php

/**
 * @brief	Fichier contenant toutes les fonctions de calcul, etc...
 * @author 	jean-daniel.knz@eduge.ch
 * @remark
 * @version 1.0.0
 * @Copyright Entreprise Ecole CFPT-I © 2016-2017 
 */

	/**
	 * Cette fonction sert à calculer la durée en jour entre 2 dates
	 * @param	date	$startDate	La date de début
	 * @param	date	$endDate	La date de fin
	 * @return	int		$duration	La durée en jour
	 */
	function calculatedDurationTraineeShip($startDate, $endDate) {
		$date1 = new DateTime(date('Y-m-d', strtotime($startDate)));
		$date2 = new DateTime(date('Y-m-d', strtotime($endDate)));
		$duration = $date1->diff($date2)->days. ' jours';
		if ($duration > 30){
			$duration = round($duration / 30) . ' mois';
		}
		else if ($duration < 30){
			$duration = round($duration / 7) . ' semaines';
		}
		return $duration;
	}
	/**
	 * Cette fonction sert à créer les select sans mettre de données en dur
	 * @param string 	$name		Le nom
	 * @param string 	$class		La classe
	 * @param array 	$options	Les valeurs
	 */
	function createSelect($name, $class, $options) {
		echo '<select class="' . $class . '" name="' . $name . '">';
		foreach ( $options as $key => $value ) {
				echo '<option value="' . $key . '">' . $value . '</option>';
		}
		echo '</select>';
	}
?>