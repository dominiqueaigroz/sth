<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

require_once '../Model/inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');
$email = "";
if (isset($_POST['email'])){
	$email = $_POST['email'];
}
$firstname = "";
if (isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}
$lastname = "";
if (isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

$retCode = 0;

if (strlen($email) > 0 && 
	strlen($firstname) > 0 && 
	strlen($lastname) > 0)
{
	$user = EUserManager::getInstance()->findUserByEmail($email);

	if ($user === false) {
		$user = new EUser($email,$lastname, $firstname);
		if (EUserManager::getInstance()->CreateUser($user) == false)
		{
			echo '{ "ReturnCode" : 2, "Message" : "Un problème de récupération des données de getAllUsers()"}';
			exit();
		}
		$retCode = 5;
	}
		
}
else{
	echo '{ "ReturnCode" : 4, "Message" : "Paramètres manquants get_users.php"}';
	exit();

}

$jsn = json_encode($user, JSON_UNESCAPED_UNICODE); // JSON_UNESCAPED_UNICODE nécessaire !

if ($jsn == false) {
	$code = json_last_error();
	echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json ('.$code.'"}';
	exit();
}
//Si j'arrive ici, ouf... c'est tout bon
echo '{"ReturnCode": '.$retCode.', "Data": '. $jsn .'}'; // ne pas mettre utf8_encode() !!
?>