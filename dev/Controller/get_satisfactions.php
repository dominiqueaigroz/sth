<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

require_once '../Model/inc.all.php';
// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$satisfactions = ESatisfactionManager::getInstance()->loadAllSatisfactions();

$criteria = '';
if (isset($_POST['idTraineeship'])) {
	// Le code qui suit sert à ajouter les critères dans le JSON des satisfactions
	// cela permettra de faire la page "avis sur le stage"
	$criteria = ECriterionManager::getInstance()->loadAllCriteria();
	$jsn = json_encode($criteria, JSON_UNESCAPED_UNICODE); // JSON_UNESCAPED_UNICODE nécessaire !
	$criteria = '"DataCriteria": '. $jsn; // ne pas mettre utf8_encode() !!
	
	$satisfForCrit = ECriterionManager::getInstance()->getSatisfactionsForCriteriaByTraineeId($_POST['idTraineeship']);
	$jsn = json_encode($satisfForCrit, JSON_UNESCAPED_UNICODE); // JSON_UNESCAPED_UNICODE nécessaire !
	$satisfForCrit = '"DataSatisfForCrit": '. $jsn; // ne pas mettre utf8_encode() !!
}

if ($satisfactions === false) {
	echo '{ "ReturnCode" : 2, "Message" : "Un problème de récupération des données de getAllSatisfactions()"}';
	exit();
}

$jsn = json_encode($satisfactions, JSON_UNESCAPED_UNICODE); // JSON_UNESCAPED_UNICODE nécessaire !

if ($jsn == false) {
	$code = json_last_error();
	echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json ('.$code.'"}';
	exit();
}
//Si j'arrive ici, ouf... c'est tout bon

if ($satisfForCrit != false && $criteria != '')	{
	echo '{"ReturnCode": 0, "Data":{"DataSatisfaction": '. $jsn .', '. $criteria . ', '. $satisfForCrit . '}}'; // ne pas mettre utf8_encode() !!
}
else if ($criteria != '') {
	echo '{"ReturnCode": 0, "Data": '. $jsn .', '. $criteria . '}'; // ne pas mettre utf8_encode() !!
}
else {
	echo '{"ReturnCode": 0, "Data": '. $jsn . '}'; // ne pas mettre utf8_encode() !!
}
exit();

?>