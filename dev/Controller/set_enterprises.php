<?php
/*
 * Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017
 */
require_once '../Model/inc.all.php';
// Nécessaire lorsqu'on retourne du json
header ( 'Content-Type: application/json' );

if (isset($_POST['idEnter'])) 
	$idEnter = $_POST['idEnter'];

if (isset($_POST['check'])) {
	$check = EAppManager::getInstance()->checkTrainFromEnter($idEnter);

	if ($check === false) {
		echo '{ "ReturnCode" : 2, "Message" : "Un problème de récupération des données de checkTrainFromEnter()"}';
		exit();
	}

	$jsn = json_encode ( $check, JSON_UNESCAPED_UNICODE ); // JSON_UNESCAPED_UNICODE nécessaire !

	if ($jsn == false) {
		$code = json_last_error();
		echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json (' . $code . '"}';
		exit();
	}
	// Si j'arrive ici, ouf... c'est tout bon
	echo '{"ReturnCode": 0, "Data": ' . $jsn . '}'; // ne pas mettre utf8_encode() !!
	exit();
}

if (isset($_POST['enterprise'])) {
	$enterprise = $_POST ['enterprise'];
}
if (isset($_POST['domain'])) {
	$domain = $_POST ['domain'];
	if ($domain == '0')
		$domain = null;
}
if (isset($_POST['description'])) {
	$description = $_POST ['description'];
	if ($description == "")
		$description = null;
}
if (isset($_POST['country'])) {
	$country = $_POST ['country'];
	if ($country == '0')
		$country = null;
}
if (isset($_POST['city'])) {
	$city = $_POST ['city'];
	if ($city == "")
		$city = null;
}
if (isset($_POST['street'])) {
	$street = $_POST ['street'];
	if ($street == "")
		$street = null;
}
if (isset($_POST['npa'])) {
	$npa = $_POST ['npa'];
	if ($npa == "")
		$npa = null;
}
if (isset($_POST['phone'])) {
	$phone = $_POST ['phone'];
	if ($phone == "")
		$phone = null;
}
if (isset($_POST['web'])) {
	$web = $_POST ['web'];
	if ($web == "")
		$web = null;
}
if (isset($_POST['delete'])) {
	$delete = $_POST['delete'];
	if ($delete) {
		EAppManager::getInstance()->deleteEnterprise($idEnter);
		exit();
	}
}
	
// Création d'une nouvelle entreprise
if ($idEnter == 'false') {
	EAppManager::getInstance ()->addEnterprise($enterprise, $description, $phone, $web, 1, $street, $npa, $city, $domain, $country);
}  
// Modification d'une entreprise
else {
	EAppManager::getInstance()->updateEnterprise($idEnter, $enterprise, $description, $phone, $web, 1, $street, $npa, $city, $domain, $country);
}

?>