<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

require_once '../Model/inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$domains = EDomainManager::getInstance()->loadAllDomains();

if ($domains === false) {
	echo '{ "ReturnCode" : 2, "Message" : "Un problème de récupération des données de getAllDomains()"}';
	exit();
}

$jsn = json_encode($domains, JSON_UNESCAPED_UNICODE); // JSON_UNESCAPED_UNICODE nécessaire !

if ($jsn == false) {
	$code = json_last_error();
	echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json ('.$code.'"}';
	exit();
}
//Si j'arrive ici, ouf... c'est tout bon
echo '{"ReturnCode": 0, "Data": '. $jsn .'}'; // ne pas mettre utf8_encode() !!
exit();

?>