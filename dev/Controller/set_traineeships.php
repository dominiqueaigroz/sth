<?php
/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

require_once '../Model/inc.all.php';

$traineeship = intval($_POST['traineeship']);

$enterprise = intval($_POST['enterprise']);
$subject = $_POST['subject'];
$sectors = intval($_POST['sectors']);
$dateBeg = $_POST['dateBeg'];
$dateEnd = $_POST['dateEnd'];
$mission = $_POST['mission'];
$compet = $_POST['compet'];
$indemn = intval($_POST['indemn']);
$available = intval($_POST['available']);
$remark = $_POST['remark'];

$studEmail = null;
$tutorEmail = null;

$civility = intval($_POST['civility']);
$title = $_POST['title'];
$lastName = $_POST['lastName'];
$firstName = $_POST['firstName'];
$email = $_POST['email'];
$workPhone = $_POST['workPhone'];
$mobilePhone = $_POST['mobilePhone'];

$inChargeOf = intval($_POST['inChargeOf']);
$status = intval($_POST['status']);

// Si $traineeship vaut false, il s'agit de la création d'un nouveau stage
if ($traineeship <= 0) {
		
	if($inChargeOf > 0) {
		// On décide d'assigner un responsable déjà existant au stage
		$inChargeOfId = $inChargeOf;
	} 
	else if (!EInChargeOfManager::getInstance()->findInChargeOfByFirstLastNameAndCivility($firstName, $lastName, $civility)) {
		// Si le responsable n'existe pas, on le crée
		$inChargeOfId = EInChargeOfManager::getInstance()->addInChargeOf($civility, $lastName, $firstName, $title, $email, $workPhone, $mobilePhone);
	}
	else {
		// TO DO : prévenir que le tente de créer un nouveau responsable qui existe déjà dans la base
	}
	
	$trainId = EAppManager::getInstance()->addTraineeship($enterprise, $subject, $dateBeg, $dateEnd, $mission, $compet, $indemn, $available, $remark, $status, $studEmail, $tutorEmail, $inChargeOfId);
	
	if($trainId != false) {
		foreach ($sectors as $sect) {
			ESectorManager::getInstance()->addSectorHasTraineeships($trainId, $sect);
		};
	}
	else {
		// Une erreure est survenu lors de la création du stage
	}
	
}
// Sinon il s'agit de la modification d'un stage existant
else {
	
	if($inChargeOf != false) {
		// On décide d'assigner un responsable déjà existant au stage
		$inChargeOfId = $inChargeOf;
	} 
	else if (!EInChargeOfManager::getInstance()->findInChargeOfByFirstLastNameAndCivility($firstName, $lastName, $civility)) {
		// Si le responsable n'existe pas, on le crée
		$inChargeOfId = EInChargeOfManager::getInstance()->addInChargeOf($civility, $lastName, $firstName, $title, $email, $workPhone, $mobilePhone);
	}
	
	
	$trainUpdate = EAppManager::getInstance()->updateTraineeship($traineeship, $enterprise, $subject, $dateBeg, $dateEnd, $mission, $compet, $indemn, $available, $remark, $status, $studEmail, $tutorEmail, $inChargeOfId);
	
	if ($trainUpdate) {
		ESectorManager::getInstance()->deleteAllSectorsByTrainId($traineeship);
		
		foreach ($sectors as $sect) {
			ESectorManager::getInstance()->addSectorHasTraineeships($traineeship, $sect);
		};
	}
	
}



?>