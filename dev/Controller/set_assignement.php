<?php
/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

require_once '../Model/inc.all.php';
require_once '../Model/sendMail.php';

$delete = $_POST['delete'];

$traineeship = $_POST['traineeship'];
$subject = $_POST['subject'];

$emailStudent = $_POST['emailStudent'];
$emailTutor = $_POST['emailTutor'];

$studentName = $_POST['studentName'];
$tutorName = $_POST['tutorName'];

$msgStudent = $_POST['msgStudent'];
$msgTutor = $_POST['msgTutor'];

if ($delete == 'true'){
	$assignement = EAppManager::getInstance()->deleteAssignement($traineeship);
	
	$jsn = json_encode($assignement, JSON_UNESCAPED_UNICODE); // JSON_UNESCAPED_UNICODE nécessaire !
	
	if ($jsn == false) {
		$code = json_last_error();
		echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json ('.$code.'"}';
		exit();
	}
	//Si j'arrive ici, ouf... c'est tout bon
	echo '{"ReturnCode": 0, "Data": '. $jsn .'}'; // ne pas mettre utf8_encode() !!
	
	exit;
}

if (EAppManager::getInstance()->traineeshipExistInAssignement($traineeship))
	$assignement = EAppManager::getInstance()->updateAssignement($emailStudent, $emailTutor, $traineeship);
else
	$assignement = EAppManager::getInstance()->addAssignement($emailStudent, $emailTutor, $traineeship);
if($assignement != false) {
	sendEmail($subject, $emailStudent, $msgStudent);
	sendEmail($subject, $emailTutor, $msgTutor);
}

$jsn = json_encode($assignement, JSON_UNESCAPED_UNICODE); // JSON_UNESCAPED_UNICODE nécessaire !

if ($jsn == false) {
	$code = json_last_error();
	echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json ('.$code.'"}';
	exit();
}
//Si j'arrive ici, ouf... c'est tout bon
echo '{"ReturnCode": 0, "Data": '. $jsn .'}'; // ne pas mettre utf8_encode() !!
?>