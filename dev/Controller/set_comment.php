<?php
require_once '../Model/inc.all.php';
require_once '../Model/sendMail.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$action = '';
if(isset($_POST['action'])) {
	$action = $_POST['action'];
}
$id = -1;
if (isset($_POST['id']))
	$id = intval($_POST['id']);

if ($id > 0){
	if ($action == 'accept'){
		ECommentManager::getInstance()->acceptComment($id);
		echo '{"ReturnCode": 0, "Data": "accept"}';
		sendEmail($subject, $emailTo, $msg);
		exit();	
	}
	else
	if ($action == 'refuse'){
		ECommentManager::getInstance()->refuseComment($id);
		echo '{"ReturnCode": 0, "Data": "refuse"}';
		sendEmail($subject, $emailTo, $msg);
		exit();
	}
}
// Si on arrive ici, c'est pas bon
echo '{ "ReturnCode" : 2, "Message" : "paramètre invalide."}';
