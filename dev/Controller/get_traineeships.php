<?php

/*
 * Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017
 */
require_once '../Model/inc.all.php';

// Nécessaire lorsqu'on retourne du json
header ( 'Content-Type: application/json' );
$IdEnter = false;
$traineeships = false;

if (isset($_POST['IdEnter'])) {
	$IdEnter = $_POST ['IdEnter'];
	if($_POST['status'] == 'toCome')
		$traineeships = EAppManager::getInstance()->getAllTraineeToComeFromEnterprise($IdEnter);
	else if($_POST['status'] == 'finished')
		$traineeships = EAppManager::getInstance()->getAllFinishedTraineeFromEnterprise($IdEnter);
}

else if (isset($_POST['traineeship'])) {
	$traineeships = EAppManager::getInstance()->findTraineeshipById($_POST['traineeship']);
}

else if(isset($_POST['search'])) {
	$traineeships = EAppManager::getInstance()->searchOffers($_POST['search']);
}

else {
	$traineeships = EAppManager::getInstance()->getAllTraineeToCome();
}
	
if ($traineeships === false) {
	echo '{ "ReturnCode" : 2, "Message" : "Un problème de récupération des données des stages"}';
	exit();
}
	
$jsn = json_encode ( $traineeships, JSON_UNESCAPED_UNICODE ); // JSON_UNESCAPED_UNICODE nécessaire !
	
if ($jsn == false) {
	$code = json_last_error();
	echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json (' . $code . '"}';
	exit();
}

// Si j'arrive ici, ouf... c'est tout bon
echo '{"ReturnCode": 0, "Data": ' . $jsn . '}'; // ne pas mettre utf8_encode() !!

?>