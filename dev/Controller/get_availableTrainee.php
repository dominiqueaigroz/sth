<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
* Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
* Version: 1.0.0
* Date: 25.11.2016
* Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

require_once '../Model/inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$traineeships = EAppManager::getInstance()->getAllTraineeToCome();

$tutor = '';
if (isset($_POST['tutor'])){
	$tutor = EUserManager::getInstance()->selectAllTutor();
	$jsn = json_encode($tutor, JSON_UNESCAPED_UNICODE); // JSON_UNESCAPED_UNICODE nécessaire !
	$tutor = '"DataTutor": ' . $jsn; // /!\ NE PAS ENCODER EN UTF-8 /!\
}

$student = '';
if (isset($_POST['student'])){
	$student = EUserManager::getInstance()->selectAllStudentNotInTrainee();
	$jsn = json_encode($student, JSON_UNESCAPED_UNICODE); // JSON_UNESCAPED_UNICODE nécessaire !
	$student = '"DataStudent": ' . $jsn; // /!\ NE PAS ENCODER EN UTF-8 /!\
}

if ($traineeships === false) {
	echo '{ "ReturnCode" : 2, "Message" : "Un problème de récupération des données de getAllTraineeToCome()"}';
	exit();
}

if ($tutor === false) {
	echo '{ "ReturnCode" : 2, "Message" : "Un problème de récupération des données de selectAllTutor()"}';
	exit();
}

if ($student === false) {
	echo '{ "ReturnCode" : 2, "Message" : "Un problème de récupération des données de selectAllStudent()"}';
	exit();
}

$jsn = json_encode($traineeships, JSON_UNESCAPED_UNICODE); // JSON_UNESCAPED_UNICODE nécessaire !

if ($jsn == false) {
	$code = json_last_error();
	echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json ('.$code.'"}';
	exit();
}
//Si j'arrive ici, ouf... c'est tout bon
echo '{"ReturnCode": 0, "Data": {"DataTrainee": '. $jsn . ', ' . $tutor . ', ' . $student .'}}'; // ne pas mettre utf8_encode() !!
exit();