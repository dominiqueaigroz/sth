<?php
/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

require_once '../Model/inc.all.php';

$idTrain = $_POST['idTraineeship'];
$arCrit = $_POST['criteria'];
$indexCrit = 1;

foreach ($arCrit as $crit) {
	ECriterionManager::getInstance()->setSatisfactionsForCriteria($idTrain, $indexCrit, $crit);
	$indexCrit++;
}

ECommentManager::getInstance()->addComment($_POST['comment'], $idTrain);



?>