<?php
require_once '../Model/inc.all.php';

$domain = '';

if (isset($_POST['idEnterprise'])) {
	$idEnterprise = $_POST['idEnterprise'];
	$idDomain = EDomainManager::getInstance()->getIdDomainByIdEnterprise($idEnterprise);
	$domain = EDomainManager::getInstance()->getDomainById($idDomain);
}

echo $domain; // Mettre echo au lieu de return !!

?>