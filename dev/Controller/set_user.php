<?php
session_start();
require_once '../Model/inc.all.php';


if (isset($_POST['email'])){
	ESession::setEmail($_POST['email']);
}

if (isset($_POST['role'])){
	$role = $_POST['role'];
	if ($role == 1)
		ESession::setRole(EC_ROLE_USER);
	else if ($role == 3)
		ESession::setRole(EC_ROLE_ADMIN);
}

if (isset($_POST['name'])){
	ESession::setName($_POST['name']);
}

if (isset($_POST['img'])){
	ESession::setImg($_POST['img']);
}
	

echo '{ "ReturnCode": 0}';
