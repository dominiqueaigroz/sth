<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

class ETraineeship implements JsonSerializable {

    /**
     * @brief	Class Constructor
     */
    public function __construct($inId = -1, $inEnter = null, $inSubj = "", $inDateBeg = "", $inDateEnd = "", $inMission = "", $inCompet = "", $inIndemn = "", $inAvailab = null, $inRemark = "", $inStatus = "", $inStud = "", $inTutor = "", $inChargeOf = "") {
        $this->id = $inId;
        $this->enter = $inEnter;
        $this->subj = $inSubj;
        $this->dateBeg = $inDateBeg;
        $this->dateEnd = $inDateEnd;
        $this->mission = $inMission;
        $this->compet = $inCompet;
        $this->indemn = $inIndemn;
        $this->availab = $inAvailab;
        $this->remark = $inRemark;
        $this->status = $inStatus;
        $this->stud = $inStud;
        $this->tutor = $inTutor;
        $this->chargeOf = $inChargeOf;
        $this->sectors = array();
        $this->criteria = array();
        $this->nameTutor = '';
        $this->duration = "";
        $this->comm = "";
    }
    public function jsonSerialize() {
    	return get_object_vars($this);
    }
    
    
    /**
     * @brief	Est-ce que cet objet est valide
     * @return  True si valide, autrement false
     */
    public function isValid() {
        return ($this->id == -1) ? false : true;
    }

    /**
     * @brief	Getter
     * @return  L'identifiant unique du stage
     */
    public function getId() {
    	return $this->id;
    }
    
    /**
     * @brief	Getter
     * @return  L'entreprise qui propose le stage
     */
    public function getEnterprise() {
    	return $this->enter;
    }
    
    /**
     * @brief	Getter
     * @return  Le nom du stage
     */
    public function getSubject() {
        return $this->subj;
    } 
    
    /**
     * @brief	Getter
     * @return  La date du début du stage
     */
    public function getDateBegin() {
    	return $this->dateBeg;
    }
    
    /**
     * @brief	Getter
     * @return  La date de fin du stage
     */
    public function getDateEnd() {
    	return $this->dateEnd;
    }
    
    /**
     * @brief	Getter
     * @return  La mission du stage
     */
    public function getMission() {
    	return $this->mission;
    }
    /**
     * @brief	Getter
     * @return  Les compétences requises pour le stage
     */
    public function getCompetence() {
    	return $this->compet;
    }
    /**
     * @brief	Getter
     * @return  Les indemnités du stage
     */
    public function getIndemnity() {
    	return $this->indemn;
    }
    /**
     * @brief	Getter
     * @return  La disponibilité du stage
     */
    public function getAvailability() {
    	return $this->availab;
    }
    /**
     * @brief	Getter
     * @return  Les remarques sur le stage
     */
    public function getRemark() {
    	return $this->remark;
    }
    /**
     * @brief	Getter
     * @return  L'état actuel du stage
     */
    public function getStatus() {
    	return $this->status;
    }
    /**
     * @brief	Getter
     * @return  L'élève qui participe au stage
     */
    public function getStudent() {
    	return $this->stud;
    }
    /**
     * @brief	Getter
     * @return  Le tuteur qui est assigné au stage
     */
    public function getTutor() {
    	return $this->tutor;
    }
    /**
     * @brief	Getter
     * @return  La personne de contact du stage
     */
    public function getChargeOf() {
    	return $this->chargeOf;
    }
    
    public function getNameTutor(){
    	return $this->nameTutor;
    }
    
    /**
     * @brief	Getter
     * @return  La référence sur le tableau des secteurs
     */
    public function & getSectors() {
    	return $this->sectors;
    }
    /**
     * @brief	Getter
     * @return	La référence sur la durée du stage
     */
    public function & getDuration() {
    	return $this->duration;
    }
    /**
     * @brief	Getter
     * @return	La référence sur le tableau des critères
     */
    public function & getCriteria() {
    	return $this->criteria;
    }
    /**
     * @brief	Getter
     * @return Le commentaire du stage
     */
    public function & getComm() {
    	return $this->comm;
    }
    
    /** @brief L'identifiant unique provenant de la base de données */
    private $id;

    /** @brief L'entreprise qui propose le stage */
    private $enter;
    
    /** @brief Le sujet du stage */
    private $subj;

    /** @brief Un tableau des secteurs appartenant au stage */
    private $sectors;

    /** @brief La date du début du stage */
    private $dateBeg;

    /** @brief La date de fin du stage */
    private $dateEnd;
    
    /** @brief La mission du stage */
    private $mission;

    /** @brief Les compétences requises pour le stage */
    private $compet;
    
    /** @brief Les indemnités du stage */
    private $indemn;
    
    /** @brief La disponibilité du stage */
    private $availab;
    
    /** @brief La remarque du stage */
    private $remark;
    
    /** @brief L'état actuel du stage */
    private $status;
    
    /** @brief L'élève qui participe au stage */
    private $stud;
    
    /** @brief Le tuteur qui est assigné au stage */
    private $tutor;
    
    /** @brief La personne de contact du stage */
    private $chargeOf;


    public $nameTutor;

    /** @brief La durée du stage en jour*/
    public $duration;

    /** @brief Un tableau des critères sur le stage */
    private $criteria;
    
    /**  @brief Le commentaire du stage */
    public $comm;

}
