<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

/**
 * Classe des inChargeOf
 * @author Jean-Daniel
 *
 */
class EInChargeOf implements JsonSerializable {

	
	/**
	 * @brief	Class Constructor
	 */
	public function __construct($inId = -1, $inCivility = "", $inLastName = "", $inFirstName = "", $inTitle = "", $inEmail = "", $inWorkPhone = "", $inMobilePhone = "") {
		$this->id = $inId;
		$this->civility = $inCivility;
		$this->lastName = $inLastName;
		$this->firstName = $inFirstName;
		$this->title = $inTitle;
		$this->email = $inEmail;
		$this->workPhone = $inWorkPhone;
		$this->mobilePhone = $inMobilePhone;
	}


	public function jsonSerialize() {
		return get_object_vars($this);
	}

	/**
	 * @brief	Est-ce que cet objet est valide
	 * @return  True si valide, autrement false
	 */
	public function isValid() {
		return ($this->id == -1) ? false : true;
	}

	/**
	 * @brief	Getter
	 * @return  La civilité
	 */
	public function getCivility() {
		return $this->civility;
	}

	/**
	 * @brief	Getter
	 * @return  Le nom de famille
	 */
	public function getLastName() {
		return $this->lastName;
	}

	/**
	 * @brief	Getter
	 * @return  Le prénom
	 */
	public function getFirstName() {
		return $this->firstName;
	}

	/**
	 * @brief	Getter
	 * @return  Le titre
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @brief	Getter
	 * @return  L'email
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @brief	Getter
	 * @return  Le téléphone du travail
	 */
	public function getWorkPhone() {
		return $this->workPhone;
	}

	/**
	 * @brief	Getter
	 * @return  Le téléphone personnel
	 */
	public function getMobilePhone() {
		return $this->mobilePhone;
	}

	/** @brief La civilité de la personne */
	private $civility;

	/** @brief Le nom de famille de la personne */
	private $lastName;

	/** @brief Le prénom de la personne */
	private $firstName;

	/** @brief Le poste/titre de la personne */
	private $title;

	/** @brief L'email de la personne */
	private $email;

	/** @brief Le numéro de téléphone de travail de la personne */
	private $workPhone;

	/** @brief Le numéro mobile de la personne */
	private $mobilePhone;

}