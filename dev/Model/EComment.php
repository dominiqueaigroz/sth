<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
* Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
* Version: 1.0.0
* Date: 25.11.2016
* Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

class EComment implements JsonSerializable {

	/**
	 * @brief	Class Constructor
	 */
	public function __construct($inId = -1, $inComment = "", $inState = -1, $inTrain = -1) {
		$this->id = $inId;
		$this->comment = $inComment;
		$this->states = $inState;
		$this->train = $inTrain;
	}

	public function jsonSerialize() {
		return get_object_vars($this);
	}
	
	/**
	 * @brief	Est-ce que cet objet est valide
	 * @return  True si valide, autrement false
	 */
	public function isValid() {
		return ($inId == -1) ? false : true;
	}

	/**
	 * @brief	Getter
	 * @return  L'ID du commentaire
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * @brief	Getter
	 * @return  Le nom du commentaire
	 */
	public function getComment() {
		return $this->comment;
	}

	/**
	 * @brief	Getter
	 * @return  L'ID de l'état
	 */
	public function getState() {
		return $this->states;
	}
	
	/** @brief L'identifiant unique provenant de la base de données */
	private $id;

	/** @brief Le nom du commentaire */
	private $comment;
	
	/** @brief L'état du commentaire */
	private $states;

	
	/** @brief L'ID du stage */
	private $train;
}
