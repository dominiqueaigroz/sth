<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

class EUser implements JsonSerializable {

    /**
     * @brief	Class Constructor
     */
    public function __construct($inEmail = "", $inLastName = "", $inFirstName = "", $inRole=1, $inCivility=1) {
        $this->email = $inEmail;
        $this->lastName = $inLastName;
        $this->firstName = $inFirstName;        
        $this->role = $inRole;
        $this->civility = $inCivility;
        $this->nbrStudent = 0;
        $this->student = array();
    }

    public function jsonSerialize() {
    	return get_object_vars($this);
    }

    
    /**
     * @brief	Getter
     * @return  L'email
     */
    public function getEmail() {
        return $this->email;
    }
    
    /**
     * @brief	Getter
     * @return  Le username
     */
    public function getFirstName() {
    	return $this->firstName;
    }
    
    /**
     * @brief	Getter
     * @return  L'identifiant unique
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     * @brief	Getter
     * @return  Le role
     */
    public function getRole() {
        return $this->role;
    }
    
    public function getTrainee(){
    	return $this->traineeship;
    }
    
    /**
     * @brief	Getter
     * @return 	La civilité
     */
    public function getCivility() {
    	return $this->civility;
    }
    
    /**
     * @brief	Getter
     * @return 	Le nombre d'élèves
     */
    public function getNbrStudent() {
    	return $this->nbrStudent;
    }
    
    /**
     * @brief	Getter
     * @return 	Les élèves
     */
    public function getStudent(){
    	return $this->student;
    }

    /** @brief L'email de l'utilisateur */
    private $email;
    
    /** @brief Le prénom de l'utilisateur */
    private $firstName;
    
    /** @brief Le nom de famille de l'utilisateur */
    private $lastName;
    
    /** @brief Le role de l'utilisateur */
    private $role;
    
    /** @brief Les stages de l'utilisateur */
    private $traineeship;
    
    /** @brief La civilité de l'utilisateur */
    private $civility;
    
    public $student;
    
    public $nbrStudent;
}
