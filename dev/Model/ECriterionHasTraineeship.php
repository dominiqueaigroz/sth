<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
* Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
* Version: 1.0.0
* Date: 25.11.2016
* Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

class ECriterionHasTraineeship implements JsonSerializable {

	/**
	 * @brief	Class Constructor
	 */
	public function __construct($ChTCrit = "", $ChTSati = "") {
		$this->idCrit = $ChTCrit;
		$this->idSatisf = $ChTSati;
	}

	public function jsonSerialize() {
		return get_object_vars($this);
	}
	
	/** @brief L'ID du critère */
	private $idCrit;

	/** @brief L'ID du stage */
	private $idSatisf;

}
