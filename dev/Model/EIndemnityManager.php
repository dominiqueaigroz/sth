<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
* Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
* Version: 1.0.0
* Date: 25.11.2016
* Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

require_once '../Model/EIndemnity.php';


class EIndemnityManager {
	private static $objInstance;
	
	/**
	 * @brief	Class Constructor - Create a new EIndemnityManager if one doesn't exist
	 * 			Set to private so no-one can create a new instance via ' = new EIndemnityManager();'
	 */
	private function __construct() {
		$this->indemnity = array();
	}
	
	/** @brief Contient le tableau des EIndemnity */
	private $indemnity;
	
	/**
	 * @brief	Retourne notre instance ou la crée
	 * @return $objInstance;
	 */
	public static function getInstance() {
		if (!self::$objInstance) {
			try {
	
				self::$objInstance = new EIndemnityManager();
			} catch (Exception $e) {
				echo "EIndemnityManager Error: " . $e;
			}
		}
		return self::$objInstance;
	}

	
	/**
	 * Charge toutes les disponibilités
	 * @return Le tableau des EIndemnity | false si une erreur se produit
	 */
	public function loadAllIndemnities() {
		$sql = 'SELECT * FROM INDEMNITIES ORDER BY CODE';
		try {
			$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute();
	
			while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
				//
				$in = new EIndemnity($row['CODE'], $row['LABEL']);
				array_push($this->indemnity, $in);
			} #end while
	
		} catch (PDOExeception $e) {
			echo "EIndemnityManager:loadAllIndemnities Error : " . $e->getMessage();
			return false;
		}
		// Return le tableau de tout les domaines
		return $this->indemnity;
	}
	
	public function getAllIndemnities() {
		return $this->indemnity;
	}
}
