<?php
/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017 */
require_once '../Model/EInChargeOf.php';


class EInChargeOfManager {

private static $objInstance;

/**
 * @brief	Class Constructor - Create a new EAppManager if one doesn't exist
 * 	Set to private so no-one can create a new instance via ' = new EInChargeOfManager();'
 */
private function __construct() {
	$this->inChargeOf = array();
}

/** @brief Contient le tableau des EInChargeOf */
private $inChargeOf;

/**
 * @brief	Retourne notre instance ou la crée
 * @return $objInstance;
 */
public static function getInstance() {
	if (!self::$objInstance) {
		try {

			self::$objInstance = new EInChargeOfManager();
		} catch (Exception $e) {
			echo "EInChargeOfManager Error: " . $e;
		}
	}
	return self::$objInstance;
}

/**
 * Charge tout les responsables des stages
 * @return Le tableau des EInChargeOf | false si une erreur se produit
 */
public function loadAllInChargeOf() {
	$sql = 'SELECT * FROM IN_CHARGE_OF';
	try {
		$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
		$stmt->execute();

		while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$civility = ECivilityManager::getInstance()->getCivilityLabelByCode($row['INCHARGEOF_CIST_CODE']);
			$ico = new EInChargeOf($row['IN_CHARGE_OF_PK'], $civility, $row['LAST_NAME'], $row['FIRST_NAME'], $row['TITLE'], $row['EMAIL'], $row['WORK_PHONE'], $row['MOBILE_PHONE']);
			array_push($this->inChargeOf, $ico);
		} #end while

	} catch (PDOExeception $e) {
		echo "EInChargeOfManager:loadAllInChargeOf Error : " . $e->getMessage();
		return false;
	}
	// Return le tableau de tout les responsables des stages
	return $this->inChargeOf;

}

    
    /**
     * Recherche si une personne en charge existe dans la base de données en fonction de son ID
     * @param	integer	L'ID de la personne en charge à rechercher
     * @return	boolean	True si la personne en charge existe | sinon false
     */
    public function inChargeOfExistInDB($inId) {
    	$sql = 'SELECT * FROM IN_CHARGE_OF WHERE IN_CHARGE_OF_PK = :id';
    	try {
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute(array(':id' => $inId));
    
    		$result = $stmt->fetchAll();
    		return (count($result) > 0) ? true : false;
    	} catch (PDOException $e) {
    		echo "EInChargeOfManager:inChargeOfExistInDB Error: " . $e->getMessage();
    		return false;
    	}
    	// J'ai pas trouvé l'entreprise
    	return false;
    }
    
    # end method

    
    /**
     * Récupère un responsable de stage via son ID
     * @param string $inId	L'ID du responsable du stage que l'on récupère
     * @return EInChargeOf	Le responsable du stage de type ETraineeship | false si il n'existe pas
     */
    public function getInChargeOfById($inId) {
    	$sql = 'SELECT * FROM IN_CHARGE_OF WHERE IN_CHARGE_OF_PK = :i';
    	try {
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute(array(':i' => $inId));
    
    		$result = $stmt->fetchAll();
    		if (count($result) > 0) {
    			// Création du tableau inChargeOf avec les données provenant de la base de données
    			$civility = ECivilityManager::getInstance()->getCivilityLabelByCode($result[0]['INCHARGEOF_CIST_CODE']);
    			$in = new EInChargeOf($result[0]['IN_CHARGE_OF_PK'], $civility, $result[0]['LAST_NAME'], $result[0]['FIRST_NAME'], $result[0]['TITLE'], $result[0]['EMAIL'], $result[0]['WORK_PHONE'], $result[0]['MOBILE_PHONE']);
				
    			array_push($this->inChargeOf, $in);
    			return $in;
    		}
    	} catch (PDOException $e) {
    		echo "EAppManager::getInChargeOfById Error: " . $e->getMessage();
    		return false;
    	}
    	// Je n'ai pas trouvé le responsable
    	return false;
    }
    
/**
 * Recherche une personne en charge spécifique dans la base de donées en fonction de sont prénom
 * @param	string	Le nom de la personne en charge que l'on cherche
 * @return	boolean	Retourne une personne en charge de type EInChargeOf | sinon false
 */
public function findInChargeOfByFirstName($inFirstName) {
	// On cherche si l'entreprise existe dans le tableau
	foreach ($this->inChargeOf as $in) {
		if ($inFirstName == $in->getFirstName())
			return $in;
	}# end foreach

	$sql = 'SELECT * FROM IN_CHARGE_OF WHERE FIRST_NAME LIKE :in';
	try {
		$stmt = EDatabase::prepare($sql);
		$stmt->execute(array(':in' => $inFirstName));

		$result = $stmt->fetchAll();
		if (count($result) > 0) {
			// Création de l'entreprise avec les données provenant de la base de données
			$in = new EInChargeOf($result[0]['IN_CHARGE_OF_PK'], $result[0]['INCHARGEOF_CIST_CODE'], $result[0]['LAST_NAME'], $result[0]['FIRST_NAME'], $result[0]['TITLE'], $result[0]['WORKPHONE'], $result[0]['MOBILEPHONE']);
			array_push($this->inChargeOf, $in);
			return $in;
		} #end while
	} catch (PDOException $e) {
		echo "EInChargeOfManager:findEntepriseByFirstName Error: " . $e->getMessage();
		return false;
	}
	// J'ai pas trouvé l'entreprise
	 
	return false;
}

/**
 * Recherche une personne en charge spécifique dans la base de donées en fonction de sont nom de famille
 * @param	string	Le nom de la personne en charge que l'on cherche
 * @return	boolean	Retourne une personne en charge de type EInChargeOf | sinon false
 */
public function findInChargeOfByLastName($inLastName) {
	// On cherche si l'entreprise existe dans le tableau
	foreach ($this->inChargeOf as $in) {
		if ($inLastName == $in->getLastName())
			return $in;
	}# end foreach

	$sql = 'SELECT * FROM IN_CHARGE_OF WHERE LAST_NAME LIKE :in';
	try {
		$stmt = EDatabase::prepare($sql);
		$stmt->execute(array(':in' => $inFirstName));

		$result = $stmt->fetchAll();
		if (count($result) > 0) {
			// Création de l'entreprise avec les données provenant de la base de données
			$in = new EInChargeOf($result[0]['IN_CHARGE_OF_PK'], $result[0]['INCHARGEOF_CIST_CODE'], $result[0]['LAST_NAME'], $result[0]['FIRST_NAME'], $result[0]['TITLE'], $result[0]['WORKPHONE'], $result[0]['MOBILEPHONE']);
			array_push($this->inChargeOf, $in);
			return $in;
		} #end while
	} catch (PDOException $e) {
		echo "EInChargeOfManager:findEntepriseByFirstName Error: " . $e->getMessage();
		return false;
	}
	// J'ai pas trouvé l'entreprise
	return false;
}

/**
 * Recherche une personne en charge spécifique dans la base de donées en fonction de son prénom et de son nom
 * @param string $inFirstName	Le prénom de la personne en charge que l'on cherche
 * @param string $inLastName	Le nom de la personne en charge que l'on cherche
 * @return boolean	Retourne une personne en charge de type EInChargeOf | sinon false
 */
public function findInChargeOfByFirstLastNameAndCivility($inFirstName, $inLastName, $inCivility) {
	$sql = 'SELECT * FROM IN_CHARGE_OF WHERE FIRST_NAME LIKE :fn AND LAST_NAME = :ln AND INCHARGEOF_CIST_CODE = :c';
	try {
		$stmt = EDatabase::prepare($sql);
		$stmt->execute(array(':fn' => $inFirstName, ':ln' => $inLastName, ':c' => $inCivility));

		$result = $stmt->fetchAll();
		if (count($result) > 0) {
			// Le responsable existe déjà dans la base
			return true;
		} #end while
		else {
			// Le responsable n'existe pas
			return false;
		}
	} catch (PDOException $e) {
		echo "EInChargeOfManager:findInChargeOfByFirstAndLastName Error: " . $e->getMessage();
		return false;
	}
	// Je n'ai pas trouvé le responsable de stage
	return false;
}

/** Cette fonction permet d'ajouter un responsable des stages dans la base de données
 * @$icoInId		L'ID du responsable
 * @$inCivility		La civilité du responsable
 * @$inLastName		Le nom de famille du responsable
 * @$inFirstName	Le prénom du responsable
 * @$inTitle		Le titre du responsable au sein de l'entreprise
 * @$inEmail		L'email du responsable
 * @$inWorkPhone	Le téléphone de travail du responsable
 * @$inMobilePhone	Le téléphone personnel du responsable
 * @return l'objet EInChargeOf créé | false si une erreur se produit
 */
public function addInChargeOf($inCivility, $inLastName , $inFirstName , $inTitle , $inEmail , $inWorkPhone , $inMobilePhone) {
	$in = false;
	$inId = null;
	$sql = 'INSERT INTO IN_CHARGE_OF (INCHARGEOF_CIST_CODE, LAST_NAME, FIRST_NAME, TITLE, EMAIL, WORK_PHONE, MOBILE_PHONE) values (:c, :ln, :fn, :t, :e, :wp, :mp)';
	try {
		$stmt = EDatabase::prepare($sql);
		$stmt->execute(array( ':c' => $inCivility, ':ln' =>  $inLastName, ':fn' => $inFirstName, ':t' => $inTitle, ':e' => $inEmail, ':wp' => $inWorkPhone, ':mp' => $inMobilePhone));
		 
		// Requete SQL qui permet de récupèrer l'ID du nouveau responsable
		$sql = 'SELECT IN_CHARGE_OF_PK FROM IN_CHARGE_OF ORDER BY IN_CHARGE_OF_PK DESC LIMIT 1';
		$stmt = EDatabase::prepare($sql);
		$stmt->execute();
		$id = $stmt->fetchAll();
		
		// j'ai réussi à ajouter le responsable du stage
		return $id[0][0];
	} catch (PDOException $e) {
		echo "EInChargeOfManager:addInChargeOf Error: " . $e->getMessage();
		return false;
	}
	
}

/**
 * Récupère l'ID du responsable du stage
 * @param int $inId	L'ID du stage
 * @return L'ID du responsable du stage | false si une erreure c'est produite | null si aucun responsable n'est assigné
 */
public function getIdInChargeOfByIdTrain($inId) {
	$sql = "SELECT TRAI_INCHARGEOF_FK FROM TRAINEESHIPS WHERE TRAINEESHIPS_PK = :i";
	
	try {
		$stmt = EDatabase::prepare($sql);
		$stmt->execute(array(':i' => $inId));
		
		$result = $stmt->fetchAll();
		
		return $result[0][0];
	} catch (PDOException $e) {
		echo "EInChargeOfManager:getIdInChargeOfByIdTrain Error: " . $e->getMessage();
		return false;
	}
}



/**
 * Cette fonction sert à modifier une personne en charge
 * @param 	integer $inId			L'ID de la personne en charge
 * @param 	integer $inCivility		La civilité de la personne en charge
 * @param 	string $inLastName		Le nom de famille de la personne en charge
 * @param 	string $inFirstName		Le prénom de la personne en charge
 * @param 	string $inTitle			Le titre/poste de la personne en charge
 * @param 	string $inEmail			L'email de la personne en charge
 * @param 	string $inWorkPhone		Le numéro de téléphone du travail de la personne en charge
 * @param 	string $inMobilePhone	Le numéro de téléphone privé de la personne en charge
 * @return 	boolean					L'objet EInChargeOf modifié | false si une erreur se produit
 */
public function updateInChargeOf($inId, $inCivility, $inLastName, $inFirstName, $inTitle, $inEmail ,$inWorkPhone, $inMobilePhone) {
	$inCreate = true;
	if ($this->enterpriseExistInDB($inId)){
		$inCreate = false;
		$sql = 'UPDATE IN_CHARGE_OF SET INCHARGEOF_CIST_CODE = :c, LAST_NAME = :ln, FIRST_NAME = :fn, TITLE = :t, EMAIL = :e, WORK_PHONE = :wp, MOBILE_PHONE = :mp WHERE IN_CHARGE_OF_PK = :id';
		try{
			$sth = EDatabase::prepare($sql);
			$sth->execute(array( ':id' => $inId, ':c' => $inCivility, ':ln' => $inLastName, ':fn' => $inFirstName, ':t' => $inTitle, ':e' => $inEmail, ':wp' => $inWorkPhone, ':mp' => $inMobilePhone));
		}catch(PDOException  $e ){
			echo "EAppManager:updateInChargeOf Error: <br/>".$e->getMessage();
			$inCreate = true;
			return false;
		}
		echo 'l\'entreprise a bien été modifiée';
	}
	// S'il existe pas et que je dois le créer, go !!!
	if ($inCreate){
		return $this->addInChargeOf($inCivility, $inLastName, $inFirstName, $inTitle, $inEmail ,$inWorkPhone, $inWorkPhone, $inMobilePhone);
	}
}

public function getAllInChargeOf() {
	return $this->inChargeOf;
}

}