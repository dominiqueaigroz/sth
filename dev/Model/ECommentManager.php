<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
* Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
* Version: 1.0.0
* Date: 25.11.2016
* Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

require_once '../Model/EComment.php';

/**
 * @brief	Helper class pour gérer les commentaires
 * @author 	jean-daniel.knz@eduge.ch
 * @remark
 * @version     1.0.0
 */
class ECommentManager {
	private static $objInstance;
	
	/**
	 * @brief	Class Constructor - Create a new EUserManager if one doesn't exist
	 * 			Set to private so no-one can create a new instance via ' = new EUserManager();'
	 */
	private function __construct() {
		$this->comment = array();
	}
	
	/** @brief Contient le tableau des EComment */
	private $comment;
	
	/**
	 * @brief	Retourne notre instance ou la crée
	 * @return $objInstance;
	 */
	public static function getInstance() {
		if (!self::$objInstance) {
			try {
	
				self::$objInstance = new ECommentManager();
			} catch (Exception $e) {
				echo "ECommentManager Error: " . $e;
			}
		}
		return self::$objInstance;
	}
	
	/**
	 * Charge tout les commentaires
	 * @return Le tableau des EComment | false si une erreur se produit
	 */
	public function loadAllComments() {
		$sql = 'SELECT * FROM COMMENTS';
		try {
			$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute();
	
			while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
				//
				$in = new EComment($row['COMMENTS_PK'], $row['COMMENT'], $row['COMM_STATE_CODE'], $row['COMM_TRAI_FK']);
				array_push($this->comment, $in);
			} #end while
	
		} catch (PDOExeception $e) {
			echo "ECommentManager:loadAllComments Error : " . $e->getMessage();
			return false;
		}
		// Return le tableau de tout les commentaires
		return $this->comment;
	}
	
	
	/**
	 * Récupère un commentaire spécifique dans la base en fonction de son id
	 * @param string $inId		L'id du commentaire que l'on cherche
	 * @return string $comment	Retourne le commentaire si il a été trouvé | sinon return false
	 */
	public function getCommentById($inId) {
		$sql = 'SELECT COMMENT FROM COMMENTS WHERE COMMENTS_PK = :id';
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array(':id' => $inId));
	
			$result = $stmt->fetchAll();
			if(empty($result))
				return "Indéfinis";
			else {
				$comment = $result[0][0];
				return $comment;
			}
		} catch (PDOException $e) {
			echo "ECommentManager:getCommentById Error: " . $e->getMessage();
			return false;
		}
		// Je n'ai pas trouvé le commentaire
		return false;
	}
	/**
	 * 
	 * @param unknown $idTrain
	 * @return unknown|boolean
	 */
	public function getIdCommentByIdTraineeship($idTrain) {
		$sql = 'SELECT COMMENTS_PK FROM COMMENTS WHERE COMM_TRAI_FK = 11';
		
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array(':id' => $idTrain));
			
			$result = $stmt->fetchAll();
			
			$idComm = $result[0][0];
				return $idComm;
			
		} catch (Exception $e) {
			echo "ECommentManager:getIdCommentByIdTraineeship Error: " . $e->getMessage();
			return false;
		}
	}
	
	
	/**
	 * Cette fonction permet d'ajouter un commentaire à la base de données
	 * @param string $inComment	le commentaire à ajouter
	 * @param string $inTrain	l'ID du stage
	 * @return EComment $in		L'objet EComment créé | false si une erreur se produit
	 */
	public function addComment($inComment, $inTrain) {
		
		$commExist = ECommentManager::getInstance()->getCommentByTrainId($inTrain, 'notAccepted');
		if ($commExist == false) {			
			$in = false;
			$inId = null;
			$inState = 1; // Cet état correspond à un commentaire en attente de validation
			$sql = 'INSERT INTO COMMENTS (COMMENT, COMM_STATE_CODE, COMM_TRAI_FK) values (:c, :s, :t)';
			try {
				$stmt = EDatabase::prepare($sql);
				$stmt->execute(array( ':c' => $inComment, ':s' => $inState, ':t' => $inTrain));
				 
				// Si pas d'erreur
				$in = new EComment($inId, $inComment, $inState, $inTrain);
				
				array_push($this->comment, $in);
			} catch (PDOException $e) {
				echo "ECommentManager:addComment Error: " . $e->getMessage();
				return false;
			}
			// j'ai réussi à ajouter le commentaire
			return $in;
		}
		else {
			ECommentManager::getInstance()->updateComment($inComment, $inTrain);
		}
	}
	
	/**
	 * Récupère le commentaire qui correspond à l'ID d'un stage spécifique si il n'a pas été validé
	 * @param string $idTrain	L'ID du stage
	 * @param bool $notAccepted	Si true la requete récupère le commentaire que si il n'est pas accepté | si false elle le récupère dans tout les cas
	 * @return string $comment	Retourne le commentaire si il a été trouvé | sinon return false
	 */
	public function getCommentByTrainId($idTrain, $state = false) {
		
		if ($state == "notAccepted")
			$sql = 'SELECT * FROM COMMENTS WHERE COMM_TRAI_FK = :i AND COMM_STATE_CODE != 2';
		else if($state == "accepted")
			$sql = 'SELECT * FROM COMMENTS WHERE COMM_TRAI_FK = :i AND COMM_STATE_CODE = 2';
		else
			$sql = 'SELECT * FROM COMMENTS WHERE COMM_TRAI_FK = :i';
		
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array('i' =>$idTrain));
			
			$result = $stmt->fetchAll();
			if(empty($result)) {
				return false;
			}
			else {
				$comm = new EComment($result[0]['COMMENTS_PK'], $result[0]['COMMENT'], $result[0]['COMM_STATE_CODE'], $result[0]['COMM_TRAI_FK']);
				return $comm;
			}
		} catch (PDOException $e) {
			echo "ECommentManager:getCommentByTrainId Error: " . $e->getMessage();
			return false;
		}
		return false;
	}
	/**
	 * Modifie le commentaire selon l'ID du stage
	 * @param string $inComment	Le nouveau commentaire
	 * @param string $idTrain	L'ID du stage
	 * @return false si une erreure c'est produite
	 */
	public function updateComment($inComment, $idTrain) {
		$sql = 'UPDATE COMMENTS SET COMMENT = :comm, COMM_STATE_CODE = 1 WHERE COMM_TRAI_FK = :id AND COMM_STATE_CODE != 2';
		
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array(':comm' => $inComment, ':id' => $idTrain));
		} catch (Exception $e) {
			echo "ECommentManager:updateComment Error: " . $e->getMessage();
			return false;
		}
	}
	
	public function selectAllUnacceptedComment(){
		$sql = 'SELECT * FROM ' . EDB_DBNAME . '.COMMENTS WHERE COMM_STATE_CODE = 1;';
		try{
			$stmt = EDatabase::prepare($sql);
			$stmt->execute();
			
			while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
				//
				$traineeShip = EAppManager::getInstance()->findTraineeshipById($row['COMM_TRAI_FK']);
				$status = EStatusManager::getInstance()->getStatusLabelByCode($row['COMM_STATE_CODE']);
			
				$in = new EComment($row['COMMENTS_PK'], $row['COMMENT'], $status, $traineeShip);
				array_push($this->comment, $in);
				}
		} catch (Exception $e){
			echo "ECommentManager:selectAllUnacceptedComment Error: " . $e-getMessage();
			return false;
		}
		return $this->comment;
	}
	
	/**
	 * Cette fonction permet de compter le nombre de commentaire qu'il reste à validé
	 * @return int|boolean	Le nombre de commentaire à validé si réussi|sinon false
	 */
	public function countAllUnacceptedComment(){
		$sql = 'SELECT COUNT(COMMENTS_PK) FROM COMMENTS WHERE COMM_STATE_CODE = 1;';
		try{
			$stmt = EDatabase::prepare($sql);
			$stmt->execute();
				
			$result = $stmt->fetchAll();
			if(empty($result)) {
				return false;
			}
			else {
				$comment = $result[0][0];
				return $comment;
			}
			
		} catch (Exception $e){
			echo "ECommentManager:countAllUnacceptedComment Error: " . $e-getMessage();
			return false;
		}
	}
	
	/**
	 * Cette fonction permet d'accepter un commentaire
	 * @param 	int 	$inId	l'id du commentaire 
	 * @return 	boolean			true si ça réussi|sinon false
	 */
	public function acceptComment($inId){
		$sql = 'UPDATE COMMENTS SET COMM_STATE_CODE = 2 WHERE COMMENTS_PK = :id; ';
		try{
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array(':id' => $inId));
			
			return true;
		} catch (Exception $e){
			echo "ECommentManager:acceptComment Error: " . $e-getMessage();
			return false;
		}
	}
	
	/**
	 * Cette fonction permet de refuser un commentaire
	 * @param 	int 	$inId	l'id du commentaire
	 * @return 	boolean			true si ça réussi|sinon false
	 */
	public function refuseComment($inId){
		$sql = 'UPDATE COMMENTS SET COMM_STATE_CODE = 3 WHERE COMMENTS_PK = :id; ';
		try{
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array(':id' => $inId));
				
			return true;
		} catch (Exception $e){
			echo "ECommentManager:refuseComment Error: " . $e-getMessage();
			return false;
		}
	}
	
	public function getAllComments() {
		return $this->comment;
	}
}
