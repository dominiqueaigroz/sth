<?php

/*
 * Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017
 */
require_once '../php/config/database.php';
require_once '../Model/EEnterprise.php';
require_once '../Model/ETraineeship.php';
require_once '../php/library/utilities.php';

/**
 * @brief Helper class pour gérer l'application
 * @author jean-daniel.knz@eduge.ch
 * @remark
 * @version 1.0.0
 */
class EAppManager {


    private static $objInstance;

    /**
     * @brief	Class Constructor - Create a new EAppManager if one doesn't exist
     * 	Set to private so no-one can create a new instance via ' = new EAppManager();'
     */
    private function __construct() {
        $this->enterprise = array();
        $this->traineeship = array();
    }
    
    /** @brief Contient le tableau des EEnterprise */
    private $enterprise;

    /** @brief Contient le tableau des ETraineeship */
    private $traineeship;

    
    /**
     * @brief	Retourne notre instance ou la crée
     * @return $objInstance;
     */
    public static function getInstance() {
        if (!self::$objInstance) {
            try {
                self::$objInstance = new EAppManager();
            } catch (Exception $e) {
                echo "EAppManager Error: " . $e;
            }
        }
        return self::$objInstance;
    }# end method

    /**
     * Charge tous les entreprises
     * @return Le tableau des EEnterprise | false si une erreur se produit
     */
    public function loadAllEnterprises() {
    	$this->enterprise = array();
    	$sql = 'SELECT * FROM ENTERPRISES';
    	try{
    		$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute();
    		while($row=$stmt->fetch(PDO::FETCH_ASSOC,PDO::FETCH_ORI_NEXT)){
    			
    			// Le nombre de stage effectué et terminé par l'entreprise
				$nbrTrainee = $this->countTraineeshipsByEnterprises($row['ENTERPRISES_PK']);
				// Le dernier stage qu'a effectué l'entreprise
				$lastTrainee = $this->selectLastTraineeFromEntreprise($row['ENTERPRISES_PK']);
				// Le domaine de l'entreprise
				$domain = EDomainManager::getInstance()->getDomainById($row['ENTE_DOMA_FK']);

    			$in = new EEnterprise($row['ENTERPRISES_PK'], $row['NAME'], $row['DESCRIPTION'], $row['PHONE'], $row['WEB'], $row['ACTIVE'], $row['STREET'], $row['ZIP'], $row['CITY'], $domain, $row['ENTE_COUN_ISO2']);
    			$in->nbrTrainee = $nbrTrainee;
    			$in->lastTrainee = $lastTrainee;
    			array_push($this->enterprise, $in);
    		} #end while
    		 
    	}catch(PDOException  $e ){
    		echo "EAppManager:loadAllEnterprises Error: ".$e->getMessage();
    		return false;
    	}
    	// Ok
    	return $this->enterprise;
    }# end method
     
    public function getAddressFromEnterprise($inId){
    	$sql = "SELECT CITY, STREET FROM ENTERPRISES WHERE ENTERPRISES_PK = :inId;";
    	try {
    		$stmt = EDatabase::prepare ( $sql );
    		$stmt->execute (array (':inId' => $inId));
    		
    		$result = $stmt->fetchAll ();
    		if (count ( $result ) > 0) {
    			$domain = EDomainManager::getInstance()->getDomainById ( $result [0] ['ENTE_DOMA_FK'] );
    			// Création de l'entreprise avec les données provenant de la base de données
    			$in = new EEnterprise ( $result [0] ['ENTERPRISES_PK'], $result [0] ['NAME'], $result [0] ['DESCRIPTION'], $result [0] ['PHONE'], $result [0] ['WEB'], $result [0] ['ACTIVE'], $result [0] ['STREET'], $result [0] ['ZIP'], $result [0] ['CITY'], $domain, $result [0] ['ENTE_COUN_ISO2'] );
    			array_push ( $this->enterprise, $in );
    			return $in;
    		} // end while
    	} catch ( PDOException $e ) {
    		echo "EAppManager:findEnterpriseById Error: " . $e->getMessage ();
    		return false;
    	}
    	// J'ai pas trouvé l'entreprise
    	return false;
    }
    
    /**
     * Charge les entreprises qui correspondent à la recherche
     * @search	La valeur de la recherche
     * @return Le tableau des EEnterprise | false si une erreur se produit
     */
    public function searchEnterprisesByName($search) {
    	$sql = "SELECT * FROM ENTERPRISES WHERE NAME LIKE '%" . $search . "%'";
    	try{
    		$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute();
    		 
    		while($row=$stmt->fetch(PDO::FETCH_ASSOC,PDO::FETCH_ORI_NEXT)){
    			$domain = EDomainManager::getInstance ()->getDomainById ( $row ['ENTE_DOMA_FK'] );
    			$nbrTrainee = $this->countTraineeshipsByEnterprises($row['ENTERPRISES_PK']);
    			$lastTrainee = $this->selectLastTraineeFromEntreprise($row['ENTERPRISES_PK']);
    			// Création du tableau avec les données provenant de la base de données
    			$in = new EEnterprise($row['ENTERPRISES_PK'], $row['NAME'], $row['DESCRIPTION'], $row['PHONE'], $row['WEB'], $row['ACTIVE'], $row['STREET'], $row['ZIP'], $row['CITY'], $domain, $row['ENTE_COUN_ISO2']);
    			$in->nbrTrainee = $nbrTrainee;
    			$in->lastTrainee = $lastTrainee;
    			array_push($this->enterprise, $in);
    		} #end while   		 
    	}catch(PDOException  $e ){
    		echo "EAppManager:searchEnterprisesByName Error: ".$e->getMessage();
    		return false;
    	}
    	// Ok
    	return $this->enterprise;
    }# end method
    
    /**
     * Recherche les offres de stage dont le sujet ou le nom de l'entreprise correspondent à la recherche
     * @param string $search	La recherche effectuée
     * @return le tableau des ETraineeship | false si une erreur c'est produite
     */
    public function searchOffers($search) {
    	
    	$sql = "SELECT * FROM TRAINEESHIPS tr, ENTERPRISES en WHERE tr.TRAI_ENTE_FK = en.ENTERPRISES_PK AND (en.`NAME` LIKE '%" . $search . "%' OR tr.`SUBJECT` LIKE '%" . $search . "%') AND DATE_BEGIN > CURDATE() GROUP BY SUBJECT";
    	
    	try {
    		$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute();
    		
    		while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
    			
    			$enterprise = $this->findEnterpriseById($row['TRAI_ENTE_FK']);
    			$tutor = EUserManager::getInstance()->findUserByEmail($row['TRAI_USER_TUTOR_FK']);
    			$student = EUserManager::getInstance()->findUserByEmail($row['TRAI_USER_STUDENT_FK']);
    			$available = EAvailableManager::getInstance()->getAvailableLabelByCode($row['TRAI_AVAI_CODE']);
    			
    			$in = new ETraineeship($row['TRAINEESHIPS_PK'], $enterprise, $row['SUBJECT'], $row['DATE_BEGIN'], $row['DATE_END'], $row['MISSION'], $row['COMPETENCE'], $row['TRAI_INDE_CODE'], $available, $row['REMARK'], $row['TRAI_STAT_CODE'], $student, $tutor, $row['TRAI_INCHARGEOF_FK']);
    			
    			if ($this->loadTraineeshipSectors( $in ) == false){
    				echo "EAppManager::loadAllTraineeships - Error call loadTraineeshipSectors";
    				return false;
    			}
    			 
    			$dateBeg = $in->getDateBegin();
    			$dateEnd = $in->getDateEnd();
    			if ($dateBeg != null && $dateEnd != null) {
    				$duration = calculatedDurationTraineeShip($dateBeg, $dateEnd);
    				$in->duration = $duration;
    			}
    			else {
    				$in->duration = 'Indéterminée';
    			}
    			array_push($this->traineeship, $in);
    		}
    	} catch (PDOException  $e ){
    		echo "EAppManager:serchOffers Error: ".$e->getMessage();
    		return false;
    	}
    	// Return le tableau de tout les stages
    	return $this->traineeship;
    }
    
    public function getAllEnterprises(){
    	$this->enterprise;
    }

    /**
     * Recherche si une entreprise existe dans la base de données en fonction de son ID
     * @$inId	L'ID de l'entreprise à rechercher
     * @return	True si l'entreprise existe, autrement false
     */
    public function enterpriseExistInDB($inId) {
        $sql = 'SELECT * FROM ENTERPRISES WHERE ENTERPRISES_PK = :id';
        try {
            $stmt = EDatabase::prepare($sql);
            $stmt->execute(array(':id' => $inId));

            $result = $stmt->fetchAll();
            return (count($result) > 0) ? true : false;
        } catch (PDOException $e) {
            echo "EAppManager:enterpriseExistInDB Error: " . $e->getMessage();
            return false;
        }
        // J'ai pas trouvé l'entreprisew2
        return false;
    }# end method
    
    /**
     * Recherche si une entreprise existe dans la base de données en fonction de son ID
     * @param	$inName	Le nom de l'entreprise à rechercher
     * @return	True 	si l'entreprise existe, autrement false
     */
    public function enterpriseExistInDBByName($inName) {
    	$sql = 'SELECT * FROM ENTERPRISES WHERE NAME = :name';
    	try {
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute(array(':name' => $inName));
    		
    		$result = $stmt->fetchAll();
    		return (count($result) > 0) ? "true" : "false";
    	} catch (PDOException $e) {
    		echo "EAppManager:enterpriseExistInDB Error: " . $e->getMessage();
    		return false;
    	}
    	// J'ai pas trouvé l'entreprisew2
    	return false;
    }# end method
    
    /**
     * Recherche une entreprise spécifique dans la base de donées en fonction de sont nom
     * @param	string	Le nom de l'entreprise que l'on cherche
     * @return	boolean	Retourne une entreprise de type EEnterprise | sinon false
     */
    public function findEnterpriseByName($inName) {
    	// On cherche si l'entreprise existe dans le tableau
    	foreach ( $this->enterprise as $in ) {
    		if ($inName == $in->getName ())
    			return $in;
    	} // end foreach
    
    	$sql = 'SELECT * FROM ENTERPRISES WHERE NAME LIKE :en';
    	try {
    		$stmt = EDatabase::prepare ( $sql );
    		$stmt->execute (array (':en' => $inName));
    			
    		$result = $stmt->fetchAll ();
    		if (count ($result) > 0) {
    			$domain = EDomainManager::getInstance ()->getDomainById ( $result [0] ['ENTE_DOMA_FK'] );
    			// Création de l'entreprise avec les données provenant de la base de données
    			$in = new EEnterprise ( $result [0] ['ENTERPRISES_PK'], $result [0] ['NAME'], $result [0] ['DESCRIPTION'], $result [0] ['PHONE'], $result [0] ['WEB'], $result [0] ['ACTIVE'], $result [0] ['STREET'], $result [0] ['ZIP'], $result [0] ['CITY'], $domain, $result [0] ['ENTE_COUN_ISO2'] );
    			array_push ( $this->enterprise, $in );
    			return $in;
    		} // end while
    	} catch ( PDOException $e ) {
    		echo "EAppManager:findEnterpriseByName Error: " . $e->getMessage ();
    		return false;
    	}
    	// J'ai pas trouvé l'entreprise
    	return false;
    }
    
    public function findEnterpriseById($inId) {
    	$sql = 'SELECT * FROM ENTERPRISES WHERE ENTERPRISES_PK = :inId';
    	try {
    		$stmt = EDatabase::prepare ( $sql );
    		$stmt->execute (array (':inId' => $inId));
    			
    		$result = $stmt->fetchAll ();
    		if (count ( $result ) > 0) {
    			$domain = EDomainManager::getInstance()->getDomainById ( $result [0] ['ENTE_DOMA_FK'] );
    			// Création de l'entreprise avec les données provenant de la base de données
    			$in = new EEnterprise ( $result [0] ['ENTERPRISES_PK'], $result [0] ['NAME'], $result [0] ['DESCRIPTION'], $result [0] ['PHONE'], $result [0] ['WEB'], $result [0] ['ACTIVE'], $result [0] ['STREET'], $result [0] ['ZIP'], $result [0] ['CITY'], $domain, $result [0] ['ENTE_COUN_ISO2'] );
    			array_push ( $this->enterprise, $in );
    			return $in;
    		} // end while
    	} catch ( PDOException $e ) {
    		echo "EAppManager:findEnterpriseById Error: " . $e->getMessage ();
    		return false;
    	}
    	// J'ai pas trouvé l'entreprise
    	return false;
    }
    
    /**
     * Retourne le nom d'une entreprise selon son ID
     * @param	$inId 	L'ID de l'entreprise
     * @return 			le nom de l'entreprise
     */
    public function getNameEnterpriseById($inId) {
    	$sql = 'SELECT NAME FROM ENTERPRISES WHERE ENTERPRISES_PK = :id';
    	try {
    		$stmt = EDatabase::prepare ( $sql );
    		$stmt->execute (array(':id' => $inId));
    			
    		$result = $stmt->fetchAll ();
    		$name = $result [0] ['NAME'];
    		return $name;
    	} catch ( PDOException $e ) {
    		echo "EAppManager:getNameEnterpriseById Error: " . $e->getMessage ();
    		return false;
    	}
    	// J'ai pas trouvé l'entreprise
    	return false;
    }
    
    /**
     * Cette fonction permet d'ajouter une entreprise dans la base de données
     * @param	string $inName		Le nom de l'entreprise
     * @param	string $inDesc		La description de l'entreprise
     * @param	string $inPhone		Le numéro de téléphone de l'entreprise
     * @param	string $inWeb		Le site web de l'entreprise
     * @param	boolean $IsActive	L'activité de l'entreprise (si elle est toujours active ou pas)
     * @param	string $inStreet	La rue de l'entreprise
     * @param	integer $inZip		Le code postal de l'entreprise
     * @param	string $inCity		La ville de l'entreprise
     * @param	string $inDomain	Le domaine d'activité de l'entreprise
     * @param	string $inCountry	Le pays de l'entreprise
     * @return	boolean				True si la commande a réussi | sinon false
     */
    public function addEnterprise($inName, $inDesc, $inPhone, $inWeb, $isActive, $inStreet, $inZip, $inCity, $inDomain, $inCountry) {
    	$inId = false;
        $sql = 'INSERT INTO ENTERPRISES (NAME, DESCRIPTION, PHONE, WEB, ACTIVE, STREET, ZIP, CITY, ENTE_DOMA_FK, ENTE_COUN_ISO2) values (:n, :d, :p, :w, :a, :s, :z, :c, :do, :co)';
        try {
            $stmt = EDatabase::prepare($sql);
            $stmt->execute(array( ':n' => $inName, ':d' => $inDesc, ':p' => $inPhone, ':w' => $inWeb, ':a' => $isActive, ':s' => $inStreet, ':z' => $inZip, ':c' => $inCity, ':do' => $inDomain, ':co' => $inCountry));
        } catch (PDOException $e) {
            echo "EAppManager:addEnterprise Error: " . $e->getMessage();
            return false;
        }
    }
    
    /**
     * Cette fonction sert à modifier une entreprise
     * @param 	integer	$inId		L'ID de l'entreprise
     * @param	string 	$inName		Le nom de l'entreprise
     * @param 	string 	$inDesc		La description de l'entreprise
     * @param 	string 	$inPhone	Le numéro de téléphone de l'entreprise
     * @param 	string 	$inWeb		Le site web de l'entreprise
     * @param 	boolean $isActive	L'activité de l'entreprise (si elle est toujours active ou pas)
     * @param 	string 	$inStreet	La rue de l'entreprise
     * @param 	integer	$inZip		Le code postal de l'entreprise
     * @param 	string 	$inCity		La ville de l'entreprise
     * @param 	string 	$inDomain	Le domaine d'activité de l'entreprise
     * @param 	string 	$inCountry	Le pays de l'entreprise
     * @return 	boolean				L'objet EEnterprise modifié | false si une erreur se produit
     */
    public function updateEnterprise($inId, $inName, $inDesc, $inPhone, $inWeb, $isActive, $inStreet, $inZip, $inCity, $inDomain, $inCountry) {
    	$inCreate = true;
    	if ($this->enterpriseExistInDB($inId)){
    		$inCreate = false;
    		$sql = 'UPDATE ENTERPRISES SET NAME = :n, DESCRIPTION = :d, PHONE = :p, WEB = :w, ACTIVE = :a, STREET = :s, ZIP = :z, CITY = :c, ENTE_DOMA_FK = :do, ENTE_COUN_ISO2 = :co WHERE ENTERPRISES_PK = :id';
    		try{
    			$sth = EDatabase::prepare($sql);
    			$sth->execute(array( ':id' => $inId, ':n' => $inName, ':d' => $inDesc, ':p' => $inPhone, ':w' => $inWeb, ':a' => $isActive, ':s' => $inStreet, ':z' => $inZip, ':c' => $inCity, ':do' => $inDomain, ':co' => $inCountry));
    		}catch(PDOException  $e ){
    			echo "EAppManager:updateEnterprise Error: ".$e->getMessage();
    			return false;
    		}
    		echo 'l\'entreprise a bien été modifiée';
    	}
    	// S'il n'existe pas et c'est que je dois le créer, go !!!
    	if ($inCreate){
    		return $this->addEnterprise($inName, $inDesc, $inPhone, $inWeb, $isActive, $inStreet, $inZip, $inCity, $inDomain, $inCountry);
    	}
    }
    
    /**
     * Cette fonction permet de suprimmer une entreprise de la base de données
     * @param	string	$inName	Le nom de l'entreprise que l'on veut suprimmer
     * @return	boolean			True si la commande a réussi, sinon false
     */
    public function deleteEnterprise($inId){
    	$sql = 'DELETE FROM ENTERPRISES WHERE ENTERPRISES_PK = :id';
    	try {
    		$trainee = array();
    		$enterprise = $this->findEnterpriseById($inId);
    		if ($enterprise != false){
    			$deleteOK = $this->checkTrainFromEnter($inId);
    			if ($deleteOK){
    				$stmt = EDatabase::prepare($sql);
    				$stmt->execute(array(':id' => $inId));
    			}
    		}
    	} catch (PDOException $e) {
    		echo "EAppManager:deleteEnteprise Error: " . $e->getMessage();
    		return false;
    	}
    	// Ok
    	echo 'l\'entreprise a bien été suprimmée';
    }# end method
    
    public function checkTrainFromEnter($inId) {
    	$trainee = $this->getAllTraineeFromEnterprise($inId);
    	$count = count($trainee);
    	if ($count == 0){
    		return true; // On peut supprimer l'entreprise
    	}
    	else {
    		return false; // On ne peut pas retourner l'entreprise
    	}
    }

    /**
     * Fonction servant à compter le nombres de stages terminé ou annulé par entreprises
     * @param	integer	$inId	L'id de l'entreprise que l'on veut pour compter ses stages
     * @return	boolean			le nombre de stage | sinon false
     */
    public function countTraineeshipsByEnterprises($inId){
    	$sql = 'SELECT COUNT(tr.SUBJECT) FROM TRAINEESHIPS tr, ENTERPRISES en WHERE tr.TRAI_ENTE_FK = en.ENTERPRISES_PK AND en.ENTERPRISES_PK = :inId AND DATE_END < CURDATE() GROUP BY en.ENTERPRISES_PK';
    	try{
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute(array(':inId' => $inId));
    		
    		$result = $stmt->fetchAll();
    		if($result == null)
    			return 0;
    		else{
    		$nbr = $result[0][0];
    		return $nbr;
    		}
    	}catch (PDOExeception $e){
    		echo "EAppManager:countTrainneshipsByEnterprises Error: " . $e->getMessage();
    	}
    	return false;
    }
    
    /**
     * Fonction servant à recevoir tous les stages d'une entreprise
     * @param	integer	$inId	L'id de l'entreprise que l'on veut pour compter ses stages
     * @return	boolean			le nombre de stage | sinon false
     */
    public function getAllTraineeFromEnterprise($inId){
    	$sql = 'SELECT * FROM TRAINEESHIPS tr, ENTERPRISES en WHERE tr.TRAI_ENTE_FK = en.ENTERPRISES_PK AND en.ENTERPRISES_PK = :inId';
    	try {
    		$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array(':inId' => $inId));
    		while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
    			$enterprise = $this->findEnterpriseById($row['TRAI_ENTE_FK']);
    			$tutor = EUserManager::getInstance()->findUserByEmail($row['TRAI_USER_TUTOR_FK']);
    			$student = EUserManager::getInstance()->findUserByEmail($row['TRAI_USER_STUDENT_FK']);
    			$available = EAvailableManager::getInstance()->getAvailableLabelByCode($row['TRAI_AVAI_CODE']);
    
    			$in = new ETraineeship($row['TRAINEESHIPS_PK'], $enterprise, $row['SUBJECT'], $row['DATE_BEGIN'], $row['DATE_END'], $row['MISSION'], $row['COMPETENCE'], $row['TRAI_INDE_CODE'], $available, $row['REMARK'], $row['TRAI_STAT_CODE'], $student, $tutor, $row['TRAI_INCHARGEOF_FK']);
    			if ($this->loadTraineeshipSectors( $in ) == false){
    				echo "EAppManager:loadAllTraineeships - Error call loadTraineeshipSectors";
    				return false;
    			}
    			$dateBeg = $in->getDateBegin();
    			$dateEnd = $in->getDateEnd();
    			if ($dateBeg != null && $dateEnd != null) {
    				$duration = calculatedDurationTraineeShip($dateBeg, $dateEnd);
    				$in->duration = $duration;
    			}
    			else {
    				$in->duration = 'Indéterminée';
    			}
    			array_push($this->traineeship, $in);
    			return $in;
    		} #end while
    	}catch (PDOExeception $e){
    		echo "EAppManager:countTrainneshipsByEnterprises Error: " . $e->getMessage();
    	}
    	return $this->traineeship;
    }
    
    public function deleteAssignement($inId){
    	$sql = 'DELETE FROM ' . EDB_DBNAME . '.STUDENTS_HAS_TUTORS WHERE STTU_TRAI_FK = :inId; UPDATE ' . EDB_DBNAME . '.TRAINEESHIPS SET TRAI_USER_STUDENT_FK = null, TRAI_USER_TUTOR_FK = null WHERE TRAINEESHIPS_PK = :inId; ';
    	try {
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute(array(':inId' => $inId));
    	} catch (PDOException $e) {
    		echo "EAppManager:deleteAssignement Error: " . $e->getMessage();
    		return false;
    	}
    	// J'ai réussi à suprimmer le stage
    	return true;
    	
    }
    
    /**
     * Fonction servant à recevoir tous les stages à venir d'une entreprise (Partie Offre)
     * @param	integer	$inId	L'id de l'entreprise que l'on veut pour compter ses stages
     * @return	boolean			le nombre de stage | sinon false
     */
    public function getAllTraineeToComeFromEnterprise($inId){
    	$sql = 'SELECT * FROM TRAINEESHIPS tr, ENTERPRISES en WHERE tr.TRAI_ENTE_FK = en.ENTERPRISES_PK AND en.ENTERPRISES_PK = :inId AND DATE_BEGIN > CURDATE();';
    	try {
    		$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array(':inId' => $inId));
    		while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
    			$enterprise = $this->findEnterpriseById($row['TRAI_ENTE_FK']);
    			$tutor = EUserManager::getInstance()->findUserByEmail($row['TRAI_USER_TUTOR_FK']);
    			$student = EUserManager::getInstance()->findUserByEmail($row['TRAI_USER_STUDENT_FK']);
    			$available = EAvailableManager::getInstance()->getAvailableLabelByCode($row['TRAI_AVAI_CODE']);
    
    			$in = new ETraineeship($row['TRAINEESHIPS_PK'], $enterprise, $row['SUBJECT'], $row['DATE_BEGIN'], $row['DATE_END'], $row['MISSION'], $row['COMPETENCE'], $row['TRAI_INDE_CODE'], $available, $row['REMARK'], $row['TRAI_STAT_CODE'], $student, $tutor, $row['TRAI_INCHARGEOF_FK']);
    			if ($this->loadTraineeshipSectors( $in ) == false){
    				echo "EAppManager:loadAllTraineeships - Error call loadTraineeshipSectors";
    				return false;
    			}
    			$dateBeg = $in->getDateBegin();
    			$dateEnd = $in->getDateEnd();
    			if ($dateBeg != null && $dateEnd != null) {
    				$duration = calculatedDurationTraineeShip($dateBeg, $dateEnd);
    				$in->duration = $duration;
    			}
    			else {
    				$in->duration = 'Indéterminée';
    			}
    			array_push($this->traineeship, $in);
    		} #end while
    	}catch (PDOExeception $e){
    		echo "EAppManager:countTrainneshipsByEnterprises Error: " . $e->getMessage();
    	}
    	return $this->traineeship;
    }
    
    
    /**
     * Fonction servant à recevoir tous les stages terminés d'une entreprise (partie historique)
     * @param	integer	$inId	L'id de l'entreprise que l'on veut pour compter ses stages
     * @return	boolean			le nombre de stage | sinon false
     */
    public function getAllFinishedTraineeFromEnterprise($inId){
    	$sql = 'SELECT * FROM TRAINEESHIPS tr, ENTERPRISES en WHERE tr.TRAI_ENTE_FK = en.ENTERPRISES_PK AND en.ENTERPRISES_PK = :inId AND DATE_END < CURDATE()';
    	try {
    		$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array(':inId' => $inId));
    		while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
    			//
    			 
    			$enterprise = $this->findEnterpriseById($row['TRAI_ENTE_FK']);
    			$tutor = EUserManager::getInstance()->findUserByEmail($row['TRAI_USER_TUTOR_FK']);
    			$student = EUserManager::getInstance()->findUserByEmail($row['TRAI_USER_STUDENT_FK']);
    			$available = EAvailableManager::getInstance()->getAvailableLabelByCode($row['TRAI_AVAI_CODE']);
    	
    			$in = new ETraineeship($row['TRAINEESHIPS_PK'], $enterprise, $row['SUBJECT'], $row['DATE_BEGIN'], $row['DATE_END'], $row['MISSION'], $row['COMPETENCE'], $row['TRAI_INDE_CODE'], $available, $row['REMARK'], $row['TRAI_STAT_CODE'], $student, $tutor, $row['TRAI_INCHARGEOF_FK']);
    			if ($this->loadTraineeshipSectors( $in ) == false){
    				echo "EAppManager:loadAllTraineeships - Error call loadTraineeshipSectors";
    				return false;
    			}
    			$dateBeg = $in->getDateBegin();
    			$dateEnd = $in->getDateEnd();
    			if ($dateBeg != null && $dateEnd != null) {
    				$duration = calculatedDurationTraineeShip($dateBeg, $dateEnd);
    				$in->duration = $duration;
    			}
    			else {
    				$in->duration = 'Indéterminée';
    			}
    			array_push($this->traineeship, $in);
    		} #end while
    	}catch (PDOExeception $e){
    		echo "EAppManager:getAllFinishedTraineeFromEnterprise Error: " . $e->getMessage();
    	}
    	return $this->traineeship;
    }
    

    
    /**
     * Fonction servant à connaitre la date du dernier stage effectué par une entreprise
     * @param	integer	$indId	L'id de l'entreprise
     * @return	date	$year	La date du dernier stage effectué
     */
    public function selectLastTraineeFromEntreprise($indId){
    	$sql = 'SELECT MAX(DATE_BEGIN) FROM ' . EDB_DBNAME . '.TRAINEESHIPS tr, ' . EDB_DBNAME . '.ENTERPRISES en WHERE tr.TRAI_ENTE_FK = en.ENTERPRISES_PK AND en.ENTERPRISES_PK = :inId AND DATE_END < CURDATE()';
    	try{
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute(array(':inId' => $indId));
    	
    		$result = $stmt->fetchAll();
    		if(empty($result))
    			return "Aucune information";
    			else{
    				$year = $result[0][0];
    				return $year;
    			}
    	}catch (PDOExeception $e){
    		echo "EAppManager:selectLastTraineeFromEntreprise Error: " . $e->getMessage();
    	}
    	return false;
    }
    
    /**
     * Charge tout les stages
     * @return Le tableau des ETraineeship | false si une erreur se produit
     */
    public function loadAllTraineeships() {
    	$sql = 'SELECT * FROM TRAINEESHIPS';
    	try {
    		$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute();
    		
    		while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
    				
    			$enterprise = $this->findEnterpriseById($row['TRAI_ENTE_FK']);
    			$available = EAvailableManager::getInstance()->getAvailableLabelByCode($row['TRAI_AVAI_CODE']);
    			$student = EUserManager::getInstance()->findUserByEmail($row['TRAI_USER_STUDENT_FK']);
    			$tutor = EUserManager::getInstance()->findUserByEmail($row['TRAI_USER_TUTOR_FK']);
    			
    			 
    			$in = new ETraineeship($row['TRAINEESHIPS_PK'], $enterprise, $row['SUBJECT'], $row['DATE_BEGIN'], $row['DATE_END'], $row['MISSION'], $row['COMPETENCE'], $row['TRAI_INDE_CODE'], $available, $row['REMARK'], $row['TRAI_STAT_CODE'], $student, $tutor, $row['TRAI_INCHARGEOF_FK']);
    			if ($this->loadTraineeshipSectors( $in ) == false){
    				echo "EAppManager::loadAllTraineeships - Error call loadTraineeshipSectors";
    				return false;
    			}
    			
    			$dateBeg = $in->getDateBegin();
    			$dateEnd = $in->getDateEnd();
    			if ($dateBeg != null && $dateEnd != null) {
    				$duration = calculatedDurationTraineeShip($dateBeg, $dateEnd);
    				$in->duration = $duration;
    			}
    			else {
    				$in->duration = 'Indéterminée';
    			}
    			array_push($this->traineeship, $in);
    		} #end while
    		
    	} catch (PDOExeception $e) {
    		echo "EAppManager:loadAllTraineeships Error : " . $e->getMessage();
    		return false;
    	}
    	// Return le tableau de tout les stages
    	return $this->traineeship;
    }   

    
    /**
     * Charge les secteurs pour un objet ETraineeship
     * @param ETraineeship $tr	La référence sur l'objet ETraineeship
     * @return boolean	True si les secteurs ont été chargés, autrement False.
     */
    private function loadTraineeshipSectors( & $tr ) {
    	$sql = 'SELECT SECTORS.SECTORS_PK, SECTORS.NAME FROM SECTORS, TRAINEESHIPS, SECTORS_HAS_TRAINEESHIPS WHERE SECTORS_HAS_TRAINEESHIPS.SETR_TRAI_FK = :id AND SECTORS.SECTORS_PK = SECTORS_HAS_TRAINEESHIPS.SETR_SECT_FK AND TRAINEESHIPS.TRAINEESHIPS_PK = SECTORS_HAS_TRAINEESHIPS.SETR_TRAI_FK;';
    	try {
    		$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array(':id' => $tr->getId()));
    	
    		while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
    			$sc = new ESector($row['SECTORS_PK'], $row['NAME']);
    			array_push( $tr->getSectors(), $sc );
    		} #end while
    	
    	} catch (PDOExeception $e) {
    		echo "EAppManager:loadTraineeshipSectors Error : " . $e->getMessage();
    		return false;
    	}
    	// Done
    	return true;
    }
    
    /**
     * Recherche si un stage existe dans la base de données en fonction de son ID
     * @param unknown $InTraineeshipId	l'ID du stage rechercher
     * @return True si le stage existe | false si il n'existe pas
     */
    public function traineeshipExistInDB($inId) {
    	$sql = 'SELECT * FROM TRAINEESHIPS WHERE TRAINEESHIPS_PK = :i';
    	
    	try {
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute(array(':i' => $inId));
    	
    		$result = $stmt->fetchAll();
    		if (count($result) > 0) {
    			// Création du tableau stage avec les données provenant de la base de données
    			$in = array($result[0]);
    			return $in;
    		}
    		return (count($result) > 0) ? true : false;
    	} catch (PDOException $e) {
    		echo "EAppManager:traineeshipExistInDB Error: " . $e->getMessage();
    		return false;
    	}
    	// J'ai pas trouvé le stage
    	return false;
    }
    
    /**
     * Recherche un stage spécifique dans la base en fonction de sont ID
     * @$inId L'id du stage que l'on cherche
     * @return Le stage type ETraineeship | false si il n'existe pas
     */
    public function findTraineeshipById($inId) {
    	$sql = 'SELECT * FROM TRAINEESHIPS WHERE TRAINEESHIPS_PK = :id';
    	try {
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute(array(':id' => $inId));
    
    		$result = $stmt->fetchAll();
    		if (count($result) > 0) {
    			$enterprise = $this->findEnterpriseById($result[0]['TRAI_ENTE_FK']);
    			$available = EAvailableManager::getInstance()->getAvailableLabelByCode($result[0]['TRAI_AVAI_CODE']);
    			$status = EStatusManager::getInstance()->getStatusLabelByCode($result[0]['TRAI_STAT_CODE']);
    			$inChargeOf = EInChargeOfManager::getInstance()->getInChargeOfById($result[0]['TRAI_INCHARGEOF_FK']);
    			$student = EUserManager::getInstance()->findUserByEmail($result[0]['TRAI_USER_STUDENT_FK']);
    			$tutor = EUserManager::getInstance()->findUserByEmail($result[0]['TRAI_USER_TUTOR_FK']);
    			 
    			// Création du tableau stage avec les données provenant de la base de données
    			$in = new ETraineeship($result[0]['TRAINEESHIPS_PK'], $enterprise, $result[0]['SUBJECT'], $result[0]['DATE_BEGIN'], $result[0]['DATE_END'], $result[0]['MISSION'], $result[0]['COMPETENCE'], $result[0]['TRAI_INDE_CODE'], $available, $result[0]['REMARK'], $status, $student, $tutor, $inChargeOf);
    			if ($this->loadTraineeshipSectors( $in ) == false){
    				echo "EAppManager::loadAllTraineeships - Error call loadTraineeshipSectors";
    				return false;
    			}
    			if ($this->loadTraineeshipCriteria( $in ) == false){
    				echo "EAppManager::loadAllTraineeships - Error call loadTraineeshipCriteria";
    				return false;
    			}
    
    			$dateBeg = $in->getDateBegin();
    			$dateEnd = $in->getDateEnd();
    			if ($dateBeg != null && $dateEnd != null) {
    				$duration = calculatedDurationTraineeShip($dateBeg, $dateEnd);
    				$in->duration = $duration;
    			}
    			else {
    				$in->duration = 'Indéterminée';
    			}
    			$in->comm = ECommentManager::getInstance()->getCommentByTrainId($result[0]['TRAINEESHIPS_PK'], 'accepted');
    			return $in;
    		}
    	} catch (PDOException $e) {
    		echo "EAppManager:findTraineeshipById Error: " . $e->getMessage();
    		return false;
    	}
   		return false;

    }
    
    /**
     * Recherche un stage spécifique dans la base en fonction de son sujet
     * @$inSubj Le sujet du stage que l'on cherche
     * @return Le stage type ETraineeship | false si il n'existe pas
     */
    public function findTraineeshipBySubject($inSubj) {

    	$sql = 'SELECT * FROM TRAINEESHIPS WHERE SUBJECT = :s';
    	try {
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute(array(':s' => $inSubj));
    		
    		$result = $stmt->fetchAll();
    		if (count($result) > 0) {
    			$enterprise = $this->findEnterpriseById($result[0]['TRAI_ENTE_FK']);
    			$available = EAvailableManager::getInstance()->getAvailableLabelByCode($result[0]['TRAI_AVAI_CODE']);
    			$status = EStatusManager::getInstance()->getStatusLabelByCode($result[0]['TRAI_STAT_CODE']);
    			$inChargeOf = EInChargeOfManager::getInstance()->getInChargeOfById($result[0]['TRAI_INCHARGEOF_FK']);
    			$student = EUserManager::getInstance()->findUserByEmail($result[0]['TRAI_USER_STUDENT_FK']);
    			$tutor = EUserManager::getInstance()->findUserByEmail($result[0]['TRAI_USER_TUTOR_FK']);
    			
    			// Création du tableau stage avec les données provenant de la base de données
    			$in = new ETraineeship($result[0]['TRAINEESHIPS_PK'], $enterprise, $result[0]['SUBJECT'], $result[0]['DATE_BEGIN'], $result[0]['DATE_END'], $result[0]['MISSION'], $result[0]['COMPETENCE'], $result[0]['TRAI_INDE_CODE'], $available, $result[0]['REMARK'], $status, $student, $tutor, $inChargeOf);
    			if ($this->loadTraineeshipSectors( $in ) == false){
    				echo "EAppManager::loadAllTraineeships - Error call loadTraineeshipSectors";
    				return false;
    			}
    			if ($this->loadTraineeshipCriteria( $in ) == false){
    				echo "EAppManager::loadAllTraineeships - Error call loadTraineeshipCriteria";
    				return false;
    			}
    			 
    			$dateBeg = $in->getDateBegin();
    			$dateEnd = $in->getDateEnd();
    			if ($dateBeg != null && $dateEnd != null) {
    				$duration = calculatedDurationTraineeShip($dateBeg, $dateEnd);
    				$in->duration = $duration;
    			}
    			else {
    				$in->duration = 'Indéterminée';
    			}
    			$in->comm = ECommentManager::getInstance()->getCommentByTrainId($result[0]['TRAINEESHIPS_PK'], 'accepted');
    			
    			array_push($this->traineeship, $in);
    			return $in;
    		}
    	} catch (PDOException $e) {
    		echo "EAppManager:findTraineeshipBySubject Error: " . $e->getMessage();
    		return false;
    	}
    	
    	return false;
    }
    

    
    /**
     * Charge les critères pour un objet ETraineeship
     * @param ETraineeship $tr	La référence sur l'objet ETraineeship
     * @return boolean	True si les critères ont été chargés, autrement False.
     */
    private function loadTraineeshipCriteria( & $tr ) {
    	$sql = 'SELECT CRITERIA.CODE, CRITERIA.LABEL, CRITERIA_HAS_TRAINEESHIPS.SATISFACTIONS_CODE FROM CRITERIA, TRAINEESHIPS, CRITERIA_HAS_TRAINEESHIPS WHERE CRITERIA_HAS_TRAINEESHIPS.CRTR_TRAI_FK = :id AND CRITERIA.CODE = CRITERIA_HAS_TRAINEESHIPS.CRTR_CRIT_CODE AND TRAINEESHIPS.TRAINEESHIPS_PK = CRITERIA_HAS_TRAINEESHIPS.CRTR_TRAI_FK ORDER BY CODE';
    	try {
    		$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array(':id' => $tr->getId()));
    		 
    		while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
    			$cr = new ECriterion($row['CODE'], $row['LABEL']);
    			$satisfaction = ESatisfactionManager::getInstance()->getSatisfactionLabelByCode($row['SATISFACTIONS_CODE']);
    			$cr->satisfaction = $satisfaction;
    			array_push( $tr->getCriteria(), $cr );
    		} #end while
    		 
    	} catch (PDOExeception $e) {
    		echo "EAppManager::loadTraineeshipCriteria Error : " . $e->getMessage();
    		return false;
    	}
    	// Done
    	return true;
    }
    
    
    /**
     * Cette fonction permet d'ajouter un stage dans la base de données
     *  @param $inEnter int			L'ID de l'entreprise qui propose le stage
     *  @param $inSubj string		Le sujet du stage
	 *  @param $inDateBeg date		La date de début du stage
	 *  @param $inDateEnd date		La date de fin du stage
	 *  @param $inMission string	La mission sur laquelle porte le stage
	 *  @param $inCompet string		Les compétences requises pour le stage
	 *  @param $inIndem int			L'ID des indemnités du stages
	 *  @param $inAvailab int		L'ID des disponibilités du stage
	 *  @param $inRemark string		Les remarques sur le stage
	 *  @param $inStatus int		L'ID de l'état actuel du stage
	 *  @param $inStud string		L'adresse mail de l'élève (stagiaire) qui participe au stage
	 *  @param $inTutor string		L'adresse mail du tuteur qui est chargé de l'élève
     *  @param $inChargeOf int		L'ID de la personne de contact du stage
     *  @param return l'objet ETraineeship créé | false si une erreur se produit
     */
    public function addTraineeship($inEnter, $inSubj, $inDateBeg, $inDateEnd, $inMission, $inCompet, $inIndemn, $inAvailab, $inRemark, $inStatus, $inStud, $inTutor, $inChargeOf) {
    	$sql = 'INSERT INTO TRAINEESHIPS (TRAI_ENTE_FK, SUBJECT, DATE_BEGIN, DATE_END, MISSION, COMPETENCE, TRAI_INDE_CODE, TRAI_AVAI_CODE, REMARK, TRAI_STAT_CODE, TRAI_USER_STUDENT_FK, TRAI_USER_TUTOR_FK, TRAI_INCHARGEOF_FK) values (:e, :su, :db, :de, :m, :comp, :i, :a, :r, :sta, :stu, :t, :ch)';
    	try {
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute(array( ':e' => $inEnter, ':su' => $inSubj, ':db' =>  $inDateBeg, ':de' => $inDateEnd, ':m' => $inMission, ':comp' => $inCompet, ':i' => $inIndemn, ':a' => $inAvailab, ':r' => $inRemark, ':sta' => $inStatus, ':stu' => $inStud, ':t' => $inTutor, ':ch' => $inChargeOf));
    		
    		// Requete SQL qui permet de récupèrer l'ID du nouveau stage
    		$sql = 'SELECT TRAINEESHIPS_PK FROM TRAINEESHIPS ORDER BY TRAINEESHIPS_PK DESC LIMIT 1';
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute();
    		$id = $stmt->fetchAll();
    		
    		// j'ai réussi à ajouter le stage
    		return $id[0][0];
    	} catch (PDOException $e) {
    		echo "EAppManager:addTraineeship Error: " . $e->getMessage();
    		return false;
    	}
    } 
    
    /**
     * Cette fonction permet de suprimmer un stage de la base de données
     * @$inSubj Le sujet du stage
     * @return	boolean	false
     */
    public function removeTraineeship($inSubj){
    	$sql = 'DELETE FROM TRAINEESHIPS WHERE SUBJECT=:s';
    	try {
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute(array(':s' => $inSubj));
    	} catch (PDOException $e) {
    		echo "EAppManager:removeTraineeship Error: " . $e->getMessage();
    		return false;
    	}
    	// J'ai réussi à suprimmer le stage
    	return true;

    }
    
    /**
     * Cette fonction sert à modifier un stage
     * @param $inId			L'ID du stage
     * @param $inEnter		L'ID de l'entreprise qui propose le stage
     * @param $inSubj		Le sujet du stage
     * @param $inDateBeg	La date de début du stage
     * @param $inDateEnd	La date de fin du stage
     * @param $inMission	La mission à accomplir durant le stage
     * @param $inCompet		Les compétences requises pour le stage
     * @param $inIndemn		L'ID de l'indemnité du stage
     * @param $inAvailab	L'ID de la disponibilité actuel du stage
     * @param $inRemark		Les remarques sur le stage
     * @param $inStatus		L'ID de l'état actuel du stage
     * @param $inStud		L'email de l'élève
     * @param $inTutor		L'email du tuteur
     * @param $inChargeOf	L'ID du responsable du stage
     * @return L'objet ETraineeship modifié | false si une erreur se produit
     */
    public function updateTraineeship($inId, $inEnter, $inSubj, $inDateBeg, $inDateEnd, $inMission, $inCompet, $inIndemn, $inAvailab, $inRemark, $inStatus, $inStud, $inTutor, $inChargeOf) {
    	if ($this->traineeshipExistInDB($inId)) {
    		$sql = 'UPDATE TRAINEESHIPS SET TRAI_ENTE_FK = :e, SUBJECT = :su, DATE_BEGIN = :db, DATE_END = :de, MISSION = :m, COMPETENCE = :comp, TRAI_INDE_CODE = :i, TRAI_AVAI_CODE = :a, REMARK = :r, TRAI_STAT_CODE = :sta, TRAI_INCHARGEOF_FK = :ch WHERE TRAINEESHIPS_PK = :id';
    		try {
    			$stmt = EDatabase::prepare($sql);
    			$stmt->execute(array( ':e' => $inEnter, ':su' => $inSubj, ':db' =>  $inDateBeg, ':de' => $inDateEnd, ':m' => $inMission, ':comp' => $inCompet, ':i' => $inIndemn, ':a' => $inAvailab, ':r' => $inRemark, ':sta' => $inStatus, ':ch' => $inChargeOf, ':id' => $inId));
    			// Le stage a été correctement modifié
    			return true;
    		} catch (PDOExecption $e) {
    			echo "EAppManager:updateTraineeship Error: " . $e->getMessage();
    			return false;
    		}
    	}
    	// S'il n'existe pas c'est que je dois le créer, go !!!
   		return $this->addTraineeship($inEnter, $inSubj, $inDateBeg, $inDateEnd, $inMission, $inCompet, $inIndemn, $inAvailab, $inRemark, $inStatus, $inStud, $inTutor, $inChargeOf);	
    }

    /**
     * Récupère le code du statut du stage via son ID
     * @param int $idTrain	L'ID du stage
     * @return boolean	le code du statut du stage | false si une erreure c'est produite
     */
    public function getStatusCodeByIdTrainee($idTrain) {
    	$sql = "SELECT TRAI_STAT_CODE FROM TRAINEESHIPS WHERE TRAINEESHIPS_PK = :id";
    	
    	try {
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute(array(':id' => $idTrain));
    		
    		$result = $stmt->fetchAll();
    		
    		return $result[0][0];
    		
    	} catch (PDOException $e) {
    		echo "EAppManager:getStatusCodeByIdTrainee Error: " . $e->getMessage();
    		return false;
    	}
    }
    
    /**
     * Récupère le code du responsable du stage via son ID
     * @param int $idTrain	L'ID du stage
     * @return boolean	le code du responsable du stage | false si une erreure c'est produite
     */
    public function getInChargeOfCodeByIdTrainee($idTrain) {
    	$sql = "SELECT TRAI_INCHARGEOF_FK FROM TRAINEESHIPS WHERE TRAINEESHIPS_PK = :id";
    	 
    	try {
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute(array(':id' => $idTrain));
    
    		$result = $stmt->fetchAll();
    
    		return $result[0][0];
    
    	} catch (PDOException $e) {
    		echo "EAppManager:getInChargeOfCodeByIdTrainee Error: " . $e->getMessage();
    		return false;
    	}
    }
    
    /**
     * Recherche un utilisateur en fonction de sont email.
     * @email	L'email à rechercher
     * @return	Le stage type ETraineeship | false si n'existe pas
     */
    public function getAllTraineeFromStudent($inEmail) {
    	$sql = "SELECT * FROM TRAINEESHIPS tr, USERS u WHERE tr.TRAI_USER_STUDENT_FK = u.EMAIL_PK AND TRAI_USER_STUDENT_FK LIKE :e";
    	try {
    		$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array(':e' => $inEmail));
    		
    		while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
    			//
    			$enterprise = $this->findEnterpriseById($row['TRAI_ENTE_FK']);
    			$tutor = EUserManager::getInstance()->findUserByEmail($row['TRAI_USER_TUTOR_FK']);
    			$comment = ECommentManager::getInstance()->getCommentByTrainId($row['TRAINEESHIPS_PK']);

    			$in = new ETraineeship($row['TRAINEESHIPS_PK'], $enterprise, $row['SUBJECT'], $row['DATE_BEGIN'], $row['DATE_END'], $row['MISSION'], $row['COMPETENCE'], $row['TRAI_INDE_CODE'], $row['TRAI_AVAI_CODE'], $row['REMARK'], $row['TRAI_STAT_CODE'], $row['TRAI_USER_STUDENT_FK'], $tutor, $row['TRAI_INCHARGEOF_FK']);
    			$in->comm = $comment;
    			if ($this->loadTraineeshipSectors( $in ) == false){
    				echo "EAppManager:getAllTraineeFromUser - Error call loadTraineeshipSectors";
    				return false;
    			}

    			array_push($this->traineeship, $in);
    		} #end while
    	} catch (PDOException $e) {
    		echo "EAppManager:getAllTraineeFromUser Error: " . $e->getMessage();
    		return false;
    	}

    	return $this->traineeship;
    }
    # end method
    
    /**
     * Récupere tous les stages d'un prof en fonction de son email
     * @param	string					$inEmail	l'email du prof				
     * @return	ETraineeship|boolean				un objet de type ETraineeship | sinon false
     */
    public function getAllTraineeFromTutor($inEmail) {
    	$sql = "SELECT * FROM TRAINEESHIPS tr, USERS u WHERE tr.TRAI_USER_TUTOR_FK = u.EMAIL_PK AND TRAI_USER_TUTOR_FK LIKE :e";
    	try {
    		$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array(':e' => $inEmail));
    
    		$tutor = EUserManager::getInstance()->findUserByEmail($inEmail);
    		while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
    			$enterprise = $this->findEnterpriseById($row['TRAI_ENTE_FK']);
    			$student = EUserManager::getInstance()->findUserByEmail($row['TRAI_USER_STUDENT_FK']);
    
    			$in = new ETraineeship($row['TRAINEESHIPS_PK'], $enterprise, $row['SUBJECT'], $row['DATE_BEGIN'], $row['DATE_END'], $row['MISSION'], $row['COMPETENCE'], $row['TRAI_INDE_CODE'], $row['TRAI_AVAI_CODE'], $row['REMARK'], $row['TRAI_STAT_CODE'], $student, $tutor, $row['TRAI_INCHARGEOF_FK']);
    			 
    			if ($this->loadTraineeshipSectors( $in ) == false){
    				echo "EAppManager:getAllTraineeFromUser - Error call loadTraineeshipSectors";
    				return false;
    			}
    
    			array_push($this->traineeship, $in);
    		} #end while
    	} catch (PDOException $e) {
    		echo "EAppManager:getAllTraineeFromTutor Error: " . $e->getMessage();
    		return false;
    	}
    	return $this->traineeship;
    }
    # end method
    
    public function getAllTraineeToCome(){
    	$sql = 'SELECT * FROM ' . EDB_DBNAME . '.TRAINEESHIPS WHERE DATE_BEGIN > CURDATE()';
    	try {
    		$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute();
    	
    		while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
    			//
    			$enterprise = $this->findEnterpriseById($row['TRAI_ENTE_FK']);
    			$available = EAvailableManager::getInstance()->getAvailableLabelByCode($row['TRAI_AVAI_CODE']);

    			$in = new ETraineeship($row['TRAINEESHIPS_PK'], $enterprise, $row['SUBJECT'], $row['DATE_BEGIN'], $row['DATE_END'], $row['MISSION'], $row['COMPETENCE'], $row['TRAI_INDE_CODE'], $available, $row['REMARK'], $row['TRAI_STAT_CODE'], $row['TRAI_USER_STUDENT_FK'], $row['TRAI_USER_TUTOR_FK'], $row['TRAI_INCHARGEOF_FK']);
    			if ($this->loadTraineeshipSectors( $in ) == false){
    				echo "EAppManager:getAllTraineeToCome - Error call loadTraineeshipSectors";
    				return false;
    			}

    			$dateBeg = $in->getDateBegin();
    			$dateEnd = $in->getDateEnd();
    			if ($dateBeg != null && $dateEnd != null) {
    				$duration = calculatedDurationTraineeShip($dateBeg, $dateEnd);
    				$in->duration = $duration;
    			}
    			else {
    				$in->duration = 'Indéterminée';
    			}
    			
    			array_push($this->traineeship, $in);
    		} #end while
    	} catch (PDOException $e) {
    		echo "EAppManager:getAllTraineeToCome Error: " . $e->getMessage();
    		return false;
    	}
    	return $this->traineeship;
    }# end method
    
    
    /**
     * Met à jour le status d'un stage
     * @param int $inId		L'ID du stage
     * @param int $inStat	Le code du status
     * @return false si une erreure s'est produite
     */
    public function updateStatusCode($inId, $inStat) {
    	$sql = "UPDATE TRAINEESHIPS SET TRAI_STAT_CODE = :s WHERE TRAINEESHIPS_PK = :i";
    	
    	try {
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute(array(':s' => $inStat, ':i' => $inId));
    		
    		
    	} catch (PDOException $e) {
    		echo "EAppManager:setStatusCodeForTrainee Error: " . $e->getMessage();
    		return false;
    	}
    }
    
    public function addStudentAndTutorToTrainee($student, $tutor, $traineeship) {
    	$sql = "UPDATE TRAINEESHIPS SET TRAI_USER_STUDENT_FK = :s, TRAI_USER_TUTOR_FK = :tu WHERE TRAINEESHIPS_PK = :tr";
    	try {
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute(array( ':s' => $student, ':tu' => $tutor, ':tr' =>  $traineeship));
    
    	} catch (PDOException $e) {
    		echo "addStudentAndTutorToTrainee Error: " . $e->getMessage();
    		return false;
    	}
    }
    
    public function traineeshipExistInAssignement($inId){
    	$sql = 'SELECT * FROM ' . EDB_DBNAME . '.STUDENTS_HAS_TUTORS WHERE STTU_TRAI_FK = :id';
    	try {
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute(array(':id' => $inId));
    		
    		$result = $stmt->fetchAll();
    		if (count($result) > 0) {
    			return true;
    		}
    	} catch (PDOException $e) {
    		echo "EAppManager:findTraineeshipById Error: " . $e->getMessage();
    		return false;
    	}
    	return false;
    }
    
    public function updateAssignement($student, $tutor, $traineeship){
    	$sql = 'UPDATE ' . EDB_DBNAME . '.STUDENTS_HAS_TUTORS SET STTU_STUD_FK=:student, STTU_TUTO_FK=:tutor WHERE `STTU_TRAI_FK`=:trainee';
    	try {
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute(array( ':student' => $student, ':tutor' => $tutor, ':trainee' =>  $traineeship));

    		$this->addStudentAndTutorToTrainee($student, $tutor, $traineeship);
    		return true;
    	} catch (PDOException $e) {
    		echo "addAssignement Error: " . $e->getMessage();
    		return false;
    	}
    	
    }
    
    public function addAssignement($student, $tutor, $traineeship) {
    	$sql = 'INSERT INTO STUDENTS_HAS_TUTORS (STTU_STUD_FK, STTU_TUTO_FK, STTU_TRAI_FK) values (:s, :tu, :tr)';
    	try {
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute(array( ':s' => $student, ':tu' => $tutor, ':tr' =>  $traineeship));
    
    		$this->addStudentAndTutorToTrainee($student, $tutor, $traineeship);
    		return true;
    	} catch (PDOException $e) {
    		echo "addAssignement Error: " . $e->getMessage();
    		return false;
    	}
    }
    	
    public function getAllTraineeships() {
    	return $this->traineeship;
    }
}
