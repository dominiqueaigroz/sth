<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
* Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
* Version: 1.0.0
* Date: 25.11.2016
* Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

class ECivility implements JsonSerializable {

	/**
	 * @brief	Class Constructor
	 */
	public function __construct($inCode = -1, $inLabel = "") {
		$this->code = $inCode;
		$this->label = $inLabel;
	}

	public function jsonSerialize() {
		return get_object_vars($this);
	}
	
	/**
	 * @brief	Est-ce que cet objet est valide
	 * @return  True si valide, autrement false
	 */
	public function isValid() {
		return ($inId == -1) ? false : true;
	}

	/**
	 * @brief	Getter
	 * @return  Le label de la civilité
	 */
	public function getLabel() {
		return $this->label;
	}


	/** @brief L'identifiant unique provenant de la base de données */
	private $code;

	/** @brief Le label de la civilité */
	private $label;

}
