<?php
/**
 * @brief	classe ESession 
 * @author 	Dimitri Petrenko -> rush B
 * @remark	
 */
class ESession {
	/**
	 * @brief	Class Constructor - Create a new database connection if one doesn't exist
	 * 			Set to private so no-one can create a new instance via ' = new KDatabase();'
	 */
	private function __construct() {}
	/**
	 * @brief	Like the constructor, we make __clone private so nobody can clone the instance
	 */
    private function __clone() {}
        
	/**
	 * @brief	Si l'utilisateur s'est connecté, retourne en session son email
	 * @return false si pas connecté
	 */
	public static function getEmail() {
        if (isset($_SESSION['useremail']))
            return $_SESSION['useremail'];
		return false;
	} # end method


	/**
	 * @brief	set l'email dans la session
	 */
	public static function setEmail($email) {
        $_SESSION['useremail'] = $email;
	} # end method


	/**
	 * @brief	Si l'utilisateur s'est connecté, retourne en session son role
	 * @return false si pas connecté
	 */
	public static function getRole() {
        if (isset($_SESSION[EC_ROLE]))
            return $_SESSION[EC_ROLE];
		return false;
	} # end method


	/**
	 * @brief	Set le role dans la session
	 */
	public static function setRole($role) {
        $_SESSION[EC_ROLE] = $role;
	} # end method

		/**
	 * @brief	Si l'utilisateur s'est connecté, retourne en session son nom
	 * @return false si pas connecté
	 */
	public static function getName() {
        if (isset($_SESSION['name']))
            return $_SESSION['name'];
		return false;
	} # end method


	/**
	 * @brief	Set le nom dans la session
	 */
	public static function setName($name) {
        $_SESSION['name'] = $name;
	} # end method

		/**
	 * @brief	Si l'utilisateur s'est connecté, retourne en session son image
	 * @return false si pas connecté
	 */
	public static function getImg() {
        if (isset($_SESSION['img']))
            return $_SESSION['img'];
		return false;
	} # end method


	/**
	 * @brief	Set le image dans la session
	 */
	public static function setImg($img) {
        $_SESSION['img'] = $img;
	} # end method


	/**
	 * @brief Detruit la session
	 *
	 * @remark Attention, ne déconnecte pas de EEL
	 */
	public static function Destroy(){
		// Unset all of the session variables.
		$_SESSION = array();

		// If it's desired to kill the session, also delete the session cookie.
		// Note: This will destroy the session, and not just the session data!
		if (ini_get("session.use_cookies")) {
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000,
				$params["path"], $params["domain"],
				$params["secure"], $params["httponly"]
			);
		}

		// Finally, destroy the session.
		session_destroy();		
	}
}


