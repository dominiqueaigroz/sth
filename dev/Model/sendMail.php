<?php
require_once '../php/config/email.param.php';
require_once '../js/thirdparty/swiftmailer5/lib/swift_required.php';

if (isset($_POST['subject'])){
	$subject = $_POST['subject'];
}

if (isset($_POST['emailTo'])){
	$emailTo = $_POST['emailTo'];
}

if (isset($_POST['msg'])){
	$msg = $_POST['msg'];
}

/**
 * Cette fonction sert à l'envoie d'email
 * @param string $subject	le sujet de l'email
 * @param string $emailTo	l'email du destinataire
 * @param string $msg		le message de l'email
 */
function sendEmail($subject, $emailTo, $msg){
	
	// Send the email
	$transport = Swift_SmtpTransport::newInstance(EMAIL_SERVER, EMAIL_PORT, EMAIL_TRANSPORT)
	->setUsername(EMAIL_EMAIL)
	->setPassword(EMAIL_PASSWORD);
	
	try {
		$mailer = Swift_Mailer::newInstance($transport);
		
		$message = Swift_Message::newInstance();
		$message->setSubject($subject);
		$message->setFrom(array(EMAIL_EMAIL => 'Entreprise Ecole'));
		$message->setTo(array($emailTo));
		
		// The message body
		$body =
		'<html>' .
		' <head></head>' .
		' <body>'.
		$msg.
		' </body>' .
		'</html>';
		
		$message->setBody($body,'text/html');
		
		$result = $mailer->send($message);
		
	} catch (Swift_TransportException $e) {
		exit();
	}
}
?>