<?php
/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
* Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
* Version: 1.0.0
* Date: 25.11.2016
* Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

require_once '../Model/EStatus.php';

/**
 * @brief	Helper class pour gérer les status
 * @author 	jean-daniel.knz@eduge.ch
 * @remark
 * @version     1.0.0
 */
class EStatusManager {
	private static $objInstance;
	
	/**
	 * @brief	Class Constructor - Create a new EAvailableManager if one doesn't exist
	 * 			Set to private so no-one can create a new instance via ' = new EStatusManager();'
	 */
	private function __construct() {
		$this->status = array();
	}
	
	/** @brief Contient le tableau des EStatus */
	private $status;
	
	/**
	 * @brief	Retourne notre instance ou la crée
	 * @return $objInstance;
	 */
	public static function getInstance() {
		if (!self::$objInstance) {
			try {
	
				self::$objInstance = new EStatusManager();
			} catch (Exception $e) {
				echo "EAvailableManager Error: " . $e;
			}
		}
		return self::$objInstance;
	}
	
	/**
	 * Récupère le label du status via son code
	 * @param string $inCode	Le code du status que l'on souhaite récupèrer
	 * @return string $label	le label du status correspondant au code, si le code correspond à aucun label ou si une erreur survient alors on return false
	 */
	public function getStatusLabelByCode($inCode) {
		$sql = 'SELECT LABEL FROM ' . EDB_DBNAME . '.STATUS WHERE CODE = :code';
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array(':code' => $inCode));
			
			$result = $stmt->fetchAll();
			$label = $result[0][0];
			return $label;
			
		} catch (PDOException $e) {
			echo "EStatusManager:getStatusLabelByCode Error: " . $e->getMessage();
			return false;
		}
		// Je n'ai pas trouvé le label la disponibilité
		return false;
	}
	
	/**
	 * Charge tout les status
	 * @return Le tableau des EStatus | false si une erreur se produit
	 */
	public function loadAllStatus() {
		$sql = 'SELECT * FROM STATUS ORDER BY CODE';
		try {
			$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute();
	
			while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
				//
				$in = new EStatus($row['CODE'], $row['LABEL']);
				array_push($this->status, $in);
			} #end while
	
		} catch (PDOExeception $e) {
			echo "EStatusManager:loadAllstatus Error : " . $e->getMessage();
			return false;
		}
		// Return le tableau de tout les status
		return $this->status;
	}
	
	public function getAllStatus() {
		return $this->status;
	}
}
