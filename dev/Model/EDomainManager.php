<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
* Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
* Version: 1.0.0
* Date: 25.11.2016
* Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

require_once '../Model/EDomain.php';

/**
 * @brief	Helper class pour gérer les domaines
 * @author 	jean-daniel.knz@eduge.ch
 * @remark
 * @version     1.0.0
 */
class EDomainManager {
	private static $objInstance;
	
	/**
	 * @brief	Class Constructor - Create a new EUserManager if one doesn't exist
	 * 			Set to private so no-one can create a new instance via ' = new EUserManager();'
	 */
	private function __construct() {
		$this->domain = array();
	}
	
	/** @brief Contient le tableau des EDomain */
	private $domain;
	
	/**
	 * @brief	Retourne notre instance ou la crée
	 * @return $objInstance;
	 */
	public static function getInstance() {
		if (!self::$objInstance) {
			try {
	
				self::$objInstance = new EDomainManager();
			} catch (Exception $e) {
				echo "EDomainManager Error: " . $e;
			}
		}
		return self::$objInstance;
	}
	
	/**
	 * Recherche si un domaine existe dans la base de données en fonction de son ID
	 * @param unknown $InDomainId	l'ID du domaine rechercher
	 * @return true si le stage existe, autrement false
	 */
	public function domainExistInDB($inId) {
		$sql = 'SELECT * FROM DOMAINS WHERE DOMAINS_PK = :i';
	
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array(':i' => $inId));
			 
			$result = $stmt->fetchAll();
			return (count($result) > 0) ? true : false;
		} catch (PDOException $e) {
			echo "EDomainManager:domainExistInDB Error: " . $e->getMessage();
			return false;
		}
		// J'ai pas trouvé le domaine
		return false;
	}
	
	/**
	 * Charge tout les domaines
	 * @return Le tableau des EDomain | false si une erreur se produit
	 */
	public function loadAllDomains() {
		$sql = 'SELECT * FROM DOMAINS';
		try {
			$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute();
	
			while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
				//
				$in = new EDomain($row['DOMAINS_PK'], $row['NAME']);
				array_push($this->domain, $in);
			} #end while
	
		} catch (PDOExeception $e) {
			echo "EDomainManager:loadAllDomains Error : " . $e->getMessage();
			return false;
		}
		// Return le tableau de tout les domaines
		return $this->domain;
	}
	
	/**
	 * Recherche un domaine spécifique dans la base en fonction de son sujet
	 * @$inName	Le nom du domaine que l'on cherche
	 * @return true si le domaine a été trouvé, sinon false
	 */
	public function findDomainByName($inName) {
		//On cherche si le domaine existe dans le tableau
		foreach ($this->domain as $in) {
			if ($inName == $in->getName())
				return $in;
		}
		$sql = 'SELECT * FROM DOMAINS WHERE NAME = :n';
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array(':n' => $inName));
	
			$result = $stmt->fetchAll();
			if (count($result) > 0) {
				// Création du domaine avec les données provenant de la base de données
				$in = new EDomain($result[0]['DOMAINS_PK'], $result[0]['NAME']);
				array_push($this->domain, $in);
				return $in;
			}
		} catch (PDOException $e) {
			echo "EDomainManager:findDomainByName Error: " . $e->getMessage();
			return false;
		}
	
		// J'ai pas trouvé le domaine
		return false;
	}
	
	/**
	 * Recherche un domaine spécifique dans la base en fonction de son id
	 * @$inId		L'id du domaine que l'on cherche
	 * @return true si le domaine a été trouvé | sinon false
	 */
	public function getDomainById($inId) {
		$sql = 'SELECT * FROM DOMAINS WHERE DOMAINS_PK = :id';
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array(':id' => $inId));
	
			$result = $stmt->fetchAll();
			if(empty($result))
				return "Indéfinis";
				else{
					$domain = new EDomain($result[0]['DOMAINS_PK'], $result[0]['NAME']);
					return $domain;
				}
		} catch (PDOException $e) {
			echo "EDomainManager:getDomainById Error: " . $e->getMessage();
			return false;
		}
		// J'ai pas trouvé le domaine
		return false;
	}
	
	/**
	 * Récupère l'ID du domaine via l'ID de l'entreprise
	 * @$inId	ID de l'entreprise
	 * @return	l'ID du domaine	
	 */
	public function getIdDomainByIdEnterprise($inId) {
		$sql = 'SELECT ENTE_DOMA_FK FROM ENTERPRISES WHERE ENTERPRISES_PK = :id';
		
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array(':id' => $inId));
			
			$result = $stmt->fetchAll();
			
			$idDomain = $result[0][0];
			return $idDomain;
		} catch (PDOException $e) {
			echo "EDomainManager:getIdDomainByIdEnterprise Error: " . $e->getMessage();
			return false;
		}
		// Je n'ai pas trouvé d'ID
		return false;
	}
	
	/**
	 * Cette fonction permet d'ajouter un domaine à la base de données
	 * @$inName	le nom du domaine à ajouter
	 * @return L'objet EDomain créé | false si une erreur se produit
	 */
	public function addDomain($inName) {
		$sql = 'INSERT INTO DOMAINS (NAME) values (:n)';
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array( ':n' => $inName));
			 
			// Requete SQL qui permet de récupèrer l'ID du nouveau responsable
			$sql = 'SELECT DOMAINS_PK FROM DOMAINS ORDER BY DOMAINS_PK DESC LIMIT 1';
			$stmt = EDatabase::prepare($sql);
			$stmt->execute();
			$id = $stmt->fetchAll();
			
			// Si pas d'erreur
			$in = new EDomain($id[0][0], $inName);
			return $id[0][0];
		} catch (PDOException $e) {
			echo "EDomainManager:addDomain Error: " . $e->getMessage();
			return false;
		}
		// j'ai réussi à ajouter le domaine
		return $in;
	}
	
	public function getAllDomains() {
		return $this->domain;
	}
}
