<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
* Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
* Version: 1.0.0
* Date: 25.11.2016
* Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

require_once '../Model/ECountries.php';

/**
 * @brief	Helper class pour gérer les Countries
 * @author 	jean-daniel.knz@eduge.ch
 * @remark
 * @version     1.0.0
 */
class ECountriesManager {
	private static $objInstance;
	
	/**
	 * @brief	Class Constructor - Create a new EUserManager if one doesn't exist
	 * 			Set to private so no-one can create a new instance via ' = new EUserManager();'
	 */
	private function __construct() {
		$this->country = array();
	}
	
	/** @brief Contient le tableau des ECountries */
	private $country;
	
	/**
	 * @brief	Retourne notre instance ou la crée
	 * @return $objInstance;
	 */
	public static function getInstance() {
		if (!self::$objInstance) {
			try {
	
				self::$objInstance = new ECountriesManager();
			} catch (Exception $e) {
				echo "ECountriesManager Error: " . $e;
			}
		}
		return self::$objInstance;
	}
	
	/**
	 * Recherche si un countrye existe dans la base de données en fonction de son ID
	 * @param unknown $IncountryId	l'ID du countrye rechercher
	 * @return true si le stage existe, autrement false
	 */
	public function CountryExistInDB($inId) {
		$sql = 'SELECT * FROM COUNTRIES WHERE ISO2 = :i';
	
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array(':i' => $inId));
			 
			$result = $stmt->fetchAll();
			return (count($result) > 0) ? true : false;
		} catch (PDOException $e) {
			echo "ECountriesManager:CountryExistInDB Error: " . $e->getMessage();
			return false;
		}
		// J'ai pas trouvé le countrye
		return false;
	}
	
	/**
	 * Charge tout les countryes
	 * @return Le tableau des ECountries | false si une erreur se produit
	 */
	public function loadAllCountries() {
		$sql = 'SELECT * FROM COUNTRIES';
		try {
			$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute();
	
			while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
				//
				$in = new ECountries($row['ISO2'], $row['LABEL']);
				array_push($this->country, $in);
			} #end while
	
		} catch (PDOExeception $e) {
			echo "ECountriesManager:loadAllCountries Error : " . $e->getMessage();
			return false;
		}
		// Return le tableau de tout les countryes
		return $this->country;
	}
	
	/**
	 * Recherche un countrye spécifique dans la base en fonction de son sujet
	 * @$inName	Le nom du countrye que l'on cherche
	 * @return true si le countrye a été trouvé, sinon false
	 */
	public function findCountriesByName($inName) {
		//On cherche si le countrye existe dans le tableau
		foreach ($this->country as $in) {
			if ($inName == $in->getName())
				return $in;
		}
		$sql = 'SELECT * FROM COUNTRIES WHERE LABEL = :n';
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array(':n' => $inName));
	
			$result = $stmt->fetchAll();
			if (count($result) > 0) {
				// Création du countrye avec les données provenant de la base de données
				$in = new ECountries($result[0]['ISO2'], $result[0]['LABEL']);
				array_push($this->country, $in);
				return $in;
			}
		} catch (PDOException $e) {
			echo "ECountriesManager:findCountriesByName Error: " . $e->getMessage();
			return false;
		}
	
		// J'ai pas trouvé le countrye
		return false;
	}
	
	/**
	 * Recherche un country spécifique dans la base en fonction de son id
	 * @$inId		L'id du countrye que l'on cherche
	 * @return true si le countrye a été trouvé | sinon false
	 */
	public function getCountriesById($inId) {
		$sql = 'SELECT NAME FROM COUNTRIES WHERE ISO2 = :id';
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array(':id' => $inId));
	
			$result = $stmt->fetchAll();
			if(empty($result))
				return "Indéfinis";
				else{
					$country = $result[0][0];
					return $country;
				}
		} catch (PDOException $e) {
			echo "ECountriesManager:getCountriesById Error: " . $e->getMessage();
			return false;
		}
		// J'ai pas trouvé le countrye
		return false;
	}
	
	/**
	 * Cette fonction permet d'ajouter un countrye à la base de données
	 * @$inName	le nom du countrye à ajouter
	 * @return L'objet ECountries créé | false si une erreur se produit
	 */
	public function addCountry($inName) {
		$in = false;
		$inId = null;
		$sql = 'INSERT INTO COUNTRIES (LABEL) values (:n)';
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array( ':n' => $inName));
			 
			// Si pas d'erreur
			$in = new ECountries($inId, $inName);
			array_push($this->country, $in);
		} catch (PDOException $e) {
			echo "ECountriesManager:addCountry Error: " . $e->getMessage();
			return false;
		}
		// j'ai réussi à ajouter le countrye
		return $in;
	}
	
	public function getAllCountries() {
		return $this->country;
	}
}
