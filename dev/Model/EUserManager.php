<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

require_once '../php/config/database.php';
require_once '../Model/EUser.php';

/**
 * @brief	Helper class pour gérer les EUser
 * @author 	jean-daniel.knz@eduge.ch
 * @remark
 * @version	1.0.0
 */
class EUserManager {

    private static $objInstance;

    /**
     * @brief	Class Constructor - Create a new EUserManager if one doesn't exist
     * 			Set to private so no-one can create a new instance via ' = new EUserManager();'
     */
    private function __construct() {
        $this->users = array();
        $this->student = array();
        $this->tutor = array();
    }

    /**
     * @brief	Like the constructor, we make __clone private so nobody can clone the instance
     */
    private function __clone() {
        
    }

    /**
     * @brief	Retourne notre instance ou la crée
     * @return $objInstance;
     */
    public static function getInstance() {
        if (!self::$objInstance) {
            try {

                self::$objInstance = new EUserManager();
            } catch (Exception $e) {
                echo "EUserManager Error: " . $e;
            }
        }
        return self::$objInstance;
    }

# end method

	/**
	 * Créer un utilisateur dans la base de données
	 * @param EUser u L'utilisateur à créer dans la base de données
	 * @return boolean True l'utilisateur a été créé, False une erreur est survenue
	 */
	public function CreateUser($u){
		$sql = 'INSERT INTO USERS (EMAIL_PK, LASTNAME, FIRSTNAME, USER_ROLE_CODE, USER_CIST_CODE) VALUES(:email, :edu_lastname, :edu_firstname, :user_role_code, :user_cist_code)';

		try {
			// Préparation de la requête
			$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			// Execution de la requête
			$stmt->execute(array(':email' => $u->getEmail(),
								 ':edu_lastname' => $u->getLastName(),
								 ':edu_firstname' => $u->getFirstName(),
								 ':user_role_code' => $u->getRole(),
								 ':user_cist_code' => $u->getCivility()));
			// Done, utilisateur créée
			return true;
		} catch (PDOException $e) {
			echo "EUserManager:CreateUser Error: " . $e->getMessage();
			return false;
		}
		// On devrait jamais arriver ici
		return false;
	}

    /**
     * Recherche si un utilisateur existe dans le base de donnée fonction de son email
     * @email		L'email à rechercher
     * @return		True si l'utilisateur existe, autrement false
     */
    public function userExistInDB($inEmail) {
        $sql = 'SELECT  EMAIL_PK, LASTNAME, FIRSTNAME, ROLES_ID FROM USERS WHERE EMAIL = :e';
        try {
            $stmt = EDatabase::prepare($sql);
            $stmt->execute(array(':e' => $inEmail));

            $result = $stmt->fetchAll();
            return (count($result) > 0) ? true : false;
        } catch (PDOException $e) {
            echo "EUserManager:userExistInDB Error: " . $e->getMessage();
            return false;
        }
        // J'ai pas trouvé le user
        return false;
    }
# end method

    /**
     * Charge tout les stages
     * @return Le tableau des EUsers | false si une erreur se produit
     */
    public function loadAllUsers() {
    	$sql = 'SELECT * FROM USERS';
    	try {
    		$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute();
    
    		while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
    			//
    			$in = new EUser($row['EMAIL_PK'], $row['LASTNAME'], $row['FIRSTNAME'], $row['USER_ROLE_CODE'], $row['USER_CIST_CODE']);
    			array_push($this->users, $in);
    		} #end while
    
    	} catch (PDOExeception $e) {
    		echo "EAppManager:loadAllUsers Error : " . $e->getMessage();
    		return false;
    	}
    	// Return le tableau de tout les stages
    	return $this->users;
    }  
    
    /**
     * Recherche un utilisateur en fonction de son email
     * @param string $inEmail	L'email à rechercher
     * @return EUser $in		L'utilisateur type EUser | false si n'existe pas
     */
    public function findUserByEmail($inEmail) {
        // On cherche si l'utilisateur existe dans le tableau
        foreach ($this->users as $in) {
            if ($inEmail == $in->getEmail())
                return $in;
        }# end foreach

        $sql = "SELECT * FROM USERS WHERE EMAIL_PK = :e";
        try {
            $stmt = EDatabase::prepare($sql);
            $stmt->execute(array(':e' => $inEmail));

            $result = $stmt->fetchAll();
            if (count($result) > 0) {

            	$civility = ECivilityManager::getInstance()->getCivilityLabelByCode($result[0]['USER_CIST_CODE']);

                // Création du user avec les données provenant de la base de données
                $in = new EUser($result[0]['EMAIL_PK'], $result[0]['LASTNAME'], $result[0]['FIRSTNAME'], $result[0]['USER_ROLE_CODE'], $civility);
                array_push($this->users, $in);
                return $in;
            } #end while
        } catch (PDOException $e) {
            echo "EUserManager:findUserByEmail Error: " . $e->getMessage();
            return false;
        }
        // J'ai pas trouvé le user
        return false;
    }#end method
    
    public function selectAllStudent(){
    	$sql = "SELECT * FROM USERS WHERE USER_ROLE_CODE = 1";
    	try {
    		$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute();
    		 
    		while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
    			
    			$user = EUserManager::getInstance()->findUserByEmail($row['EMAIL_PK']);
    			// Création du user avec les données provenant de la base de données
    			$in = new EUser($row['EMAIL_PK'], $row['FIRSTNAME'], $row['LASTNAME'], $row['USER_ROLE_CODE'], $user->getCivility());
    			array_push($this->student, $in);
    		} #end while
    	} catch (PDOException $e) {
    		echo "EUserManager:selectAllStudent Error: " . $e->getMessage();
    		return false;
    	}
    	// J'ai pas trouvé le user
    	return $this->student;
    	}#end method
    	
    	/**
    	 * Cette fonction permet de sélectionner tous les utilisateur qui ne sont pas associé à un stage en cours ou à venir
    	 * @return boolean
    	 */
    	public function selectAllStudentNotInTrainee(){
    		$sql = 'SELECT * FROM ' . EDB_DBNAME . '.USERS WHERE NOT exists (SELECT TRAI_USER_STUDENT_FK as EMAIL_PK FROM ' . EDB_DBNAME . '.TRAINEESHIPS WHERE (DATE_BEGIN > CURDATE() OR (DATE_BEGIN <= CURDATE() AND DATE_END >= CURDATE())) AND (TRAINEESHIPS.TRAI_USER_STUDENT_FK = USERS.EMAIL_PK)) AND USER_ROLE_CODE = 1;';
    		try {
    			$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    			$stmt->execute();
    			 
    			while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
    				 
    				$user = EUserManager::getInstance()->findUserByEmail($row['EMAIL_PK']);
    				// Création du user avec les données provenant de la base de données
    				$in = new EUser($row['EMAIL_PK'], $row['FIRSTNAME'], $row['LASTNAME'], $row['USER_ROLE_CODE'], $user->getCivility());
    				array_push($this->student, $in);
    			} #end while
    		} catch (PDOException $e) {
    			echo "EUserManager:selectAllStudentNotInTrainee Error: " . $e->getMessage();
    			return false;
    		}
    		// J'ai pas trouvé le user
    		return $this->student;
    	}#end method
    	
    	public function selectAllTutor(){
    		$sql = "SELECT * FROM USERS WHERE USER_ROLE_CODE <> 1";
    		try {
    			$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    			$stmt->execute();
    			 
    			while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
    				
    				$user = EUserManager::getInstance()->findUserByEmail($row['EMAIL_PK']);
    				$nbrStudent = $this->getInstance()->getCountAllStudentByTutor($row['EMAIL_PK']);
    				// Création du user avec les données provenant de la base de données
    				$in = new EUser($row['EMAIL_PK'], $row['FIRSTNAME'], $row['LASTNAME'], $row['USER_ROLE_CODE'], $user->getCivility());
    				$in->nbrStudent = $nbrStudent;
    				array_push($this->tutor, $in);
    			} #end while
    		} catch (PDOException $e) {
    			echo "EUserManager:selectAllStudent Error: " . $e->getMessage();
    			return false;
    		}
    		// J'ai pas trouvé le user
    		return $this->tutor;
    	}#end method
    
    	/**
    	 * Fonction servant à récupérer le libellé de la civilité en foncftion d'un id
    	 * @param	integer	$inId	L'id de la civilité
    	 * @return	string			Le label de la civilité
    	 */
    public function selectCivilityById($inId){
    	$sql = 'SELECT LABEL FROM CIVILIAN_STATES c, USERS u WHERE u.USER_CIST_CODE = c.CODE AND u.USER_CIST_CODE = :inId';
    	try {
    		$stmt = EDatabase::prepare($sql);
    		$stmt->execute(array(':inId' => $inId));
    		
    		$result = $stmt->fetchAll();
    		if($result == null)
    			return "Aucune information";
    			else{
    				$nbr = $result[0][0];
    				return $nbr;
    		} #end while
    	} catch (PDOException $e) {
    		echo "EUserManager:findUserByEmail Error: " . $e->getMessage();
    		return false;
    	}
    	// J'ai pas trouvé le user
    	return false;
    	}#end method
    	
    	/**
    	 * Cette fonction permet d'obtenir les élèves qua déja eu un profs
    	 * @param 	string			$inEmail	l'email du profs à chercher
    	 * @return	EUser|string				les élèves que le profs a eu | sinon Aucune information
    	 */
    	public function getAllStudentByTutor($inEmail){
    		$sql = 'SELECT STTU_STUD_FK, STTU_TRAI_FK FROM STUDENTS_HAS_TUTORS WHERE STTU_TUTO_FK = :e;';
    		try {
    			$stmt = EDatabase::prepare($sql);
    			$stmt->execute(array(':e' => $inEmail));
    	
    			if (count($result) > 0) {
    			
    				$civility = ECivilityManager::getInstance()->getCivilityLabelByCode($result[0]['USER_CIST_CODE']);
    			
    				// Création du user avec les données provenant de la base de données
    				$in = new EUser($result[0]['EMAIL_PK'], $result[0]['FIRSTNAME'], $result[0]['LASTNAME'], $result[0]['USER_ROLE_CODE'], $civility);
    				array_push($this->users, $in);
    				return $in;
    			} #end while
    			} catch (PDOException $e) {
    				echo "EUserManager:findUserByEmail Error: " . $e->getMessage();
    				return false;
    			}
    			// J'ai pas trouvé le user
    			return false;
    		}#end method
    	
    	/**
    	 * Cette fonction permet d'obtenir le nombre d'élèves qua déja eu un profs
    	 * @param 	string		$inEmail	l'email du profs à chercher
    	 * @return	int|int				le nombre d'élèves que le profs a eu | sinon 0
    	 */
    	public function getCountAllStudentByTutor($inEmail){
    		$sql = 'SELECT COUNT(STTU_STUD_FK) FROM STUDENTS_HAS_TUTORS WHERE STTU_TUTO_FK = :e;';
    		try {
    			$stmt = EDatabase::prepare($sql);
    			$stmt->execute(array(':e' => $inEmail));

    			$result = $stmt->fetchAll();
    			if($result == null)
    				return "0";
    				else{
    					$nbr = $result[0][0];
    					return $nbr;
    				} #end while
    			} catch (PDOException $e) {
    				echo "EUserManager:getCountAllStudentByTutor Error: " . $e->getMessage();
    				return false;
    			}
    			// J'ai pas trouvé le user
    			return false;
    			}#end method
    			
    /**
     * Retourne le tableau de tous les utilisateurs
     * @return Le tableau des EUser
     */
    public function getAllUsers() {
        return $this->users;
    }

    /** @var Contient le tableau des EUser */
    private $users;
}
