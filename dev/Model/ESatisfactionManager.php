<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
* Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
* Version: 1.0.0
* Date: 25.11.2016
* Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

require_once '../Model/ESatisfaction.php';

/**
 * @brief	Helper class pour gérer les domaines
 * @author 	jean-daniel.knz@eduge.ch
 * @remark
 * @version     1.0.0
 */
class ESatisfactionManager {
	private static $objInstance;
	
	/**
	 * @brief	Class Constructor - Create a new ESatisfactionManager if one doesn't exist
	 * 			Set to private so no-one can create a new instance via ' = new ESatisfactionManager();'
	 */
	private function __construct() {
		$this->satisfaction = array();
	}
	
	/** @brief Contient le tableau des EDomain */
	private $satisfaction;
	
	/**
	 * @brief	Retourne notre instance ou la crée
	 * @return $objInstance;
	 */
	public static function getInstance() {
		if (!self::$objInstance) {
			try {
	
				self::$objInstance = new ESatisfactionManager();
			} catch (Exception $e) {
				echo "ESatisfactionManager Error: " . $e;
			}
		}
		return self::$objInstance;
	}
	
	/**
	 * Charge tout les critères
	 * @return Le tableau des ESatisfaction | false si une erreur se produit
	 */
	public function loadAllSatisfactions() {
		$sql = 'SELECT * FROM SATISFACTIONS ORDER BY CODE';
		try {
			$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute();
	
			while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
				//
				$in = new ESatisfaction($row['CODE'], $row['LABEL']);
				array_push($this->satisfaction, $in);
			} #end while
	
		} catch (PDOExeception $e) {
			echo "ESatisfactionManager::loadAllSatisfactions Error : " . $e->getMessage();
			return false;
		}
		// Return le tableau de tout les domaines
		return $this->satisfaction;
	}
	
	
		public function getSatisfactionLabelByCode($inCode) {
		$sql = "SELECT LABEL FROM SATISFACTIONS WHERE CODE = :code";
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array(':code' => $inCode));
			
			$result = $stmt->fetchAll();
			$label = $result[0][0];
			return $label;
			
		} catch (PDOException $e) {
			echo "ESatisfactionManager:getSatisfactionLabelByCode Error: " . $e->getMessage();
			return false;
		}
		// Je n'ai pas trouvé le label la disponibilité
		return false;
	}
	
	public function getAllSatisfaction() {
		return $this->satisfaction;
	}
}
