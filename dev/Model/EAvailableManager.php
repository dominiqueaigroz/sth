<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
* Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
* Version: 1.0.0
* Date: 25.11.2016
* Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

require_once '../Model/EAvailable.php';

/**
 * @brief	Helper class pour gérer les domaines
 * @author 	jean-daniel.knz@eduge.ch
 * @remark
 * @version     1.0.0
 */
class EAvailableManager {
	private static $objInstance;
	
	/**
	 * @brief	Class Constructor - Create a new EAvailableManager if one doesn't exist
	 * 			Set to private so no-one can create a new instance via ' = new EAvailableManager();'
	 */
	private function __construct() {
		$this->available = array();
	}
	
	/** @brief Contient le tableau des EDomain */
	private $available;
	
	/**
	 * @brief	Retourne notre instance ou la crée
	 * @return $objInstance;
	 */
	public static function getInstance() {
		if (!self::$objInstance) {
			try {
	
				self::$objInstance = new EAvailableManager();
			} catch (Exception $e) {
				echo "EAvailableManager Error: " . $e;
			}
		}
		return self::$objInstance;
	}
	
	
	public function getAvailableLabelByCode($inCode) {
		$sql = 'SELECT * FROM ' . EDB_DBNAME . '.AVAILABLE WHERE CODE = :code';
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array(':code' => $inCode));
			
			$result = $stmt->fetchAll();
			$in = new EAvailable($result[0]['CODE'], $result[0]['LABEL']);
			return $in;
			
		} catch (PDOException $e) {
			echo "EAvailableManager:getAvailableLabelByCode Error: " . $e->getMessage();
			return false;
		}
		// Je n'ai pas trouvé le label la disponibilité
		return false;
	}
	
	/**
	 * Charge toutes les disponibilités
	 * @return Le tableau des EAvailable | false si une erreur se produit
	 */
	public function loadAllAvailable() {
		$sql = 'SELECT * FROM AVAILABLE ORDER BY CODE';
		try {
			$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute();
	
			while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
				//
				$in = new EAvailable($row['CODE'], $row['LABEL']);
				array_push($this->available, $in);
			} #end while
	
		} catch (PDOExeception $e) {
			echo "EAvailableManager:loadAllAvailable Error : " . $e->getMessage();
			return false;
		}
		// Return le tableau de tout les domaines
		return $this->available;
	}
	
	public function getAllAvailable() {
		return $this->available;
	}
	
}
