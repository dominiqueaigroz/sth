<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
* Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
* Version: 1.0.0
* Date: 25.11.2016
* Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

require_once '../Model/ESector.php';

/**
 * @brief	Helper class pour gérer les secteurs
 * @author 	jean-daniel.knz@eduge.ch
 * @remark
 * @version     1.0.0
 */
class ESectorManager {
	private static $objInstance;
	
	/**
	 * @brief	Class Constructor - Create a new EUserManager if one doesn't exist
	 * 			Set to private so no-one can create a new instance via ' = new EUserManager();'
	 */
	private function __construct() {
		$this->sector = array();
	}
	
	/** @brief Contient le tableau des ESector */
	private $sector;
	
	/**
	 * @brief	Retourne notre instance ou la crée
	 * @return $objInstance;
	 */
	public static function getInstance() {
		if (!self::$objInstance) {
			try {
	
				self::$objInstance = new ESectorManager();
			} catch (Exception $e) {
				echo "EDomainManager Error: " . $e;
			}
		}
		return self::$objInstance;
	}
	
	/**
	 * Recherche si un secteur existe dans la base de données en fonction de son ID
	 * @param unknown $InSectorId	l'ID du secteur rechercher
	 * @return true si le stage existe, autrement false
	 */
	public function sectorExistInDB($inId) {
		$sql = 'SELECT * FROM SECTORS WHERE SECTORS_PK = :i';
	
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array(':i' => $inId));
			 
			$result = $stmt->fetchAll();
			return (count($result) > 0) ? true : false;
		} catch (PDOException $e) {
			echo "EAppManager:sectorExistInDB Error: " . $e->getMessage();
			return false;
		}
		// J'ai pas trouvé le secteur
		return false;
	}
	
	/**
	 * Charge tout les secteurs
	 * @return Le tableau des ESector | false si une erreur se produit
	 */
	public function loadAllSectors() {
		$sql = 'SELECT * FROM SECTORS';
		try {
			$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute();
	
			while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
				//
				$in = new ESector($row['SECTORS_PK'], $row['NAME']);
				array_push($this->sector, $in);
			} #end while
	
		} catch (PDOExeception $e) {
			echo "EAppManager:loadAllSectors Error : " . $e-getMessage();
			return false;
		}
		// Return le tableau de tout les secteurs
		return $this->sector;
	}
	
	/**
	 * Recherche un secteur spécifique dans la base en fonction de son sujet
	 * @$inName	Le nom du secteur que l'on cherche
	 * @return l'objet de type ESector si le secteur a été trouvé | sinon false
	 */
	public function findSectorByName($inName) {
		//On cherche si le stage existe dans le tableau
		foreach ($this->sector as $in) {
			if ($inName == $in->getName())
				return $in;
		}
		$sql = 'SELECT * FROM SECTORS WHERE NAME = :n';
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array(':n' => $inName));
	
			$result = $stmt->fetchAll();
			if (count($result) > 0) {
				// Création du stage avec les données provenant de la base de données
				$in = new ESector($result[0]['SECTORS_PK'], $result[0]['NAME']);
				array_push($this->sector, $in);
				return $in;
			}
		} catch (PDOException $e) {
			echo "EAppManager:findSectorByName Error: " . $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Recherche un secteur spécifique dans la base en fonction de son sujet
	 * @$inName	Le nom du secteur que l'on cherche
	 * @return l'objet de type ESector si le secteur a été trouvé | sinon false
	 */
	public function findSectorById($inId) {
		$sql = 'SELECT * FROM SECTORS WHERE NAME = :n';
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array(':n' => $inId));
	
			$result = $stmt->fetchAll();
			if (count($result) > 0) {
				// Création du stage avec les données provenant de la base de données
				$in = new ESector($result[0]['SECTORS_PK'], $result[0]['NAME']);
				array_push($this->sector, $in);
				return $in;
			}
		} catch (PDOException $e) {
			echo "EAppManager:findSectorByName Error: " . $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Cette fonction permet d'ajouter un secteur à la base de données
	 * @$inName	le nom du secteur à ajouter
	 * @return L'objet ESector créé | false si une erreur se produit
	 */
	public function addSector($inName) {
		$sql = 'INSERT INTO SECTORS (NAME) values (:n)';
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array( ':n' => $inName));
			 
			// Requete SQL qui permet de récupèrer l'ID du nouveau responsable
			$sql = 'SELECT SECTORS_PK FROM SECTORS ORDER BY SECTORS_PK DESC LIMIT 1';
			$stmt = EDatabase::prepare($sql);
			$stmt->execute();
			$id = $stmt->fetchAll();
			
			// Si pas d'erreur
			$in = new ESector($id[0][0], $inName);
			return $id[0][0];
			
		} catch (PDOException $e) {
			echo "EAppManager:addSector Error: " . $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Cette fonction permet d'ajouter un secteur qui correspond à un stage
	 * @param int $inTrain	l'ID du stage à ajouter
	 * @param int $inSect		l'ID du secteur à ajouter
	 * @return false si une erreure c'est produite
	 */
	public function addSectorHasTraineeships($inTrain, $inSect) {

		$sql = 'INSERT INTO SECTORS_HAS_TRAINEESHIPS (SETR_TRAI_FK, SETR_SECT_FK) values (:t, :s)';
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array( ':t' => $inTrain, ':s' => $inSect));
	
			
		} catch (PDOException $e) {
			echo "EAppManager:addSectorHasTraineeships Error: " . $e->getMessage();
			return false;
		}
	}
	
	public function deleteAllSectorsByTrainId($inTrain) {
		
		$sql = 'DELETE FROM SECTORS_HAS_TRAINEESHIPS WHERE SETR_TRAI_FK = :t';
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array( ':t' => $inTrain));
			
		} catch (PDOException $e) {
			echo "EAppManager:deleteAllSectorsByTrainId Error: " . $e->getMessage();
			return false;
		}
		
	}
	
	public function getAllSectors() {
		return $this->sector;
	}
}