<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
* Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
* Version: 1.0.0
* Date: 25.11.2016
* Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

require_once '../Model/ECivility.php';

/**
 * @brief	Helper class pour gérer les domaines
 * @author 	jean-daniel.knz@eduge.ch
 * @remark
 * @version     1.0.0
 */
class ECivilityManager {
	private static $objInstance;
	
	/**
	 * @brief	Class Constructor - Create a new ECivilityManager if one doesn't exist
	 * 			Set to private so no-one can create a new instance via ' = new ECivilityManager();'
	 */
	private function __construct() {
		$this->civility = array();
	}
	
	/** @brief Contient le tableau des EDomain */
	private $civility;
	
	/**
	 * @brief	Retourne notre instance ou la crée
	 * @return $objInstance;
	 */
	public static function getInstance() {
		if (!self::$objInstance) {
			try {
	
				self::$objInstance = new ECivilityManager();
			} catch (Exception $e) {
				echo "ECivilityManager Error: " . $e;
			}
		}
		return self::$objInstance;
	}
	
		public function getCivilityLabelByCode($inCode) {
		$sql = "SELECT LABEL FROM CIVILIAN_STATES WHERE CODE = :code";
		try {
			$stmt = EDatabase::prepare($sql);
			$stmt->execute(array(':code' => $inCode));
			
			$result = $stmt->fetchAll();
			$label = $result[0][0];
			return $label;
			
		} catch (PDOException $e) {
			echo "ECivilityManager:getCivilityLabelByCode Error: " . $e->getMessage();
			return false;
		}
		// Je n'ai pas trouvé le label la disponibilité
		return false;
	}
	
	/**
	 * Charge toutes les civilités
	 * @return Le tableau des ECivility | false si une erreur se produit
	 */
	public function loadAllCivilities() {
		$sql = 'SELECT * FROM CIVILIAN_STATES ORDER BY CODE';
		try {
			$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute();
	
			while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
				//
				$in = new ECivility($row['CODE'], $row['LABEL']);
				array_push($this->civility, $in);
			} #end while
	
		} catch (PDOExeception $e) {
			echo "ECivilityManager:loadAllCivilities Error : " . $e->getMessage();
			return false;
		}
		// Return le tableau de tout les domaines
		return $this->civility;
	}
	
	public function getAllCivlity() {
		return $this->civility;
	}
}
