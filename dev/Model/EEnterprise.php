<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

class EEnterprise implements JsonSerializable {

    /**
     * @brief	Class Constructor
     */
    public function __construct($inId = -1, $inName = null, $inDesc = null, $inPhone = null, $inWeb = null, $isActive = false, $inStreet = null, $inZip = null, $inCity = null, $inDomain = 1, $inCountry = 1) {
        $this->id = $inId;
        $this->name = $inName;
        $this->desc = $inDesc;
        $this->phone = $inPhone;
        $this->web = $inWeb;
        $this->isActive = $isActive;
        $this->street = $inStreet;
        $this->zip = $inZip;
        $this->city = $inCity;
        $this->domain = $inDomain;
        $this->country = $inCountry;
        $this->nbrTrainee = 0;
        $this->lastTrainee = 0;
    }



    public function jsonSerialize() {
    	return get_object_vars($this);
    }

    /**
     * @brief	Est-ce que cet objet est valide
     * @return  True si valide, autrement false
     */
    public function isValid() {
        return ($this->id == -1) ? false : true;
    }

    /**
     * @brief	Getter
     * @return  L'Id de l'entreprise
     */
    public function getId() {
    	return $this->id;
    }
    
    /**
     * @brief	Getter
     * @return  Le nom de l'entreprise
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @brief	Getter
     * @return  La descritpion
     */
    public function getDesc() {
        return $this->desc;
    }

    /**
     * @brief	Getter
     * @return  Le numéro de téléphone
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * @brief	Getter
     * @return  Le site web
     */
    public function getWeb() {
        return $this->web;
    }

    /**
     * @brief	Getter
     * @return  L'activité
     */
    public function getActive() {
        return $this->isActive;
    }
    
    /**
     * @brief	Getter
     * @return  La rue
     */
    public function getStreet() {
    	return $this->street;
    }
    
    /**
     * @brief	Getter
     * @return  Le code postal
     */
    public function getZip() {
    	return $this->zip;
    }
    
    /**
     * @brief	Getter
     * @return  La ville
     */
    public function getCity() {
    	return $this->city;
    }
    
    /**
     * @brief	Getter
     * @return  Le domaine
     */
    public function & getDomains() {
    	return $this->domain;
    }
    
    /**
     * @brief	Getter
     * @return  Le pays
     */
    public function getCountry() {
    	return $this->country;
    }
    
    /**
     * @brief	Getter
     * @return	Le nombre de stages effectués
     */
    public function & getNbrTrainee() {
    	return $this->nbrTrainee;
    }

    /**
     * @brief	Getter
     * @return	La date du dernier stage effectué
     */
    public function getLastTrainee() {
    	
    }
    
    /** @brief L'identifiant unique provenant de la base de données */
    private $id;

    /** @brief Le nom de l'entreprise */
    private $name;

    /** @brief La description de l'entreprise */
    private $desc;

    /** @brief Le numéro de téléphone de l'entreprise */
    private $phone;

    /** @brief Le site web de l'entreprise */
    private $web;

    /** @brief L'activité de l'entreprise (Si elle est toujours active ou pas) */
    private $isActive;
    
    /** @brief La rue de l'entreprise */
    private $street;
    
    /** @brief Le code postal de l'entreprise */
    private $zip;
    
    /** @brief La ville de l'entreprise */
    private $city;
    
    /** @brief le domaine de l'entreprise */
    private $domain;
    
    /** @brief Le pays de l'entreprise */
    private $country;
    
    /** @brief Le nombre de stages TERMINÉS que l'entreprise a effectuée */
    public $nbrTrainee;
    
    /** @brief La date du dernier stage effectué par l'entreprise */
    public $lastTrainee;
}
