<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
* Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
* Version: 1.0.0
* Date: 25.11.2016
* Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

require_once '../Model/ECriterion.php';
require_once '../Model/ECriterionHasTraineeship.php';

/**
 * @brief	Helper class pour gérer les critères
 * @author 	jean-daniel.knz@eduge.ch
 * @remark
 * @version     1.0.0
 */
class ECriterionManager {
	private static $objInstance;
	
	/**
	 * @brief	Class Constructor - Create a new ECriterionManager if one doesn't exist
	 * 			Set to private so no-one can create a new instance via ' = new ECriterionManager();'
	 */
	private function __construct() {
		$this->criterion = array();
		$this->critHasTrain = array();
	}
	
	/** @brief Contient le tableau des ECriterion */
	private $criterion;
	
	/** @brief Contient le tableau des ECriterionHasTraineeship */
	private $critHasTrain;
	
	/**
	 * @brief	Retourne notre instance ou la crée
	 * @return $objInstance;
	 */
	public static function getInstance() {
		if (!self::$objInstance) {
			try {
				self::$objInstance = new ECriterionManager();
			} catch (Exception $e) {
				echo "ECriterionManager Error: " . $e;
			}
		}
		return self::$objInstance;
	}
	
	/**
	 * Charge tout les critères
	 * @return Le tableau des ECriterion | false si une erreur se produit
	 */
	public function loadAllCriteria() {
		$sql = 'SELECT * FROM CRITERIA ORDER BY CODE';
		try {
			$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute();
	
			while($row=$stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
				//
				$in = new ECriterion($row['CODE'], $row['LABEL']);
				array_push($this->criterion, $in);
			} #end while
	
		} catch (PDOExeception $e) {
			echo "ECriterionManager::loadAllCriteria Error : " . $e->getMessage();
			return false;
		}
		// Return le tableau de tout les domaines
		return $this->criterion;
	}
	
	/**
	 * Récupère le nombre de critère dans la base
	 * @return le nombre de critère
	 */
	public function getNumberOfCriteria() {
		$sql = 'SELECT COUNT(CODE) FROM CRITERIA';
		
		try {
			$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute();
			$result = $stmt->fetchAll ();
			
			return $result[0][0];
			
		} catch (Exception $e) {
			echo "ECriterionManager::getNumberOfCriteria Error : " . $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Définit le niveau de satisfactions des critères pour un stage
	 * @param string $idTrain		L'ID du stage
	 * @param string $codeCrit		Le code du critère
	 * @param string $codeSatisf	Le code de satisfaction
	 */
	public function setSatisfactionsForCriteria($idTrain, $codeCrit, $codeSatisf) {
		$sql = 'INSERT INTO CRITERIA_HAS_TRAINEESHIPS (CRTR_TRAI_FK, CRTR_CRIT_CODE, SATISFACTIONS_CODE) values (:t, :c, :s)';
		
		try {
			$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute(array(':t' => $idTrain, ':c' => $codeCrit, ':s' =>$codeSatisf));
			
			return true;
			
		} catch (Exception $e) {
			echo "ECriterionManager::setSatisfactionsForCriteria Error : " . $e->getMessage();
			return false;
		}
	}

	/**
	 * Récupère les satisfactions des critères pour un stage
	 * @param string $idTrain	L'ID du stage
	 * @return Le tableau des ECriterionHasTraineeship | false si le stage n'a aucun critère ou si une erreure c'est produite
	 */
	public function getSatisfactionsForCriteriaByTraineeId($idTrain) {
		$sql = 'SELECT CRTR_CRIT_CODE, SATISFACTIONS_CODE FROM CRITERIA_HAS_TRAINEESHIPS WHERE CRTR_TRAI_FK = :id';
	
		try {
			$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute(array(':id' => $idTrain));
				
			$result = $stmt->fetchAll();
				
			if(count($result) > 0) {
				foreach ($result as $crit) {
					$in = new ECriterionHasTraineeship($crit['CRTR_CRIT_CODE'], $crit['SATISFACTIONS_CODE']);
					array_push($this->critHasTrain, $in);
				}
				return $this->critHasTrain;
			}
			else {
				return false;
			}
				
		} catch (Exception $e) {
			echo "ECriterionManager::getSatisfactionsForCriteriaByTraineeId Error : " . $e->getMessage();
			return false;
		}
	}
}
