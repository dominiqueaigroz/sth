<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
* Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
* Version: 1.0.0
* Date: 25.11.2016
* Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

class ESectorHasTraineeship implements JsonSerializable {

	/**
	 * @brief	Class Constructor
	 */
	public function __construct($ShTSect = "", $ShTTrai = "") {
		$this->idSect = $ShTSect;
		$this->idTrai = $ShTTrai;
	}

	public function jsonSerialize() {
		return get_object_vars($this);
	}
	
	/**
	 * @brief	Est-ce que cet objet est valide
	 * @return  True si valide, autrement false
	 */
	public function isValid() {
		// ?
	}




	/** @brief L'identifiant unique provenant de la base de données */
	private $idSect;

	/** @brief Le nom du secteur */
	private $idTrai;

}
