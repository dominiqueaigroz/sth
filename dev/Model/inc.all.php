<?php

/* Auteur: Jessica Baertschi, Jean-Daniel Küenzi, Dario Genga
 * Titre: annuaire_stage
 * Description : Annuaire des entreprises permettant aux élèves souhaitant faire un stage d’avoir un outil qui les aidera pendant leurs recherches.
 * Version: 1.0.0
 * Date: 25.11.2016
 * Copyright: Entreprise Ecole CFPT-I © 2016-2017 */

require_once '../php/config/database.php';
require_once '../php/config/constants.php';
require_once '../Model/ESession.php';
require_once '../Model/EAppManager.php';
require_once '../Model/EDomainManager.php';
require_once '../Model/ECountriesManager.php';
require_once '../Model/ESectorManager.php';
require_once '../Model/EUserManager.php';
require_once '../Model/EAvailableManager.php';
require_once '../Model/EStatusManager.php';
require_once '../Model/EInChargeOfManager.php';
require_once '../Model/ECivilityManager.php';
require_once '../Model/ECriterionManager.php';
require_once '../Model/ESatisfactionManager.php';
require_once '../Model/ECommentManager.php';
require_once '../Model/EIndemnityManager.php';
require_once '../Model/ETraineeship.php';
require_once '../php/library/utilities.php';